#!/bin/bash
JIRA_HEADER="X-Atlassian-Token: no-check"
JIRA_SERVER="abtasty.atlassian.net"
PAGEID="542507009"
TOKEN_CONFLUENCE="bW9uaWthLnBhbmRhQGFidGFzdHkuY29tOjlSTVJGZjhYR1NpdDY5YlFDaWdwOTRFRA=="
PASSWORD="glpat-qndxtssza2auBB_oEZLH"
JOB_ID=$1

# Download the pipeline report
result=$(curl --location --header "PRIVATE-TOKEN: $PASSWORD" "https://gitlab.com/api/v4/projects/15913854/jobs/$JOB_ID/artifacts/ABTastyautomation/target/surefire-reports/custom-emailable-report.html" --output report.html)
echo $result
report_path="$(pwd -P)/report.html"
echo $report_path
REPORT=$(cat $report_path)
# d=$(date)
# echo "Time: $d" >> reports/report.txt

attachmentinfo=`
curl --location --request GET "https://${JIRA_SERVER}/wiki/rest/api/content/${PAGEID}/child/attachment?start=0&limit=10" \
--header "${JIRA_HEADER}" \
--header "Authorization: Basic ${TOKEN_CONFLUENCE}" \
`
ATTACHMENTID=$(echo $attachmentinfo | jq -r '.results[0].id')
echo "ATTACHMENTID: $ATTACHMENTID"

oldversion=`
curl --location --request POST "https://${JIRA_SERVER}/wiki/rest/api/content/${PAGEID}/child/attachment/${ATTACHMENTID}/data" \
--header "${JIRA_HEADER}" \
--header "Content-Type: multipart/form-data" \
--header "minorEdit: true" \
--header "Authorization: Basic ${TOKEN_CONFLUENCE}" \
--form "file=@\"report.html\"" 
`
# echo $oldversion
version=$(echo $oldversion | jq -r '.version.number')
echo "version attachment: $version"

page_version=version=$(curl -s --header "Authorization: Basic ${TOKEN_CONFLUENCE}" "https://${JIRA_SERVER}/wiki/rest/api/content/$PAGEID" | jq '.version.number')
echo $page_version
new_version="$(($page_version + 1))"
echo $new_version


if [[ $version -gt 250 ]]; then
  echo "version > 250, delete attachment and create new one"
curl --location --request POST "https://${JIRA_SERVER}/wiki/json/removeattachment.action?pageId=542507009&fileName=report.html" \
--header "${JIRA_HEADER}" \
--header "Content-Type: application/json" \
--header "Authorization: Basic ${TOKEN_CONFLUENCE}" \

curl --location --request POST "https://${JIRA_SERVER}/wiki/rest/api/content/${PAGEID}/child/attachment" \
--header "${JIRA_HEADER}" \
--header "Content-Type: multipart/form-data" \
--header "Authorization: Basic ${TOKEN_CONFLUENCE}" \
--form "file=@\"report.html\"" ; fi

# alias chrome='/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome'
# chrome --headless --disable-gpu --screenshot=google.png http://www.google.com
report_link="https://abtasty.gitlab.io/-/backoffice/qa-automation/-/jobs/$JOB_ID/artifacts/ABTastyautomation/target/surefire-reports/custom-emailable-report.html"
update_page_payload='{
    "type": "page",
    "title": "Selenium reports",
    "status": "current",
    "body": {
        "editor2": {
            "representation": "storage"
        }
    }
}'
link="<h3>Link to the current HTML report <br></br> (a gitlab account is required) : <br></br><a href=\"$report_link\">📄 click</a></h3><h3>Latest e2e selenium tests logs (click here please 👇): </h3><ac:image><ri:attachment ri:filename=\"report.html\" /></ac:image><h3>How can I see the history of test results ? </h3><p>1. Open More actions → Attachments → Files <br></br>2. Check Versions <br></br></p>"

update_page_payload=$(echo $update_page_payload | jq --arg new_version "$new_version" '.version.number = $new_version')
update_page_payload=$(echo $update_page_payload | jq --arg link "$link" '.body.editor2.value = $link')
echo $update_page_payload

update_page_payload=$(curl --location --request PUT "https://${JIRA_SERVER}/wiki/rest/api/content/${PAGEID}" \
--header "Content-Type: application/json" \
--header "Authorization: Basic $TOKEN_CONFLUENCE" \
--data "$update_page_payload")
echo $update_page_payload