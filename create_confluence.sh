#!/bin/bash

# Set the GitLab URL and login credentials
PASSWORD="glpat-qndxtssza2auBB_oEZLH"
#   token='bW9uaWthLnBhbmRhQGFidGFzdHkuY29tOmdscGF0LXFuZHh0c3N6YTJhdUJCX29FWkxI'
#   artifacts_path='Artifacts/ABTastyautomation/target/surefire-reports/custom-emailable-report.html'

# Set the project ID and pipeline ID
#   PROJECT_ID="15913854"
#   PIPELINE_ID="730105870"
JOB_ID=$1

# Download the pipeline report
result=$(curl --location --header "PRIVATE-TOKEN: $PASSWORD" "https://gitlab.com/api/v4/projects/15913854/jobs/$JOB_ID/artifacts/ABTastyautomation/target/surefire-reports/custom-emailable-report.html" --output report.html)
echo $result
report_path="$(pwd -P)/report.html"
echo $report_path
REPORT=$(cat $report_path)

# Set the API token and endpoint URL
API_TOKEN='9RMRFf8XGSit69bQCigp94ED'
CONFLUENCE_API_URL='https://abtasty.atlassian.net/wiki/rest/api/content'
base64_token='bW9uaWthLnBhbmRhQGFidGFzdHkuY29tOjlSTVJGZjhYR1NpdDY5YlFDaWdwOTRFRA=='

timestamp=$(date +%Y-%m-%d_%H-%M-%S)
# Set the necessary payload for creating the new page

create_page_payload='{
    "type": "page",
    "status": "current",
    "space": {
        "key": "ATK"
    },
    "ancestors": [
        {
            "id": 542507009
        }
    ], 
    "body": {
        "editor2": {
            "value": "<h3>Please find attached the test report:</h3>",
            "representation": "storage"
        }
    }
}'


PAYLOAD=$(echo $create_page_payload | jq --arg timestamp "$timestamp" '.title = $timestamp')

echo $PAYLOAD
ATTACHMENT_FILE_PATH='report.html'

# # Send the POST request to create the new page
response=$(curl -X POST -H "Content-Type: application/json" \
-H "Authorization: Basic $base64_token" \
-d "$PAYLOAD" "$CONFLUENCE_API_URL" \
-H "X-Atlassian-Token: nocheck")
# echo $response

page_id=$(echo $response | jq -r '.id')
echo $page_id

version=$(echo $response | jq -r '.version.number')
echo $version

title=$(echo $response | jq -r '.title')
echo $title

attach_response=$(curl -X POST "$CONFLUENCE_API_URL/$page_id/child/attachment" \
-H "Authorization: Basic $base64_token" \
-H "X-Atlassian-Token: nocheck" \
-F "file=@$ATTACHMENT_FILE_PATH")
# echo $attach_response

# Extract the attachment id from the response
attachment_id=$(echo $attach_response | jq -r '.results[].id')
echo $attachment_id

# attachment_url==$(echo $attach_response | jq -r '.results[]._links.download')
# echo $attachment_url
attachment_url="/download/attachments/543031322/report.html?version=1modificationDate=1673935603243&cacheVersion=1api=v2"

# Add the <img> tag with the attachment URL to the page content
page_content="<img src='$attachment_url'/>"

update_page_payload='{
    "version": {
        "number": 2
    },
    "type": "page",
    "status": "current",
    "body": {
        "editor2": {
            "value": "<h3>Link to the current HTML report<br></br></h3><ac:image><ri:attachment ri:filename=\"report.html\" /></ac:image>",
            "representation": "storage"
        }
    }
}'

update_page_payload=$(echo $update_page_payload | jq --arg timestamp "$timestamp" '.title = $timestamp')
echo $update_page_payload

update_page_payload=$(curl --location --request PUT "https://abtasty.atlassian.net/wiki/rest/api/content/$page_id?status=current&conflictPolicy=abort" \
--header "Content-Type: application/json" \
--header "Authorization: Basic $base64_token" \
--data-raw "$update_page_payload")
# echo $update_page_payload