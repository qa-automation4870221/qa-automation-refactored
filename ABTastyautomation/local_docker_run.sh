#!/bin/bash
set -x

this_path=$(dirname "$0")
project_path=$(cd "$(dirname "$1")"; pwd -p)

docker run -it abtastyautomation:latest \
    cd /qa-automation/ABTastyautomation && mvn clean test -DsuiteFile=overallregression.xml -Dtestenvironment="staging"
