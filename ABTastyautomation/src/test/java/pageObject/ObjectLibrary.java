package pageObject;

import core.BasePage;
import lombok.SneakyThrows;
import org.checkerframework.checker.units.qual.A;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;


public class ObjectLibrary extends BasePage {

    public static By by = null;

    /**
     * Login page
     */
    private final By id = By.xpath("//*[@id='email']");
    private final By password = By.xpath("//*[@id='password']");
    private final By signIn = By.xpath("//*[text()='Sign in']");
    /**
     * AB Tasty - Test campaigns (Pulsar) page
     */
    private final By backToPulsarPageButton = By.xpath("//a[contains(@href,'/experiments')]");
    private final By experimentationPage = By.xpath("//div[@aria-label='Experimentation']");
    //private final By existingABTest = By.xpath("(//div[contains(@class,'TestName__') and contains(@data-testid,'tooltip-trigger')])[1]");
    private final By existingABTest = By.xpath("(//a[contains(text(),'AutotestAB')])[1]");
    private final By accountNames = By.xpath("//div[@role='combobox']");
    private final By typeAccNameInSearchField = By.xpath("//input[@id='downshift-0-input']");
    private final By qaAutomationAcc = By.xpath("//li[text()='QA Automation']");
    private final By isQaAutomationSelected = By.xpath("//input[@placeholder='QA Automation']");
    public final By authWelcome = By.xpath("//div[contains(@class,'UserNameAndRole__welcome')]");
    private final By createCampaignButton = By.xpath("//div[@data-testid='buttonAction-container']");
    private final By createABTestButton = By.xpath("//*[text()='Compare different versions of the same page on your website']");
    private final By createMPTestButton = By.xpath("//*[text()='Test all the pages of a conversion funnel on your site']");
    private final By editCampaignPencilIcon = By.xpath("//*[text()='Test all the pages of a conversion funnel on your site']");

    /**
     * Left SideBar with 7 steps (Main Information, Visual Editor, Goals etc.)
     */
    private final By editorTab = By.xpath("//a[contains(@href,'/editor')]");
    private final By goalTab = By.xpath("//a[contains(@href,'/reporting-metrics')]");
    private final By targetTab = By.xpath("//a[contains(@href,'/audience')]");
    private final By trafficTab = By.xpath("//a[contains(@href,'/modulation')]");

    /**
     * Creating Tests (Main Information step)
     */
    private final By urlInMainInformation = By.xpath("//input[@name='url']");
    private final By abTestNameField = By.xpath("//input[@name='name']");
    private final By saveStepButton= By.xpath("//div[@data-testid='experimentBottomBar']//button[text()='Save step']");
    private final By saveAndGoButton = By.xpath("//button[contains(text(),'Save & Go to next step')]");
    private final By beamerAnnouncementSnippetCloseButton = By.xpath("//div[@class='beamerAnnouncementSnippetClose']");
    /* MP Test */
    private final By firstMPTestNameField1 = By.xpath("(//input[@name='name'])[2]");
    private final By secondMPTestNameField2 = By.xpath("(//input[@name='name'])[3]");
    private final By secondUrlInMainInformation2 = By.xpath("(//input[@name='url'])[2]");

    /**
     * Editor (step 2 in left SideBar)
     */
    //Elements on main page of template website
    private final By titleHeader = By.xpath("//div[@class='title']");
    private final By editElementButtonFromList = By.xpath("//li[@class='hasSubmenu menuicon edit']");
    private final By editStyletButtonFromEditOption = By.xpath("//a[@href='#editStyle']");
    private final By isOnEditorLocator = By.id("abtasty_iframe_overlay");

    private final By widgetSearchField = By.xpath("//input[@class='pluginslibrary_search']");
    private final By addButtonInModal = By.xpath("//button[text()='Add']");

    //[Widget-Banner] tabs locators:
    private final By contentTabInWidget = By.xpath("//div[@name='content']");
    private final By contentTabIsSelected = By.xpath("//div[@name='content' and @class='el-tabs__item is-active']");
    private final By styleTabInWidget = By.xpath("//div[@name='style']");
    private final By styleTabsSelected = By.xpath("//div[@name='style' and @class='el-tabs__item is-active']");
    private final By conditionsTabInWidget = By.xpath("//div[@name='conditions']");
    private final By conditionsTabIsSelected = By.xpath("//div[@name='conditions' and @class='el-tabs__item is-active']");
    private final By bannerText = By.xpath("//*[@id='input__message']");
    private final By saveButton = By.xpath("//button[text()='Save']");
   // private final By cancelButton = By.xpath("//button[text()='Cancel']");
    private final By saveAudience = By.xpath("//div[contains(@class,'styles__cta')]/child::*[2]");
    private final By checkBannerButtonDisplayed = By.xpath("//*[text()='Your call to action']");

    //Iframe on main page

    private final By iframeWidget = By.xpath("//iframe[@sandbox]");

    //Header with Variations, Tag, etc..
    private final By variation2 = By.xpath("//li//span[text()='Variation 2']");
    private final By removeVariationButton = By.xpath("//button[@title='Remove']");
    private final By yesButtonToDeleteVariation = By.xpath("//button[text()='Yes']");
    private final By createNewVariationButton = By.xpath("//li[contains(@class,'tooltip-container')]//button");

    //Edit style modal
    private final By colorTabFromEditStyle = By.xpath("//a[@href='#tabs-styleColor']");
    private final By backgroundColorFromEditStyle = By.xpath("//*[@id='bgColor']");
    private final By elementOpacityFromEditStyle = By.xpath("//input[@id='elementOpacity']");
    private final By closeButtonFromEditStyle = By.xpath("//button[@id='ab-embed-editor__editStyle__save']/preceding-sibling::button");
    private final By saveButtonFromEditStyle = By.xpath("//button[@id='ab-embed-editor__editStyle__save']");

    //Right SideBar with (active changes, add widgets, trackers and etc.)
    private final By arrowLeftSideBar = By.xpath("//*[@class='ab-embed-editor__sidebar__toggle']");
    private final By arrowRightSideBar = By.xpath("//img[@id='arrowRightSidebar']");
    private final By addWidgetButton = By.xpath("//button[contains(., 'Add Widgets')]");
    private final By activeChangesButton = By.xpath("//button[contains(., 'Active changes')]");
    private final By trackersButton = By.xpath("//button[contains(., 'Trackers')]");
    private final By editStyleInActiveChanges = By.xpath("//h2/span[text()='Edit style']");
    private final By opacityForCheck = By.xpath("(//dd[@class])[1]");
    private final By backGroundColorForCheck = By.xpath("(//dd[@class])[2]");

    /**
     * Goals (step 3 in left SideBar)
     */
    //Goals
    private final By actionTracking = By.xpath("//div[contains(@class,'Metric')]//span[contains(text(),'Action tracking')]");
    private final By pageTracking = By.xpath("//div[contains(@class,'Metric')]//span[contains(text(),'Page tracking')]");
    private final By customTracking = By.xpath("//div[contains(@class,'Metric')]//span[contains(text(),'Custom Tracking')]");
    private final By browsing = By.xpath("//div[contains(@class,'Metric')]//span[contains(text(),'Browsing')]");
    //All the existing trackers
    private final By mousedownTracker = By.xpath("(//div[contains(@class,'Item__')]//span[contains(text(),'mousedown')])[1]");
    private final By pageTrackinAccountLevel = By.xpath("//div[contains(@class,'Item__')]//span[contains(text(),'page')]");
    private final By customTracker = By.xpath("//div[contains(@class,'Item')]//span[contains(text(),'custom tracking')]");
    private final By bounceTracker = By.xpath("//div[contains(@class,'Item')]//span[contains(text(),'Bounce')]");
    private final By pagesTracker = By.xpath("//div[contains(@class,'Item')]//span[contains(text(),'Pages')]");
    private final By revisitTracker = By.xpath("//div[contains(@class,'Item')]//span[contains(text(),'Revisit')]");
    //Primal Goal And Secondary Goal
    private final By primaryGoal = By.xpath("(//div[@data-testid='primaryGoal']//div[contains(@class,'Goal')])[1]");
    private final By secondaryGoal = By.xpath("(//div[@data-testid='secondaryGoal']//div[contains(@class,'Goal')])[1]");
    private final By deletePrimaryGoal = By.xpath("//div[@data-testid='primaryGoal']//span[@data-testid='deleteGoalIcon']");
    private final By deleteSecondaryGoal = By.xpath("//div[@data-testid='secondaryGoal']//span[@data-testid='deleteGoalIcon']");
    private final By howManyTargetSelected = By.xpath("//div[contains(@class,'MetricList')]//div[@data-testid='tooltip-trigger']");

    /**
     * Targeting (step 4 in left SideBar)
     */
    //private final By audiencesDropdown = By.xpath("//div[@data-testid='audiencesDropdown']");
    private final By campaignTargetingH1 = By.xpath("//h1[@data-testid='title']");
    private final By audiencesDropdown = By.xpath("//div[@data-testid='audiencesDropdown']//div[contains(@class,'_content')]");
    private final By allVisitorsSegment = By.xpath("//div[@data-testid='audiencesDropdownOption']//div[contains(text(),'All visitors')]");
    private final By allVisitorSelected = By.xpath("//*[contains(@class,'singleValue') and contains(text(),'All visitors')]");
    private final By createNewSegment = By.xpath("//button[@data-testid='audiencesDropdownButton']");
    private final By leaveButtonInModal = By.xpath("//button[text()='Leave']");
    /**
     * Segment builder (Setting up audiences)
     */
    private final By segmentBuilderTitle = By.xpath("//div[contains(@class,'BuilderName') and contains(text(),' ')]");
    private final By segmentDropZone = By.xpath("//div[contains(@class,'containerDroppableZone')]");
    private final By audienceNameChangeButton = By.xpath("//*[@data-testid='audience-name-edit']");
    private final By audienceNameChangeField = By.xpath("//input[@placeholder='Name your segment']");
    private final By createAudienceAnyway = By.xpath("//button[text()='Create']");
    //browsing behavior
    private final By browsingBehaviorList = By.xpath("//*[contains(text(),'Browsing')]/ancestor::div[@data-testid='criteriaListContainer']");
    private final By cartAbandonmentSegment = By.xpath("//*[contains(text(),'Cart aban')]/ancestor::div[@data-testid='tooltip-trigger']");
    private final By cookieSegment = By.xpath("//*[contains(text(),'Cookie')]/ancestor::div[@data-testid='tooltip-trigger']");
    private final By engagementLevelSegment = By.xpath("//*[contains(text(),'Engagement')]/ancestor::div[@data-testid='tooltip-trigger']");
    private final By new_ReturningVisitorsSegment = By.xpath("//*[contains(text(),'Returning')]/ancestor::div[@data-testid='tooltip-trigger']");
    private final By campaignExposureSegment = By.xpath("//*[contains(text(),'Campaign')]/ancestor::div[@data-testid='tooltip-trigger']");
    private final By numberOfSessionsSegment = By.xpath("//*[contains(text(),'Number')]/ancestor::div[@data-testid='tooltip-trigger']");
    private final By daysSinceLastSessionSegment = By.xpath("//*[contains(text(),'Last Session')]/ancestor::div[@data-testid='tooltip-trigger']");
    private final By daysSinceFirstSessionSegment = By.xpath("//*[contains(text(),'First Session')]/ancestor::div[@data-testid='tooltip-trigger']");
    private final By actionTrackingSegment = By.xpath("//*[contains(text(),'Action')]/ancestor::div[@data-testid='tooltip-trigger']");
    //Geographic
    private final By geographicList = By.xpath("//*[contains(text(),'Geographic')]/ancestor::div[@data-testid='criteriaListContainer']");
    private final By geolocationSegment = By.xpath("//*[contains(text(),'Geolocation')]//ancestor::div[@draggable='true']");
    private final By geolocationSegment1 = By.xpath("//*[contains(text(),'Geolocation')]//ancestor::div[contains(@class,'rah-static')]");
    private final By geolocationSelectACountry = By.xpath("(//div[@data-testid='dropdown']//input)[1]");
    private final By geolocationSelectARegion = By.xpath("(//div[@data-testid='dropdown']//input)[2]");
    private final By geolocationSelectASubRegion = By.xpath("(//div[@data-testid='dropdown']//input)[3]");
    private final By geolocationSelectACity = By.xpath("(//div[@data-testid='dropdown']//input)[4]");
    //Interest
    private final By interestList = By.xpath("//*[contains(text(),'Interest')]/ancestor::div[@data-testid='criteriaListContainer']");
    private final By contentInterestSegment = By.xpath("//*[contains(text(),'Content')]/ancestor::div[@data-testid='tooltip-trigger']");
    private final By pageInterestSegment = By.xpath("//*[contains(text(),'Page')]/ancestor::div[@data-testid='tooltip-trigger']");
    //Purchase behavior
    private final By purchaseBehaviorList = By.xpath("//*[contains(text(),'Purchase')]/ancestor::div[@data-testid='criteriaListContainer']");
    private final By lastPurchaseSegment = By.xpath("//*[contains(text(),'Last Purchase')]/ancestor::div[@data-testid='tooltip-trigger']");
    private final By purchaseFrequencySegment = By.xpath("//*[contains(text(),'Purchase frequency')]/ancestor::div[@data-testid='tooltip-trigger']");
    //TECHNOGRAPHIC
    private final By technographicList = By.xpath("//*[contains(text(),'Technographic')]/ancestor::div[@data-testid='criteriaListContainer']");
    private final By deviceSegment = By.xpath("//*[contains(text(),'Device')]/ancestor::div[@data-testid='tooltip-trigger']");
    //Data integration
    private final By dataIntegrationList = By.xpath("//*[contains(text(),'Data integration')]/ancestor::div[@data-testid='criteriaListContainer']");
    private final By visitorAttributes = By.xpath("//*[contains(text(),'Visitor')]/ancestor::div[@data-testid='tooltip-trigger']");

    /**
     * Traffic allocation (step 5 in left SideBar)
     */
    private final By trafficAllocateInput = By.xpath("(//input[@type='number'])[1]");
    private final By changeAllocationButton = By.xpath("//button[@data-testid='SAVE_BUTTON_MODAL']");


    /**
     * List with Edit, Add, Reorder, Add track
     */
    private final By addTrackerButton = By.xpath("//li[@id='contextMenuClickTracking']");
    /**
     * Tracker Modal
     */
    private final By inputNameFromTrackerModal = By.xpath("//input[@placeholder='Click link']");
    private final By saveChangesButtonFromTrackerModal = By.xpath("(//button[contains(text(),'Save changes')])[1]");

    public WebElement getWidgetElement(String widgetName) {
        return waitVisibilityOfElementLocated(By.xpath("//h4[contains(text(),'" + widgetName + "')]/../ancestor::div[@data-testid='widgetCard']"));
    }

    public WebElement getActiveTrackersByName(String trackerName) {
        return driver.findElement(By.xpath("//h5[contains(text(), '" + trackerName + "')]"));
    }

    public void backToPulsar(){
        clickButtonWithWait(backToPulsarPageButton);
        handleAlert();
    }
    public void switchToFrame() {
        WebElement iFrame = driver.findElement(iframeWidget);
        driver.switchTo().frame(iFrame);
    }

    public void switchBackFromFrame() {
        driver.switchTo().defaultContent();
    }

    public void addTrackToSecondaryGoal(By goal, By... trackers) {
        clickButtonWithWait(goal);
        for(By tracker:trackers){
            clickButtonWithWait(tracker);
        }
    }



    public ObjectLibrary enterEmail(String email) {
        WebElement element = driver.findElement(id);
        waitElemetIsVisible(element);
        element.sendKeys(email);
        return this;
    }


    public ObjectLibrary enterPassword(String userPassword) {
        WebElement element = driver.findElement(password);
        waitElemetIsVisible(element);
        element.sendKeys(userPassword);
        return this;
    }

    public ObjectLibrary clickSignIn() {
        driver.findElement(signIn).click();
        return this;
    }


    public ObjectLibrary selectAccount() {
        driver.findElement(typeAccNameInSearchField).sendKeys("QA Automation");
        handleStaleElementThenClick(qaAutomationAcc);
        return this;
    }

    public void isAccSelected() {
        int attempts = 0;
        while (attempts < 2) {
            try {
                waitVisibilityOfElementLocated(isQaAutomationSelected);
                WebElement el = driver.findElement(isQaAutomationSelected);
                String actualValue = el.getAttribute("placeholder");
                String expectedValue = "QA Automation";
                Assert.assertEquals(actualValue, expectedValue, "The selected account does not match");
                break;
            } catch (StaleElementReferenceException e) {
            }
            attempts++;
        }
    }


    public ObjectLibrary createABTest(String testName, String URL) {
        //clicking on create A/B test on the Pulsar page
        driver.navigate().to("https://staging-app2.abtasty.com/dashboard");
        handleStaleElementThenClick(experimentationPage);
        waitVisibilityOfElementLocated(createCampaignButton).click();
        clickButtonWithWait(createABTestButton);

        //creating A/B test page
        waitVisibilityOfElementLocated(abTestNameField).sendKeys(testName);
        waitVisibilityOfElementLocated(urlInMainInformation).sendKeys(URL);
        tryToClickButton(beamerAnnouncementSnippetCloseButton);
        clickButtonWithWait(saveAndGoButton);

        //Assertion, that we are successfully redirected to Editor page
        boolean isIframeOpened = driver.findElement(isOnEditorLocator).isDisplayed();
        Assert.assertTrue(isIframeOpened, "New page didn't create");

        return this;
    }

    @SneakyThrows
    public ObjectLibrary editABTest() {
        //clicking on first existing A/B test on the Pulsar page
        try {
            handleStaleElementThenClick(backToPulsarPageButton);
        } catch (TimeoutException e) {
            driver.navigate().to("https://staging-app2.abtasty.com/dashboard");
        }
        handleAlert();
        handleStaleElementThenClick(experimentationPage);
        tryToClickButton(beamerAnnouncementSnippetCloseButton);
        waitVisibilityOfElementLocated(existingABTest).click();
        changeTab();
        return this;
    }


    public ObjectLibrary createBanner() {

        //changeTab();
        clickButtonWithWait(editorTab);

        waitVisibilityOfElementLocated(addWidgetButton).click();

        getWidgetElement("Banner").click();
        clickButtonWithWait(addButtonInModal);

        clickButtonWithWait(contentTabInWidget);
        assertElementDisplayed(contentTabIsSelected, "Content tab not selected");


        clickButtonWithWait(styleTabInWidget);
        assertElementDisplayed(styleTabsSelected, "Style tab not selected");

        clickButtonWithWait(conditionsTabInWidget);
        assertElementDisplayed(conditionsTabIsSelected, "Conditions tab is not selected");

        clickButtonWithWait(saveButton);
        waitVisibilityOfElementLocated(iframeWidget);
        switchToFrame();
        assertElementDisplayed(checkBannerButtonDisplayed, "Banner is not displayed");
        switchBackFromFrame();
        return this;
    }

    @SneakyThrows
    public ObjectLibrary editStyleCheck() {
       // changeTab();
        clickButtonWithWait(editorTab);
        //changing style of Title to green background and 70% opacity
        switchToFrame();
        Thread.sleep(3000);
        clickButtonWithWait(titleHeader);
        switchBackFromFrame();
        Thread.sleep(3000);
        waitVisibilityOfElementLocated(editElementButtonFromList);
        hoverToElement(driver, editElementButtonFromList);
        hoverAndClick(driver, editStyletButtonFromEditOption);

        waitVisibilityOfElementLocated(colorTabFromEditStyle).click();
        WebElement backGroundColor = driver.findElement(backgroundColorFromEditStyle);
        backGroundColor.clear();
        backGroundColor.sendKeys("#3DFFA5");
        WebElement opacity = driver.findElement(elementOpacityFromEditStyle);
        opacity.clear();
        opacity.sendKeys("70");
        clickButton(saveButtonFromEditStyle);

        clickButtonWithWait(activeChangesButton);
        clickButtonWithWait(editStyleInActiveChanges);
        assertValueEquality(opacityForCheck, "70");
        assertValueEquality(backGroundColorForCheck, "#3DFFA5");
        clickButton(closeButtonFromEditStyle);
        return this;
    }

    @SneakyThrows
    public ObjectLibrary addTrack(String trackerName) {
      //  changeTab();
        clickButtonWithWait(editorTab);
        switchToFrame();
        Thread.sleep(3000);
        clickButtonWithWait(titleHeader);
        switchBackFromFrame();
        Thread.sleep(3000);

        hoverAndClick(driver, addTrackerButton);

        waitVisibilityOfElementLocated(inputNameFromTrackerModal).sendKeys(trackerName);
        clickButtonWithWait(saveChangesButtonFromTrackerModal);
        clickButtonWithWait(trackersButton);
        assertElementDisplayed(getActiveTrackersByName(trackerName), "There are no suitable trackers");
        return this;
    }


    @SneakyThrows
    public ObjectLibrary addDwellTimeTracking(String trackerName) {
        //changeTab();
        clickButtonWithWait(editorTab);
        Thread.sleep(5000);
        waitVisibilityOfElementLocated(addWidgetButton).click();

        getWidgetElement(trackerName).click();
        clickButtonWithWait(addButtonInModal);
        clickButtonWithWait(saveButton);

        clickButtonWithWait(trackersButton);
        assertElementDisplayed(getActiveTrackersByName(trackerName), "There are no suitable trackers");
        return this;
    }

    public ObjectLibrary createVariation() {
       // changeTab();
        clickButtonWithWait(editorTab);
        clickButtonWithWait(createNewVariationButton);
        assertElementDisplayed(variation2, "Error while creating a variation ");
        clickButtonWithWait(variation2);
        clickButtonWithWait(removeVariationButton);
        clickButtonWithWait(yesButtonToDeleteVariation);

        return this;
    }


    public ObjectLibrary addGoals() {
        clickButtonWithWait(goalTab);
        handleAlert();
        clickButtonWithWait(mousedownTracker);

        waitVisibilityOfElementLocated(primaryGoal);
        assertElementDisplayed(primaryGoal, "The tracker did not move to Primary Goals.");

        int countBefore = driver.findElements(howManyTargetSelected).size();

        addTrackToSecondaryGoal(pageTracking,pageTrackinAccountLevel);
        assertElementDisplayed(secondaryGoal, "The tracker did not move to Secondary Goals.");

        int countAfterAdditionOneElement = driver.findElements(howManyTargetSelected).size();
        Assert.assertEquals(countAfterAdditionOneElement, countBefore + 1, "The number of elements did not increase by 1 as expected");

        addTrackToSecondaryGoal(customTracking,customTracker);
        addTrackToSecondaryGoal(browsing,bounceTracker,pagesTracker,revisitTracker);

        int countAfterAdditionAllElements = driver.findElements(howManyTargetSelected).size();
        Assert.assertEquals(countAfterAdditionAllElements, countBefore + 5, "The number of elements did not increase as expected");

        deleteAndAssertElementNotPresent(deletePrimaryGoal);
        assertOneLessElementAfterClick(deleteSecondaryGoal);
        return this;
    }


    public ObjectLibrary selectTargetAllVisitors() {

        clickButtonWithWait(targetTab);
       // handleAlert();
        clickButtonWithWait(audiencesDropdown);
        handleStaleElementThenClick(allVisitorsSegment);
        assertElementDisplayed(allVisitorSelected, "'All visitors' are not selected");

        //tryToClickButton(beamerAnnouncementSnippetCloseButton);

        clickButtonWithWait(saveStepButton);
        ifWindowPopUps(leaveButtonInModal);

        return this;
    }

    @SneakyThrows
    public ObjectLibrary createSegmentGeolocation(String countryName, String regionName, String subRegionName, String cityName) {

        ifWindowPopUps(leaveButtonInModal);
        //changeTab();
        clickButtonWithWait(targetTab);
        //handleAlert();
        clickButton(audiencesDropdown);
        clickButtonWithWait(createNewSegment);
        assertElementDisplayed(segmentBuilderTitle, "You are not on a Segment Builder page");
        clickButton(geographicList);
        waitVisibilityOfElementLocated(geolocationSegment);
        waitVisibilityOfElementLocated(segmentDropZone);
        dragAndDropUsingJS(geolocationSegment, segmentDropZone);

        sendKeysIfElementVisible(geolocationSelectACountry, countryName);
        sendKeysIfElementVisible(geolocationSelectARegion, regionName);
        sendKeysIfElementVisible(geolocationSelectASubRegion, subRegionName);
        sendKeysIfElementVisible(geolocationSelectACity, cityName);


        clickButton(audienceNameChangeButton);
        webElement(audienceNameChangeField).sendKeys("Audience city: " + cityName);

      //  tryToClickButton(beamerAnnouncementSnippetCloseButton);
        clickButton(geolocationSelectACountry);
        Thread.sleep(2000);
        jsClick(webElement(saveAudience));

        tryToClickButton(createAudienceAnyway);
        assertElementDisplayed(campaignTargetingH1,"You didn't create a new segment");
        return this;
    }

    public ObjectLibrary allocateTrafic(String percent) {
       // changeTab();
        clickButtonWithWait(trafficTab);
        // handleAlert();
        WebElement trafficAllocate = driver.findElement(trafficAllocateInput);
        trafficAllocate.clear();
        trafficAllocate.sendKeys(percent);
        clickButtonWithWait(saveStepButton);
        ifWindowPopUps(changeAllocationButton);

        if (driver.getPageSource().contains("You're changing Traffic")) {
            clickButtonWithWait(changeAllocationButton);
        }
        String value = trafficAllocate.getAttribute("value");
        Assert.assertEquals(percent, value);
        return this;
    }












    public static By id() {
        by = By.xpath("//*[@id='email']");
        return by;
    }

    public static By password() {
        by = By.xpath("//*[@id=\"password\"]");
        return by;
    }

    public static By signIn() {
        by = By.xpath("//*[text()=\"Sign in\"]");
        return by;
    }

    public static By accountNames() {
        by = By.xpath("//div[@role=\"combobox\"]");
        return by;
    }

    public static By searchAccName() {
        by = By.xpath("//input[@id=\"downshift-0-input\"]");
        return by;
    }

    public static By authWelcome() {
        return By.xpath("//div[contains(@class,'UserNameAndRole__welcome')]");
    }

    public static By createCampaignButton() {
        by = By.xpath("//div[@data-testid='buttonAction-container']");
        return by;
    }

    public static By createABTestButton() {
        by = By.xpath("//*[text()='Compare different versions of the same page on your website']");
        return by;
    }

    public static By urlInMainInformation() {
        by = By.xpath("//input[@name='url']");
        return by;

    }

    public static By allTestsNameField() {
        return By.xpath("//input[@name='name']");
    }

    public static By sourcecode() {
        by = By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div/div[4]/div/div/div/div[2]/div");
        return by;
    }

    public static By saveAndGoButton() {
        by = By.xpath("//button[contains(text(),'Save & Go to next step')]");
        return by;
    }

    public static By perssavetest() {
        by = By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div[2]/div[2]/button");
        return by;
    }

    public static By createMPTestButton() {
        by = By.xpath("//*[text()='Test all the pages of a conversion funnel on your site']");
        return by;
    }

    public static By firstMPTestNameField1() {
        by = By.xpath("(//input[@name='name'])[2]");
        return by;
    }

    public static By secondMPTestNameField2() {
        by = By.xpath("(//input[@name='name'])[3]");
        return by;
    }

    public static By secondUrlInMainInformation2() {
        by = By.xpath("(//input[@name='url'])[2]");
        return by;
    }

    public static By goalsTab() {
        by = By.xpath("//a[contains(@href,'/reporting-metrics')]");
        return by;
    }

    public static By targetTab() {
        by = By.xpath("//*[contains(text(),\"Targeting\")]");
        return by;
    }

    public static By actionTrackingsMousedown() {
        by = By.xpath("//*[text()='mousedown']");
        return by;
    }

    public static By pagetracking() {
        by = By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]");
        return by;
    }

    public static By customTracking() {
        by = By.xpath("//*[contains(text(),'Custom Tracking')]");
        return by;
    }

    public static By browsingMetrics() {
        by = By.xpath("//*[contains(text(),'Browsing indicators')]");
        return by;
    }

    public static By bounceRate() {
        by = By.xpath("//*[contains(text(),'Bounce rate')]");
        return by;
    }

    public static By pagesPerSession() {
        by = By.xpath("//*[contains(text(),'Pages per session')]");
        return by;
    }

    public static By revisitRate() {
        by = By.xpath("//*[contains(text(),'Revisit rate')]");
        return by;
    }

    public static By transactionGoal() {
        by = By.xpath("(//*[contains(@class,'Metric__item___EfQ1s')])[5]");
        return by;
    }

    public static By searchTestField() {
        by = By.xpath("(//input[@type='search'])[1]");
        return by;
    }

    public static By testidNEEDTOCHECK() {
        by = By.xpath("//*[@id=\"headerToolBarSearchInput\"]");
        return by;
    }

    public static By editCampaignPencilIcon() {
        by = By.xpath("//*[@data-testid='edit-cell']");
        return by;
    }

    public static By visualEditorInSteps() {
        by = By.xpath("//a[contains(@href,'/editor')]");
        return by;
    }

    public static By goalsInSteps() {
        by = By.xpath("//a[contains(@href,'/reporting-metr')]");
        return by;
    }

    public static By variationeditor() {
        by = By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div[1]/ul/li[2]/a/p");
        return by;
    }

    public static By targetingInSteps() {
        by = By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div[1]/ul/li[4]/a/p");
        return by;
    }

    public static By whocampaign() {
        by = By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div[2]/div/div[1]/div[3]/div[3]/div[1]/div/div[1]/p");
        return by;
    }

    public static By MPtrigger() {
        by = By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div[2]/div/div[1]/div[3]/div[3]/div[1]/div/div[1]/p");
        return by;
    }

    public static By Audience() {
        by = By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div/div/div/div/div/span");
        return by;
    }

    public static By MPtriggerAudience() {
        by = By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div[2]/div/div[1]/div[3]/div[3]/div[2]/div/div/div/div/div/div/div/div/div/div/div[1]/span");
        return by;
    }

    public static By traffic() {
        by = By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div[1]/ul/li[5]/a/p");
        return by;
    }

    public static By varpercent() {
        by = By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[2]/div[2]/div/label/input");
        return by;
    }

    public static By persvarpercent() {
        by = By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div[2]/div[1]/div[2]/div[2]/div[1]/div/div[1]/div[2]/div[2]/label/input");
        return by;
    }

    public static By savetraffic() {
        by = By.xpath("//*[contains(text(),\"Save step\")]");
        return by;
    }

    public static By Elementtobechanged() {
        by = By.xpath("//*[@id=\"main\"]/div[1]/h1");
        return by;
    }

    public static By Edit() {
        by = By.xpath("//*[@id=\"ab-embed-editor__context-menu\"]/li[3]/a");
        return by;
    }

    public static By Editstyle() {
        by = By.xpath("//a[contains(text(),'Edit style')]");
        return by;
    }

    public static By colourtab() {
        by = By.xpath("//*[@id=\"modalTabs\"]/li[3]/a");
        return by;
    }

    public static By greencolor() {
        by = By.xpath("//*[@id=\"bgColor\"]");
        return by;
    }

    public static By savechanges() {
        by = By.xpath("//*[@id=\"ab-embed-editor__editStyle__save\"]");
        return by;
    }

    public static By playtest() {
        by = By.xpath("//*[contains(text(), \"Paused\")]");
        return by;
    }

    public static By editorplay() {
        by = By.xpath("//*[contains(@class, \"pause\")]");
        return by;
    }

    public static By pausetest() {
        by = By.xpath("//*[contains(text(), \"Live\")]");
        return by;
    }

    public static By Elementtobetracked() {
        by = By.xpath("//*[@id=\"main\"]/div[3]/a/h1");
        return by;
    }

    public static By actiontracking() {
        by = By.xpath("//*[text()=\"Add a tracker\"]");
        return by;
    }

    public static By actionname() {
        by = By.xpath("//*[@id=\"ab-embed-editor__modal__edit-action-tracking__input-name\"]");
        return by;
    }

    public static By saveaction() {
        by = By.xpath("//*[@id=\"ab-embed-editor__modal__edit-action-tracking-name\"]/div/div/div[2]/div[3]/button[2]");
        return by;
    }

    public static By nudge() {
        by = By.xpath("//*[text()=\"Add Widgets\"]");
        return by;
    }

    public static By component() {
        by = By.xpath("//button[@data-content=\"ab-embed-editor__page-sidebar__content-components\"]");
        return by;
    }

    public static By searchcomp() {
        by = By.xpath("//input[@class=\"componentsgallery_search\"]");
        return by;
    }

    public static By comp() {
        by = By.xpath("//*[@id=\"ab-embed-editor__modal__pluginsLibrary\"]/div/div/div/section/div/div[3]/div[1]/div[1]/footer/h3");
        return by;
    }

    public static By addcomponent() {
        by = By.xpath("//*[text()=\"Add component template\"]");
        return by;
    }

    public static By savecomponent() {
        by = By.xpath("//*[text()=\"Save\"]");
        return by;
    }

    public static By searchnudge() {
        by = By.xpath("//input[@class=\"pluginslibrary_search\"]");
//by = By.xpath("//*[@id=\"ab-embed-editor__modal__pluginsLibrary\"]/div/div/div/section/div/div[1]/input");
        return by;
    }

    public static By selectwidget() {
        by = By.xpath("//*[@id=\"ab-embed-editor__modal__pluginsLibrary\"]/div/div/div/section/div/div[3]/div/div[1]/footer/h3");
        return by;
    }

    public static By addwidget() {
        by = By.xpath("//button[text()=\"Add\"]");
        return by;
    }

    public static By NPS() {
        by = By.xpath("//*[text()=\"NPS\"]");
        return by;
    }

    public static By countdown() {
        by = By.xpath("//*[text()=\"Countdown\"]");
        return by;
    }

    public static By simplepopin() {
        by = By.xpath("//*[text()=\"Simple Pop-in\"]");
        return by;
    }

    public static By imagepopin() {
        by = By.xpath("//*[text()=\"Image Pop-in\"]");
        return by;
    }

    public static By videopopin() {
        by = By.xpath("//*[text()=\"Video Pop-in\"]");
        return by;
    }

    public static By tooltip() {
        by = By.xpath("//*[text()=\"Tooltip\"]");
        return by;
    }

    public static By snowflake() {
        by = By.xpath("//*[text()=\"Snowflake Animation\"]");
        return by;
    }

    public static By saveNPS() {
        by = By.xpath("//button[text()=\"Save\"]");
        return by;
    }

    public static By Showeditor() {
        by = By.xpath("//*[@id=\"arrowRight\"]");
        return by;
    }

    public static By campaign() {
        by = By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/nav/div[2]/div[1]/div[1]/a");
        return by;
    }

    public static By personalisation() {
        by = By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/nav/div[2]/div[1]/div[1]/div/a[2]/div[2]/p[1]");
        return by;
    }

    public static By persURL() {
        by = By.xpath("//*[@id=\"url0\"]");
        return by;
    }

    public static By perstestname() {
        by = By.xpath("//*[@id=\"name\"]");
        return by;
    }

    public static By persname() {
        by = By.xpath("//*[@id=\"name0\"]");
        return by;
    }

    public static By perssourcecode() {
        by = By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div[2]/div[1]/div[5]/div/div/div[3]/div/div/div[2]/div");
        return by;
    }

    public static By persvareditor() {
        by = By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div[1]/ul/li[3]/a/p");
        return by;
    }

    public static By persgoaltab() {
        by = By.xpath("//*[@id=\"ab-embed-editor__menu-steps\"]/div[2]/div[1]/ul/li[4]/a/p");
        return by;
    }

    public static By perstesttarget() {
        by = By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div[1]/ul/li[2]/a/p");
        return by;
    }

    public static By saveTarget() {
        by = By.xpath("//*[contains(@data-testid,'experimentBottomBar')]//button[2]");
        return by;
    }

    public static By perstraffic() {
        by = By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div[1]/ul/li[5]/a/p");
        return by;
    }

    public static By testoptions() {
        by = By.xpath("/html/body/ui-view[1]/div[1]/div/div/div/div/div/div/section/div/test-list/div/div[1]/div/div/div[5]/div/a[2]/i");
        return by;
    }

    public static By archive() {
        by = By.xpath("/html/body/div[8]/div[2]/div/div/ul/li[3]/a/div/p");
        return by;
    }

    public static By dashboard() {
        by = By.xpath("/html/body/ui-view[1]/div[1]/div/div/div/div/div/div/section/header-tool-bar/section/div[2]/div[2]/i[2]");
        return by;
    }

    public static By duplicate() {
        by = By.xpath("/html/body/div[8]/div[2]/div/div/ul/li[2]/a/div/p");
        return by;
    }

    public static By duplicateAccount() {
        by = By.xpath("/html/body/div[8]/div[2]/div/div/ul/li[2]/a/div/div/div[2]/div/div/ui-select-duplicate-test/div/div/div[1]/input");
        return by;
    }

    public static By Targetfilter() {
        by = By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div[2]/div/div[1]/div[3]/div[3]/div[2]/div/div/div/div[2]/div[2]/div/div/span[1]");
        return by;
    }

    public static By Targetselect() {
        by = By.xpath("/html/body/div[12]/div/div/div/div[2]/div[1]/div[2]/div/div/label/input");
        return by;
    }

    public static By Targetdeletehover() {
        by = By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div[1]/div[1]/div/div[1]/div[2]");
        return by;
    }

    public static By Targetdelete() {
        by = By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div[1]/div[1]/div/div[1]/div[2]/span");
        return by;
    }

    public static By Tagupdate() {
        by = By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/nav/div[2]/div[3]/div/div/button");
        return by;
    }

    public static By WidgetContent() {
        by = By.xpath("//*[contains(@name,\"content\")]");
        return by;
    }

    public static By WidgetLayout() {
        by = By.xpath("//*[contains(@name,\"layout\")]");
        return by;
    }

    public static By widgetsStyle() {
        by = By.xpath("//*[contains(@name,\"style\")]");
        return by;
    }

    public static By widgetCondition() {
        by = By.xpath("//*[contains(@name,\"conditions\")]");
        return by;
    }

    public static By Widgetplay() {
        by = By.xpath("//*[@id=\"menu-playpause-test\"]");
        return by;
    }

    public static By sidebar() {
        by = By.xpath("//*[text()=\"Active changes\"]");
//*[@id="ab-embed-editor__page-sidebar"]/div[3]/div/button[1]
//*[@id="ab-embed-editor__page-sidebar"]/div[3]/div/button[2]
        return by;
    }

    public static By submenu() {
        by = By.xpath("//*[@id=\"ab-embed-editor__page-sidebar__content-history\"]/ul/li[2]/div/div/span/i");
        return by;
    }

    public static By menudelete() {
        by = By.xpath("//*[@id=\"ab-embed-editor__page-sidebar__content-history\"]/ul/li[2]/div/div/div/span");
        return by;
    }

    public static By trigger() {
        by = By.xpath("//*[text()=\"How the test will be triggered\"]");
        return by;
    }

    public static By wheresection() {
        by = By.xpath("//*[contains(text(),\"Where the test will be displayed\")]");
        return by;
    }

    public static By whosection() {
        by = By.xpath("//*[contains(text(),\"Who will see the test\")]");
        return by;
    }

    public static By MPwheresection() {
        by = By.xpath("//*[contains(text(),\"Where the subtest will be displayed\")]");
        return by;
    }

    public static By triggeraudience() {
        by = By.xpath("//*[@class=\"ValueContainer__container___1skfC\"]");
        return by;
    }

    public static By perstrigger() {
        by = By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div[2]/div/div[1]/div[3]/div[3]/div[1]/div/div[1]/p");
        return by;
    }

    public static By perstriggeraudience() {
        by = By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div[2]/div/div[1]/div[3]/div[3]/div[2]/div/div/div/div[1]/div/div/div/div/div/div/div[2]/span[2]");
        return by;
    }

    public static By report() {
        by = By.xpath("//*[text()=\"Reporting\"]");
        return by;
    }

    public static By realtimereport() {
        by = By.xpath("//*[@class=\"reportingHeaderTop\"]");
        return by;
    }

    public static By arrowBackToDashbaord() {
        by = By.xpath("//a[@href='/experiments']");
        return by;
    }

    public static By allCampaignsNameOnTheDashboard() {
        by = By.cssSelector("[data-testid*='row-test-'] > :first-child > :nth-child(2) > :first-child > :first-child > :first-child > :first-child > :first-child");
        return by;
    }

    public static By deleteCampaignFromDashboard() {
        by = By.xpath("//*[text()='Delete']");
        return by;
    }

    public static By confirmDeleteCampaignOnModal() {
        by = By.cssSelector("[data-testid='testDeleteModal'] > :nth-child(3) > :nth-child(2)");
        return by;
    }

//*
//Dashboard
//*

    public static By onboardingModalCloseIcon() {
        by = By.xpath("//*[@data-testid=\\\"onboardingModalCloseIcon\\\"]");
        return by;
    }

    public static By Personalization() {
        by = By.xpath("//div[contains(text(), 'Personalization')]");
        return by;
    }

    public static By Experimentation() {
        by = By.xpath("//div[contains(text(), 'Experimentation')]");
        return by;
    }

    public static By searchDashboard() {
        by = By.xpath("//input[@placeholder='Search...']");
        return by;
    }

//Filter

    public static By newFilter() {
        by = By.xpath("//button[contains(text(),'Filter')]");
        return by;
    }

    public static By liveFilter() {
        by = By.xpath("//span[normalize-space()='Live']");
        return by;
    }

    public static By pauseFilter() {
        by = By.xpath("//span[normalize-space()='Paused']");
        return by;
    }

    public static By AbTestFilter() {
        by = By.xpath("//span[normalize-space()='A/B Test']");
        return by;
    }

    public static By multiPageTestFilter() {
        by = By.xpath("//span[normalize-space()='Multipage test']");
        return by;
    }

    public static By multiVariateTestFilter() {
        by = By.xpath("//span[normalize-space()='Multivariate test']");
        return by;
    }

    public static By patchTestFilter() {
        by = By.xpath("//span[normalize-space()='Patch']");
        return by;
    }

    public static By dynamicABTestFilter() {
        by = By.xpath("//span[normalize-space()='Dynamic AB Test']");
        return by;
    }

    public static By simplePersoFilter() {
        by = By.xpath("//span[normalize-space()='Simple personalization']");
        return by;
    }

    public static By multiPagePersoFilter() {
        by = By.xpath("//span[normalize-space()='Multipage personalization']");   //
        return by;
    }

    public static By multiExperiencePersoFilter() {
        by = By.xpath("//span[normalize-space()='Multiexperience personalization']");
        return by;
    }


    public static By multipagePersonalization() {
        by = By.xpath("//span[normalize-space()='Multipage personalization']");
        return by;
    }


    public static By applyFilter() {
        by = By.xpath("//button[normalize-space()='Apply']");
        return by;
    }

    public static By resetFilter() {
        by = By.xpath("//button[normalize-space()='Reset']");
        return by;
    }

    public static By moreActions() {
        by = By.xpath("//*[@data-testid=\"moreActions\"]");
//by = By.xpath("//*[contains(@data-testid, \"row-test\")][1]//*[@data-testid=\"moreActions\"]");
//*[contains(@data-testid, \"row-test\")][1]//*[@data-testid=\"moreActions\"]
        return by;
    }

    public static By archiveDashboard() {
        by = By.xpath("//div[contains(text(),'Archive')]");
        return by;
    }

    public static By archiveButton() {
        by = By.xpath("//div[@data-testid = 'Dialog']//button[contains(text(),'Archive')]");
        return by;
    }

    public static By duplicateCampaign() {
        by = By.xpath("//*[(text()='Duplicate')]");
        return by;
    }

    public static By cancelButton() {
        by = By.xpath("//button[contains(text(),'Cancel')]");
        return by;
    }

    public static By duplicateButton() {
        by = By.xpath("//button[2][. = 'Duplicate']");
        return by;
    }

    public static By targetevents() {
        by = By.xpath("//label[@data-testid=\"targetPerEvents\"]");
        return by;
    }


}
