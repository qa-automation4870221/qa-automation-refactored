package core;

//import com.relevantcodes.extentreports.ExtentReports;
//import com.relevantcodes.extentreports.ExtentTest;
import lombok.SneakyThrows;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import pageObject.ObjectLibrary;

import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.File;
import java.io.IOException;

public abstract class BaseTest {
    protected ChromeDriver driver;
    protected WebDriverWait waiter;

 /*   protected ExtentTest test;
    protected ExtentReports report;*/
    protected ObjectLibrary objectLibrary;

    @SneakyThrows
    @BeforeSuite
    public void startTest() {
        try {
            Files.walk(Paths.get("allure-results"))
                    .filter(Files::isRegularFile)
                    .map(Path::toFile)
                    .forEach(File::delete);
        } catch (IOException e) {
            e.printStackTrace();
        }
//        report = new ExtentReports(System.getProperty("user.dir") + "/test-output/ExtentReportResults.html");
//        test = report.startTest("ExtentDemo");
        driver = DriverInitializer.getInstance().getDriver();
        waiter = DriverInitializer.getInstance().getWaiter();
        objectLibrary = new ObjectLibrary();
    }

    @AfterSuite
    public void closedriver() {
        /*report.endTest(test);
        report.flush();*/
        driver.quit();
    }

    @AfterMethod
    public void close(ITestResult result){
        if(driver!= null){
            if (result.getStatus() == ITestResult.FAILURE) {
                AttachScreenshots.screenshotAs("page source png");
                AttachScreenshots.pageSource();
            }
        }
    }
}
