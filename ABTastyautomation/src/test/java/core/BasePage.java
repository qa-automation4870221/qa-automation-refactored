package core;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BasePage {
    protected ChromeDriver driver;
    protected WebDriverWait wait;

    protected BasePage() {
        this.driver = DriverInitializer.getInstance().getDriver();
        this.wait = DriverInitializer.getInstance().getWaiter();
    }

    public boolean isElementExist(By element) {
        return driver.findElements(element).size() > 0;
    }

    public WebElement waitElemetIsVisible(WebElement element) {
        wait.until(ExpectedConditions.visibilityOf(element));
        return element;
    }

    public void assertElementDisplayed(String locator, String errorMessage) {
        WebElement path = driver.findElement(By.xpath(locator));
        Assert.assertTrue(path.isDisplayed(), errorMessage);
    }

    public void assertElementDisplayed(WebElement element, String errorMessage) {
        Assert.assertTrue(element.isDisplayed(), errorMessage);
    }

    public void assertElementDisplayed(By locator, String errorMessage) {
        WebElement path = waitVisibilityOfElementLocated(locator);
        Assert.assertTrue(path.isDisplayed(), errorMessage);
    }

    public void assertValueEquality(By locator, String expectedValue) {
        String actualValue = driver.findElement(locator).getText();
        Assert.assertEquals(actualValue, expectedValue);
    }

    public void clickButtonWithWait(By locator) {
        wait.until(ExpectedConditions.elementToBeClickable(locator)).click();
    }

    public void clickButton(By locator) {
        driver.findElement(locator).click();
    }
    public void jsClick(WebElement element) {
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click();", element);
    }

  /*  public void clickButtonSeveralTimes(By button, int maxAttempts) {

        int attempts = 0;
        while(attempts < maxAttempts) {
            try {
                clickButtonWithWait(button);
                break;
            } catch (Exception e) {
                System.out.println("Attempt to click button failed. Trying again.");
                attempts++;
            }
        }
    }*/
    public WebElement webElement(By locator) {
        WebElement element = driver.findElement(locator);
        return element;
    }

    public void deleteAndAssertElementNotPresent(By locator) {
        driver.findElement(locator).click();
        List<WebElement> elements = driver.findElements(locator);
        Assert.assertTrue(elements.isEmpty(), "The element should not exist on the page");
    }

    public void handleAlert() {

        int attempts = 0;
        while(attempts < 2) {
            try {
                WebDriverWait wait2 = new WebDriverWait(driver,Duration.ofSeconds(5));
                wait2.until(ExpectedConditions.alertIsPresent());
                Alert alert = driver.switchTo().alert();
                System.out.println("Alert detected: " + alert.getText());
                alert.accept();
                break;
            } catch (NoAlertPresentException e) {
                // Alert not present
                System.out.println("No alert is present.");
            } catch (UnhandledAlertException e) {
                // Alert appeared while we were interacting with the page
                System.out.println("Alert appeared while interacting with the page. Attempting to accept again.");
            } catch (TimeoutException e) {
                System.out.println("No alert appeared within the expected time.");
            }
            attempts++;
        }
    }


    public void assertOneLessElementAfterClick(By locator) {
        //for checking how many elements before deleting
        List<WebElement> elementsBeforeClick = driver.findElements(locator);
        int countBeforeClick = elementsBeforeClick.size();
        driver.findElement(locator).click();
        //for checking how many elements after deleting
        List<WebElement> elementsAfterClick = driver.findElements(locator);
        int countAfterClick = elementsAfterClick.size();
        //Assert that one element less
        Assert.assertEquals(countAfterClick, countBeforeClick - 1, "There should be one less element after the click");
    }

    public void hoverAndClick(WebDriver driver, By elementToHover) {
        new Actions(driver).moveToElement(waitVisibilityOfElementLocated(elementToHover)).click().perform();
    }

    public void hoverToElement(WebDriver driver, By elementToHover) {
        new Actions(driver).moveToElement(waitVisibilityOfElementLocated(elementToHover)).perform();
    }

    public WebElement waitVisibilityOfElementLocated(By element) {
        return wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.visibilityOfElementLocated(element));
    }

    public void handleStaleElementThenClick(By locator) {
        int attempts = 0;
        while (attempts < 2) {
            try {
                WebDriverWait wait1 = new WebDriverWait(driver, Duration.ofSeconds(7));
                WebElement element = wait1.until(ExpectedConditions.elementToBeClickable(locator));
                element.click();
                break;
            } catch (StaleElementReferenceException e) {
            }
            attempts++;
        }
    }

    public void ifWindowPopUps(By locator) {
        try {
            WebDriverWait wait1 = new WebDriverWait(driver, Duration.ofSeconds(7));
            WebElement element = wait1.until(ExpectedConditions.presenceOfElementLocated(locator));
            if(element.isEnabled()){
                element.click();
            }
        } catch (TimeoutException e) {
            System.out.println("Modal window did not appear, continue execution.");
        }
    }

    public void dragAndDropUsingJS(By drag, By drop) {
        WebElement from = driver.findElement(drag);
        WebElement to = driver.findElement(drop);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("function createEvent(typeOfEvent) {\n" +
                "var event = document.createEvent(\"CustomEvent\");\n" +
                "event.initCustomEvent(typeOfEvent, true, true, null);\n" +
                "event.dataTransfer = {\n" +
                "data: {},\n" +
                "setData: function(type, val){\n" +
                "this.data[type] = val;\n" +
                "},\n" +
                "getData: function(type){\n" +
                "return this.data[type];\n" +
                "}\n" +
                "};\n" +
                "return event;\n" +
                "}\n" +
                "\n" +
                "function dispatchEvent(element, event, transferData) {\n" +
                "if (transferData !== undefined) {\n" +
                "event.dataTransfer = transferData;\n" +
                "}\n" +
                "if (element.dispatchEvent) {\n" +
                "element.dispatchEvent(event);\n" +
                "} else if (element.fireEvent) {\n" +
                "element.fireEvent(\"on\" + event.type, event);\n" +
                "}\n" +
                "}\n" +
                "\n" +
                "function simulateHTML5DragAndDrop(element, target) {\n" +
                "var dragStartEvent = createEvent('dragstart');\n" +
                "dispatchEvent(element, dragStartEvent);\n" +
                "var dropEvent = createEvent('drop');\n" +
                "dispatchEvent(target, dropEvent, dragStartEvent.dataTransfer);\n" +
                "var dragEndEvent = createEvent('dragend');\n" +
                "dispatchEvent(element, dragEndEvent, dropEvent.dataTransfer);\n" +
                "}\n" +
                "\n" +
                "var source = arguments[0];\n" +
                "var destination = arguments[1];\n" +
                "simulateHTML5DragAndDrop(source, destination);", from, to);
    }

    public void sendKeysIfElementVisible(By locator, String location) {
        WebDriverWait wait1 = new WebDriverWait(driver, Duration.ofSeconds(7));
        WebElement element = wait1.until(ExpectedConditions.elementToBeClickable(locator));
        Optional.ofNullable(element)
                .filter(WebElement::isDisplayed)
                .ifPresent(e -> e.sendKeys(location, Keys.ENTER));
    }

    public void changeTab() {
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(tabs.size() - 1));
    }
    public void tryToClickButton(By locator) {

        try {
            WebDriverWait wait1 = new WebDriverWait(driver, Duration.ofSeconds(7));
            WebElement closeBtn = wait1.until(ExpectedConditions.visibilityOfElementLocated(locator));
            closeBtn.click();
            System.out.println( locator + " button clicked.");
        } catch (TimeoutException e) {
            System.out.println(locator + " button not found, continue.");
        }
    }


}
