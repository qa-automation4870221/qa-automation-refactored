package core;
import io.qameta.allure.Attachment;
import java.nio.charset.StandardCharsets;
import org.openqa.selenium.OutputType;

public class AttachScreenshots {
    public AttachScreenshots() {
    }

    @Attachment(
            value = "{attachName}",
            type = "text/plain"
    )
    public static String attachAsText(String attachName, String message) {
        return message;
    }

    @Attachment(
            value = "Page source",
            type = "text/plain"
    )
    public static byte[] pageSource() {
        return DriverInitializer.getInstance().getDriver().getPageSource().getBytes(StandardCharsets.UTF_8);
    }

    @Attachment(
            value = "{attachName}",
            type = "image/png"
    )
    public static byte[] screenshotAs(String attachName) {
        return (byte[]) DriverInitializer.getInstance().getDriver().getScreenshotAs(OutputType.BYTES);
    }
}
