package core;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;
import java.util.Collections;

public class DriverInitializer {
    private static DriverInitializer instance;
    private ChromeDriver driver;
    private WebDriverWait waiter;
    private String appUrl;

    public String getAppUrl() {
        return appUrl;
    }

    public WebDriverWait getWaiter() {
        return waiter;
    }

    public ChromeDriver getDriver() {
        return driver;
    }
    private DriverInitializer(){
        driver = createDriver();
    }

    public static DriverInitializer getInstance(){
        if(instance == null) {
            instance = new DriverInitializer();
        }
        return instance;
    }

    private ChromeDriver createDriver(){
     //   ChromeDriver driver = null;
        WebDriverManager.chromedriver().setup();
        driver = initDriver();
        waiter = new WebDriverWait(driver, Duration.ofSeconds(20));
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(20));
        driver.manage().window().maximize();
        return driver;
    }

     private ChromeDriver initDriver(){
        String testEnv = System.getenv("ENV");
        if (testEnv == null || testEnv.isEmpty()) {
            testEnv = "staging";
        }
        if (testEnv.equalsIgnoreCase("staging")) {;
            ChromeOptions options = new ChromeOptions();
            options.setExperimentalOption("useAutomationExtension", false);
            options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
            options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR,UnexpectedAlertBehaviour.IGNORE);
            // options.addArguments("headless");
            options.addArguments("--remote-allow-origins=*");
            options.addArguments("--window-size=1920,1080");
            options.addArguments("--no-sandbox");
            options.addArguments("--start-maximized");
            options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
            options.addArguments("--disable-dev-shm-usage");
            appUrl = "https://staging-app2.abtasty.com/";
            driver = new ChromeDriver(options);
            driver.get(appUrl);
        } else {
            ChromeOptions options = new ChromeOptions();
            options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR,UnexpectedAlertBehaviour.IGNORE);
            options.addArguments("--remote-allow-origins=*");
            options.setExperimentalOption("useAutomationExtension", false);
            options.addArguments("--window-size=1920,1080");
            options.addArguments("--no-sandbox");
            options.addArguments("--start-maximized");
            options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
            options.addArguments("--disable-dev-shm-usage");
            appUrl = "https://app2.abtasty.com/experiments";
            driver = new ChromeDriver(options);
            driver.get(appUrl);
            System.out.println("prod");
            System.out.println("login " + driver.getCurrentUrl());
        }
        return driver;
    }

}
