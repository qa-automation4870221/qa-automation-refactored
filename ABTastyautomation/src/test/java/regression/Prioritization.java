package regression;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import pageObject.ObjectLibrary;

import java.awt.Robot;
import java.io.IOException;
import java.util.Collections;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;

//*************************************************************************************************************
//*	                                Prioritization			                  						          *
//*************************************************************************************************************

public class Prioritization {

	static WebDriver driver;
	private By by;
	static String parentWindow;
	public static String ReportURL;
	static String variationid;
	static String Customerid;
	static String testrowno;
	static String typeoftest;
	static String testenvironment;
	public static String browserName;
	public static String os;
	//static String chromedriverpath = "/usr/local/bin/chromedriver";
	static String chromedriverpath = "C:\\abTastySelenium\\drivers\\chromedriver.exe";
	static String browserpath = "--user-data-dir=/Users/monikapanda/Documents/Selenium/BrowserProfile/Profilenew";
	static String excelpath = System.getProperty("user.dir") + "/ABTastyData1.xlsx";

	@BeforeSuite
	public static void initialslackmsg() throws IOException {
		String testername = CommonLibrary.main_data_excel("tester", "Name");
		// EyeSlack.initialpost(testername ,
		// "https://hooks.slack.com/services/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");
	}

//*************************************************************************************************************
//*	                                Logging in into ABTasty                                                   *
//*************************************************************************************************************

	@Parameters({ "testenvironment" })
	@SuppressWarnings("deprecation")
	@Test(priority = 0)
	public static void Login(String testenv) throws InterruptedException, IOException {

		System.out.println("Login started");

		System.setProperty("webdriver.chrome.driver", chromedriverpath);

		// To check whether to test in staging or production

		testrowno = "1st";
		testenvironment = testenv;

		// String testtype = CommonLibrary.input_data_excel(testrowno, "typeofenv");
		if (testenv.equalsIgnoreCase("staging")) {
			// Initialize browser
			ChromeOptions options = new ChromeOptions();
			// options.addArguments("-incognito");
			options.setExperimentalOption("useAutomationExtension", false);
			options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));

			// to start chrome in headless mode
			options.addArguments("headless");
			options.addArguments("--window-size=1920,1080");
			options.addArguments("--no-sandbox");
			options.addArguments("--disable-dev-shm-usage");

			// DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			// capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			// driver=new ChromeDriver(capabilities);
			// options.addArguments(browserpath);
			System.out.println("staging");

			// Open the editor
			driver = new ChromeDriver(options);
			driver.get("https://staging-app2.abtasty.com/experiments");
			System.out.println("login " + driver.getCurrentUrl());
			if (driver.getPageSource().contains("403 ERROR")) {
				System.out.println("403 error on staging");
			}
		} else {
			// closing the extra line at top
			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("useAutomationExtension", false);
			options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
			// to start chrome in headless mode
			options.addArguments("headless");
			options.addArguments("--window-size=1920,1080");
			options.addArguments("--no-sandbox");
			options.addArguments("--disable-dev-shm-usage");

			driver = new ChromeDriver(options);
			driver.get("https://app2.abtasty.com/experiments");
			System.out.println("prod");
			System.out.println("login " + driver.getCurrentUrl());
		}
		// deriving the browser and os
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		browserName = caps.getBrowserName();
		String browserVersion = caps.getVersion();

		// Get OS name.
		os = System.getProperty("os.name").toLowerCase();
		System.out.println("browserName" + browserName + "browserVersion" + browserVersion + "os" + os);

		// login to the editor
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.id()).click();
		driver.findElement(ObjectLibrary.id()).sendKeys(CommonLibrary.main_data_excel("tester", "userid"));
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.password()).click();
		driver.findElement(ObjectLibrary.password()).sendKeys(CommonLibrary.main_data_excel("tester", "password"));
		driver.findElement(ObjectLibrary.signIn()).click();
		driver.manage().window().maximize();

		System.out.println("Login successfully done");
	}

//*************************************************************************************************************
//*	                                Changing account names                                                    *
//*************************************************************************************************************

	@Test(priority = 1)
	public static void selectAccount() throws Exception {

		System.out.println("select account names started");

		// Locate 'Account Names' drop down
		Thread.sleep(4000);
		if (driver.getPageSource().contains("Welcome to the new navigation experience!")) {
			driver.findElement(By.xpath("//*[@data-testid=\"onboardingModalCloseIcon\"]")).click();
		}
		// change account
		CommonLibrary.ChangeAccount(driver, "Mathumitha Murugan");
		Thread.sleep(1000);

		System.out.println("Select account names successfully done");
	}

//*************************************************************************************************************
//*	                               Open Prioritization     	       	        				                  *
//*************************************************************************************************************

	@Test(priority = 2)
	public void openPrioritization() throws Exception {

		System.out.println("Open Prioritization started");

		// click perso
		driver.findElement(By.xpath("//div[contains(text(), 'Personalization')]")).click();
		Thread.sleep(1000);

		// click Prioritization
		driver.findElement(By.xpath("//a[normalize-space()='Prioritization']")).click();
		// click collapse
		driver.findElement(By.xpath("//button[@aria-label='Collapse']//*[name()='svg']")).click();
		// click Welcome
		driver.findElement(By.xpath("//h1[contains(text(),'Prioritization')]")).click();

		System.out.println("Open Prioritization successfully done");
	}

//*************************************************************************************************************
//*	                               Open Prioritization     	       	        				                  *
//*************************************************************************************************************

	@Test(priority = 3)
	public void PrioritizationOnboarding() throws Exception {

		System.out.println("Prioritization Onboarding started");

		// Click how button
		driver.findElement(By.xpath("//div[@data-testid = \"onboardingModalTrigger\"]")).click();
		// Check onboarding Modal
		boolean displayed = driver.findElement(By.xpath("//div[@data-testid = \"onboardingModalSteps\"]"))
				.isDisplayed();

		if (displayed) {
			System.out.println("onboardingModal displayed");
		}

		driver.findElement(By.xpath("//button[contains(text(),'Next')]")).click();
		driver.findElement(By.xpath("//button[contains(text(),'Next')]")).click();
		driver.findElement(By.xpath("//button[contains(text(),'Next')]")).click();
		driver.findElement(By.xpath("//button[contains(text(),'Next')]")).click();
		driver.findElement(By.xpath("//button[contains(text(),\"Ok let's start\")]")).click();

		System.out.println("Prioritization Onboarding successfully done");
	}

//*************************************************************************************************************
//*	                               Check default date selection 	       	     			                  *
//*************************************************************************************************************

	@Test(priority = 4)
	public void default3Months() throws Exception {

		System.out.println("Test for default date selection successfully done");

		String bgcolor = driver.findElement(By.xpath("//li[contains(text(),'3 months')]")).getCssValue("color");
		System.out.println(bgcolor);

		String[] hexValue = bgcolor.replace("rgba(", "").replace(")", "").split(",");
		System.out.println(hexValue);

		hexValue[0] = hexValue[0].trim();
		int hexValue1 = Integer.parseInt(hexValue[0]);

		hexValue[1] = hexValue[1].trim();
		int hexValue2 = Integer.parseInt(hexValue[1]);

		hexValue[2] = hexValue[2].trim();
		int hexValue3 = Integer.parseInt(hexValue[2]);

		String actualColor = String.format("#%02x%02x%02x", hexValue1, hexValue2, hexValue3);
		System.out.println(actualColor);
		Assert.assertTrue(actualColor.equals("#007f91"));

		System.out.println("3 months is selected by default");

		System.out.println("Test for default date selection successfully done");

	}
//*************************************************************************************************************
//*	                               Check DragDrop	       	     			            				      *
//*************************************************************************************************************

	@Test(priority = 5)
	public void dragDrop() throws Exception {

		System.out.println(" dragDrop Test started");

		// div[@data-testid='priorityDnDItem'])[2]
		// div[@data-rbd-droppable-id='priorities-1']
		WebElement from = driver.findElement(By.xpath("//div[@class='CampaignsList__container___k3JwD']/div[1]"));
		WebElement to = driver.findElement(By.xpath("//div[@data-rbd-droppable-id='priorities-1']"));
		Thread.sleep(1000);
		// Creating object of Actions class to build composite actions
		Actions builder = new Actions(driver);

		// moving the cursor to the desired location
		Point location = to.getLocation();
		int x = location.getX();
		int y = location.getY();
		System.out.println("x=" + x + "y=" + y);
		Robot robot = new Robot();
		Thread.sleep(2000);
		robot.mouseMove(x, y);

		// drag and drop
		builder.dragAndDrop(from, to).click().build().perform();

		boolean button = driver.findElement(By.xpath("//div[@data-testid='publishButtonWrapper']")).isEnabled();
		if (!button) {
			System.out.println("publish Button is disabled");
		}
		System.out.println(" dragDrop Test ended");
	}

//*************************************************************************************************************
//*	                                Closing the drive                                        	              *
//*************************************************************************************************************	

	@AfterSuite
	public void closedriver() throws IOException {
		driver.quit();
	}

}
