package regression;

import org.testng.annotations.Test;
import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.devtools.DevTools;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Parameters;

import pageObject.ObjectLibrary;

public class Tag_only_regression {
	
	static DevTools chromeDevTools;
	static ChromeDriver driver;
	static String parentWindow;
	static String expectedvalue;
	static String testrowno;
	static String testenvironment;
	static String message = "targetting is not properly applied";
	//static String chromedriverpath = "/usr/local/bin/chromedriver";
	static String chromedriverpath = "C:\\abTastySelenium\\drivers\\chromedriver.exe";

	//*************************************************************************************************************
	//*	                                Logging in into ABTasty                                                   *
	//*************************************************************************************************************	
	   
	@Parameters({"testenvironment"})
	@Test(priority = 0)
	public static void Login(String testenv) throws InterruptedException, IOException {
		
	System.out.println("Login started")	;	
		
	System.setProperty("webdriver.chrome.driver", chromedriverpath);

	//To check whether to test in staging or production

	testrowno = "1st";
	testenvironment = testenv;
		
	//String testtype = CommonLibrary.input_data_excel(testrowno, "typeofenv");
	if (testenv.equalsIgnoreCase("staging"))
	{
	//Initialize browser
		ChromeOptions options = new ChromeOptions();
//		options.addArguments("-incognito");
		options.setExperimentalOption("useAutomationExtension", false);
		options.setExperimentalOption("excludeSwitches",Collections.singletonList("enable-automation")); 
		//to start chrome in headless mode
//		options.addArguments("headless");
//		options.addArguments("--window-size=1920,1080");
//		options.addArguments("--no-sandbox");
//		options.addArguments("--start-maximized");
//		options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
//		options.addArguments("--disable-dev-shm-usage") ;
		
	//Open the editor
		driver = new ChromeDriver(options);
	    driver.get("https://staging-app2.abtasty.com/experiments");
		System.out.println("staging");
	}
	else
	{
		//closing the extra line at top
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("useAutomationExtension", false);
		options.setExperimentalOption("excludeSwitches",Collections.singletonList("enable-automation"));  
		//to start chrome in headless mode
//		options.addArguments("headless");
//		options.addArguments("--window-size=1920,1080");
//		options.addArguments("--no-sandbox");
//		options.addArguments("--start-maximized");
//		options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
//		options.addArguments("--disable-dev-shm-usage") ;
		
		driver = new ChromeDriver(options);
		driver.get("https://app2.abtasty.com/experiments");
		System.out.println("prod");
	}

	//login to the editor
	Thread.sleep(1000);
	driver.findElement(ObjectLibrary.id()).click();
	driver.findElement(ObjectLibrary.id()).sendKeys(CommonLibrary.main_data_excel("tester", "userid"));
	Thread.sleep(1000);
	driver.findElement(ObjectLibrary.password()).click();
	driver.findElement(ObjectLibrary.password()).sendKeys(CommonLibrary.main_data_excel("tester", "password"));
	driver.findElement(ObjectLibrary.signIn()).click();
	driver.manage().window().maximize();
	
	System.out.println("Login successfully ended")	;
	}

	//*************************************************************************************************************
	//*	                                Changing account names                                                    *
	//*************************************************************************************************************	

	@Test(priority = 1)
	public static void selectAccount() throws InterruptedException, IOException {
		
	System.out.println("select account started")	;	
		
	//Locate 'Account Names' drop down
	Thread.sleep(5000);
	if (driver.getPageSource().contains("Welcome to the new navigation experience!")) {
	    driver.findElement(By.xpath("//*[@data-testid=\"onboardingModalCloseIcon\"]")).click();
	}
	Thread.sleep(1000);
	WebElement accountnames = driver.findElement(ObjectLibrary.accountNames());
	accountnames.click();
	WebElement name = driver.findElement(ObjectLibrary.searchAccName());
	String accountname = CommonLibrary.input_data_excel(testrowno, "accountname");
	name.sendKeys(accountname);
	Thread.sleep(1000);
	name.sendKeys(Keys.ARROW_DOWN, Keys.RETURN);
	Thread.sleep(2000);
	
	System.out.println("select account successfully ended")	;
	}


//*************************************************************************************************************
//*	                                Editing an existing test                                                  *
//*************************************************************************************************************	

@Test(priority = 2)
	public static void editABTest() throws InterruptedException, IOException {
	
	System.out.println("edit test started")	;
	
	//Searching for the particular test id
		Thread.sleep(3000);
		Actions acti =  new Actions(driver);
		acti.moveToElement(driver.findElement(ObjectLibrary.searchTestField())).click().build().perform();
		driver.findElement(ObjectLibrary.searchTestField()).sendKeys("914084");
		//clicking on edit option
		Thread.sleep(4000);
		CommonLibrary.hoverAndClick(driver,driver.findElement(ObjectLibrary.editCampaignPencilIcon()),driver.findElement(ObjectLibrary.editCampaignPencilIcon()));
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[contains(text(),'Visual editor')]")).click();
		
		System.out.println("edit test successfully ended")	;
		}
	//*************************************************************************************************************
	//*	                                      editor test                                                         *
	//*************************************************************************************************************	
	
	@Test(priority = 3)
	public static void editortest() throws Exception {
		
		System.out.println("editortest started")	;
		
		if (driver.getWindowHandles().size() > 1) {
			//Switch to new window opened
			for(String winHandle : driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
		}
		
		//Changing background color of a literal
		Thread.sleep(14000);
		driver.switchTo().frame(0);
		driver.findElement(ObjectLibrary.Elementtobechanged()).click();
		driver.switchTo().defaultContent();
		Thread.sleep(1000);
		WebElement menu = driver.findElement(ObjectLibrary.Edit());
		Actions act = new Actions(driver); 
		act.moveToElement(menu).build().perform(); 
		Thread.sleep(1000);
		CommonLibrary.hoverAndClick(driver,driver.findElement(ObjectLibrary.Edit()),driver.findElement(ObjectLibrary.Editstyle()));
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.colourtab()).click();
		driver.findElement(ObjectLibrary.greencolor()).clear();
		driver.findElement(ObjectLibrary.greencolor()).sendKeys("#3DFFA5");
		driver.findElement(By.xpath("//*[@id=\"tabs-styleColor\"]/table/tbody/tr[6]/td[1]/label")).click();
		driver.findElement(ObjectLibrary.savechanges()).click();
		
		//Adding an image popin
		String widget = "Image Pop-in";
		Thread.sleep(1000);
		Actions acti =  new Actions(driver);
		acti.moveToElement(driver.findElement(ObjectLibrary.nudge())).click().perform();
		Thread.sleep(1000);
		acti.moveToElement(driver.findElement(ObjectLibrary.searchnudge())).click().build().perform();
		driver.findElement(ObjectLibrary.searchnudge()).sendKeys(widget);

		driver.findElement(By.xpath("//*[@id=\"ab-embed-editor__modal__pluginsLibrary\"]/div/div/div/section/div/div[3]/div[2]/div[1]/footer/h3")).click();
		driver.findElement(ObjectLibrary.addwidget()).click();
		Thread.sleep(3000);
		driver.findElement(ObjectLibrary.WidgetContent()).click();
		Thread.sleep(2000);
		driver.findElement(ObjectLibrary.widgetsStyle()).click();
		Thread.sleep(2000);
		driver.findElement(ObjectLibrary.widgetCondition()).click();
		Thread.sleep(3000);
		driver.findElement(ObjectLibrary.saveNPS()).click();
		Thread.sleep(2000);
		driver.findElement(ObjectLibrary.Showeditor()).click();

		//updating the tag
		Thread.sleep(1000);
		if(driver.getPageSource().contains("Live")) {
			driver.findElement(By.xpath("//*[text()=\"Tag\"]")).click();
		}
		else {
		driver.findElement(ObjectLibrary.editorplay()).click();
		}
		parentWindow= driver.getWindowHandle();
		verifyTest();
		if (driver.getPageSource().contains("popin-image")) {
			 expectedvalue = "true";
		}
		else {
			 expectedvalue = "false";
		}
		Thread.sleep(1000);
		Assert.assertEquals(expectedvalue,"true", message);
		editor();
		
		//removing the widget
	    Thread.sleep(8000);
		WebElement ele = driver.findElement(ObjectLibrary.sidebar());
		Actions actions = new Actions(driver);
		actions.moveToElement(ele).click().perform();
		Thread.sleep(1000);
		//CommonLibrary.HoverAndClick(driver,driver.findElement(By.xpath("//*[@class=\"el-checkbox\"]")),driver.findElement(By.xpath("//*[@class=\"el-checkbox\"]")));
		driver.findElement(By.xpath("//*[@id=\"ab-embed-editor__modal__history\"]/div/div/div/div/div[3]/div/div[1]/button")).click();
		Thread.sleep(1000);
	driver.findElement(By.xpath("//*[contains(@class,\"redBtn\")]")).click();
	Thread.sleep(1000);
	driver.findElement(By.xpath("//*[@id=\"ab-embed-editor__modal__history\"]/div/div/div/div/div[4]/div/button[2]")).click();
	driver.findElement(By.xpath("//button[@class=\"closeBtn\"]")).click();
	
	System.out.println("editortest successfully ended")	;
	}
	
	//*************************************************************************************************************
	//*	                                      targetting test                                                         *
	//*************************************************************************************************************	
	
	@Test(priority = 4)
	public static void targettingtest() throws Exception {
		
		System.out.println("targettingtest started")	;
		
		if (driver.getWindowHandles().size() > 1) {
			//Switch to new window opened
			for(String winHandle : driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
		}
		
		Thread.sleep(7000);
		driver.findElement(ObjectLibrary.targetTab()).click();
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
	    wait.until(ExpectedConditions.visibilityOfElementLocated(ObjectLibrary.trigger()));
		WebElement element1 = driver.findElement(ObjectLibrary.trigger());
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element1);
		element1.click();
		//driver.findElement(objectlibrary.trigger()).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div/div[2]/div/div[1]/div[3]/div[3]/div[2]/div/div/div/div[2]/div/div/div/div[2]")).click();
		
		driver.findElement(By.xpath("//*[@id=\"react-select-3-input\"]")).sendKeys("browser");
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[text()=\"Browser trigger\"]")).click();
		driver.findElement(By.xpath("//*[text()=\"Save step\"]")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[text()=\"Tag\"]")).click();
		verifyTest1();
		JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
		Object result = javascriptExecutor.executeScript("return ABTasty.getTestsOnPage()");
		System.out.println(result);
		
		if((result.toString()).contains("807981={name=AutotestAB, status=accepted,")) {
			
		//System.out.println(javascriptExecutor.executeScript("return ABTasty.getTestsOnPage()"));
			 expectedvalue = "true";
		}
		else {
			 expectedvalue = "false";
		}
		Thread.sleep(1000);
		Assert.assertEquals(expectedvalue,"true", message);
		editor();
		Thread.sleep(7000);
		System.out.println("targettingtest successfully ended")	;
	}
	
	//*************************************************************************************************************
	//*	                                      consent test                                                        *
	//*************************************************************************************************************	
		
	@Test(priority = 5)
	public static void consent() throws Exception {
		
		System.out.println("consent started")	;
		//navigating to cookies page
		Thread.sleep(1000);
		if (testenvironment.equalsIgnoreCase("staging")) {	
		       driver.get("https://staging-app2.abtasty.com/settings/cookies/deposit");
		 }
			else {
		       driver.get("https://app2.abtasty.com/settings/cookies/deposit");
		 }	
		//clicking on yes for consent
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div/div/div/form/section[2]/article/div/div[2]")).click();
		//adding a custom consent
		Thread.sleep(2000);
		WebElement element1 = driver.findElement(By.xpath("//*[text()=\"Custom condition (JavaScript)\"]"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element1);
		element1.click();
		Thread.sleep(1000);
		WebElement element = driver.findElement(By.xpath("//*[@id=\"cookieJs\"]/div[1]/div/div"));

		Actions act = new Actions(driver); 
		act.moveToElement(element).click().build().perform();
		Thread.sleep(1000);
		act.moveToElement(element).click().sendKeys(element, "return window.test == true").build().perform();
		
		//saving the consent
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[text()=\"Save\"]")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[text()=\"Tag\"]")).click();
		
		//validating the consent
		Thread.sleep(50000); 
//		if (testenvironment.equalsIgnoreCase("staging")) {
//	 	    //intercepting network calls
//	 	    networkmocking.networkmock(driver);
//	 	}
		networkmocking.networkread(driver,testenvironment);
		driver.get("http://abtastylab.com/023c2cee0f06c645d5c3d28e61b2e773/");
		Thread.sleep(2000);
		if (networkmocking.consent > 0) {
			expectedvalue = "1st time consent given";
	      	System.out.println("1st time consent given");
	    }
	    else {
	        expectedvalue = "1st time consent not given";
	        System.out.println("1st time consent not given");
	    }
		Assert.assertEquals(expectedvalue,"1st time consent not given", "Test is failed");
		//networkreading.networkread(driver);
		JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
		System.out.println(javascriptExecutor.executeScript("return window.test = true"));
		Thread.sleep(1000);
		 if (networkmocking.consent > 0) {
			 expectedvalue = "2nd time consent given";
	      	 System.out.println("2nd time consent given");
	     }
	     else {
	         expectedvalue = "2nd time consent not given";
	         System.out.println("2nd time consent not given");
	     }
		 Assert.assertEquals(expectedvalue,"2nd time consent given", "Test is failed");
		 
		 //going back to settings page
		 driver.navigate().back();
		 element1 = driver.findElement(By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div/div/div/form/section[2]/article/div[1]/div[2]/div"));
		 ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element1);
		 element1.click();
		// driver.findElement(By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div/div/div/form/section[2]/article/div[1]/div[2]/div")).click();
		 Thread.sleep(1000);
		 driver.findElement(By.xpath("//*[text()=\"Save\"]")).click();
		
		
//		chromeDevTools = driver.getDevTools();
//        chromeDevTools.createSession();
//        chromeDevTools.send(Log.enable());
//        chromeDevTools.addListener(Log.entryAdded(),
//                logEntry -> {
//                    System.out.println("log: "+logEntry.getText());
//                    System.out.println("level: "+logEntry.getLevel());
//                });
//        
//        driver.get("http://abtastylab.com/023c2cee0f06c645d5c3d28e61b2e773/");
//        Thread.sleep(2000);
		
		 System.out.println("consent successfully ended")	;
        	
	}
	
	//*************************************************************************************************************
	//*	                                Verify the test                                                           *
	//*************************************************************************************************************	

		public static void verifyTest() throws Exception {
			
			Thread.sleep(50000); 
		 	String URL = CommonLibrary.input_data_excel(testrowno, "URL");
		 	System.out.println(URL);
//		 	((JavascriptExecutor) driver).executeScript("window.open(arguments[0])", URL);
		 	if (testenvironment.equalsIgnoreCase("staging")) {
		 	    //intercepting network calls
		 	    networkmocking.networkmock(driver);
		 	}
		 	driver.get(URL);
		    Thread.sleep(3000);

			int tabno = driver.getWindowHandles().size();
			ArrayList<String> windowHandles = new ArrayList<String> (driver.getWindowHandles());
		    driver.switchTo().window(windowHandles.get(tabno-1));

		    //refreshing the page  
		        Thread.sleep(1000);
		        driver.navigate().refresh();
		        Thread.sleep(1000);

			}
		
		//*************************************************************************************************************
		//*	                                Verify the test                                                           *
		//*************************************************************************************************************	

			public static void verifyTest1() throws Exception {
				
				Thread.sleep(50000); 
				
			 	String URL = CommonLibrary.input_data_excel(testrowno, "URL");
			 	driver.get(URL);
			 	//intercepting network calls
			 	//networkmocking.networkmock(driver);

				int tabno = driver.getWindowHandles().size();
				ArrayList<String> windowHandles = new ArrayList<String> (driver.getWindowHandles());
			    driver.switchTo().window(windowHandles.get(tabno-1));

			    //refreshing the page  
		        Thread.sleep(1000);
		        driver.navigate().refresh();
		        Thread.sleep(1000);
				}
		
		//*************************************************************************************************************
		//*	                                going back to editor                                                      *
		//*************************************************************************************************************	

			
			public static void editor() throws Exception {
				
				if (testenvironment.equalsIgnoreCase("staging")) {	
			       driver.get("http://abtastylab.com/023c2cee0f06c645d5c3d28e61b2e773/?abtasty_editor_preprod=914084");
				}
				else {
			       driver.get("http://abtastylab.com/023c2cee0f06c645d5c3d28e61b2e773/?abtasty_editor=914084");
			    }	
			}	
				
		//*************************************************************************************************************
		//*	                                Closing the driver                                                        *
		//*************************************************************************************************************

			@AfterSuite
			public static void closedriver() throws IOException {
				driver.quit();
			}			
}	

