package regression;

import org.testng.annotations.Test;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;

import pageObject.ObjectLibrary;

public class target_only_regression {
	
	static WebDriver driver;
	static String parentWindow;
	static String incexc = "";
	static String value = "";
	static String value2 = "";
	static String value3 = "";
	static String expectedvalue;
	static String testrowno;
	public static String browserName;
	public static String os;
	static String message = "targetting is not properly applied";
	static String chromedriverpath = "C:\\abTastySelenium\\drivers\\chromedriver.exe";
	static String browserpath = "--user-data-dir=/Users/monikapanda/Documents/Selenium/BrowserProfile/Profilenew";
	static String excelpath = "/Users/monikapanda/Documents/ABTastyData1.xlsx";

//*************************************************************************************************************
//*	                                Logging in into ABTasty                                                   *
//*************************************************************************************************************	
		   
		@BeforeSuite
		public static void Login() throws InterruptedException, IOException {
			
		//sending slack message about a test in progress
		String testername = CommonLibrary.main_data_excel("tester", "Name");
		//EyesSlack.initialpost(testername , "https://hooks.slack.com/services/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");
		System.setProperty("webdriver.chrome.driver", chromedriverpath);

		//To check whether to test in staging or production

	    testrowno = "1st";
		String testtype = CommonLibrary.input_data_excel(testrowno, "typeofenv");
		if (testtype.equalsIgnoreCase("staging"))
		{
		//Initialize browser
			ChromeOptions options = new ChromeOptions();
			options.addArguments("-incognito");
			options.addArguments(browserpath);
			
		//Open the editor
			driver = new ChromeDriver(options);
		    driver.get("https://staging-app2.abtasty.com/experiments");
		}
		else
		{
			//closing the extra line at top
			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("useAutomationExtension", false);
			options.setExperimentalOption("excludeSwitches",Collections.singletonList("enable-automation"));    
			driver = new ChromeDriver(options);
			driver.get("https://app2.abtasty.com/experiments");
		}
		
		//deriving the browser and os
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		browserName = caps.getBrowserName();
		String browserVersion = caps.getVersion();

		//Get OS name.
		os = System.getProperty("os.name").toLowerCase();
		System.out.println("browserName" + browserName + "browserVersion" + browserVersion + "os" + os);


		//login to the editor
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.id()).click();

		driver.findElement(ObjectLibrary.id()).sendKeys(CommonLibrary.main_data_excel("tester", "userid"));

		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.password()).click();
		driver.findElement(ObjectLibrary.password()).sendKeys(CommonLibrary.main_data_excel("tester", "password"));
		driver.findElement(ObjectLibrary.signIn()).click();
		driver.manage().window().maximize();
		}

		//*************************************************************************************************************
		//*	                                Changing account names                                                    *
		//*************************************************************************************************************	

		@Test(priority = 0)
		public static void selectAccount() throws InterruptedException, IOException {
		//Locate 'Account Names' drop down
			Thread.sleep(4000);
			WebElement accountnames = driver.findElement(ObjectLibrary.accountNames());
			accountnames.click();
			WebElement name = driver.findElement(ObjectLibrary.searchAccName());
			String accountname = CommonLibrary.input_data_excel(testrowno, "accountname");
			name.sendKeys(accountname);
			Thread.sleep(1000);
			name.sendKeys(Keys.RETURN);
		}
		
//*************************************************************************************************************
//*	                                Editing an existing test                                                  *
//*************************************************************************************************************	

@Test(priority = 1)
	public static void editABTest() throws InterruptedException, IOException {
	//Searching for the particular test id
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div[2]/div[2]/div/div/div/div[2]/div/div[4]/label/input")).click();
			driver.findElement(By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div[2]/div[2]/div/div/div/div[2]/div/div[4]/label/input")).sendKeys(CommonLibrary.input_data_excel(testrowno, "testid"));
			//clicking on edit option
			Thread.sleep(2000);
			CommonLibrary.hoverAndClick(driver,driver.findElement(ObjectLibrary.editCampaignPencilIcon()),driver.findElement(ObjectLibrary.editCampaignPencilIcon()));
			Thread.sleep(1000);
			//CommonLibrary.HoverAndClick(driver,driver.findElement(By.xpath("//*[contains(text(),'Edit')]")),driver.findElement(By.xpath("//*[contains(text(),'Edit')]")));
			WebElement element = driver.findElement(By.xpath("//*[contains(text(),'Targeting')]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
			element.click();
			}

//*************************************************************************************************************
//*	                                Create Test Target                                                        *
//*************************************************************************************************************	

@Test(priority = 2)
	public static void setTarget() throws Exception {
		if (driver.getWindowHandles().size() > 1) {
			//Switch to new window opened
			for(String winHandle : driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
		    }
			Thread.sleep(3000);
			//to select the All Visitors
			if(!driver.getPageSource().contains("All visitors")) {
				driver.findElement(ObjectLibrary.whocampaign()).click();
				Thread.sleep(1000);
				driver.findElement(ObjectLibrary.Audience()).click();
				driver.findElement(By.xpath("//*[text()='All visitors']")).click();
			}
			
			//to select the no trigger
		    Thread.sleep(1000);
		    if(!driver.getPageSource().contains("No trigger")) {
		    driver.findElement(ObjectLibrary.trigger()).click();
		    Thread.sleep(1000);
		    driver.findElement(ObjectLibrary.triggeraudience()).click();
		    driver.findElement(By.xpath("//*[text()='No trigger']")).click();
			Thread.sleep(1000);
			//closing the trigger tab
			driver.findElement(ObjectLibrary.trigger()).click();
		    }
			parentWindow= driver.getWindowHandle();
			//saving the target
			Thread.sleep(500);
			WebElement element = driver.findElement(ObjectLibrary.saveTarget());
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
			Thread.sleep(1000);
			element.click();
            //updating the tag
			Thread.sleep(1000);
			driver.findElement(ObjectLibrary.Tagupdate()).click();
			verifyTest();
			Assert.assertEquals(expectedvalue,"true", message);	
}
					
@Test(priority = 4)
	public static void setBrowser() throws Exception {

			//for browser 
			String targetName = "Browser";
			
			driver.findElement(ObjectLibrary.Targetfilter()).click();
			Thread.sleep(1000);
			driver.findElement(ObjectLibrary.Targetselect()).click();
			driver.findElement(ObjectLibrary.Targetselect()).sendKeys(targetName);
			Thread.sleep(1000);
			//CommonLibrary.HoverAndClick(driver,driver.findElement(By.xpath("//div[contains(@class, 'styles__fadeOnHover___32GOV')]")),driver.findElement(By.xpath("//button[contains(@class, 'ButtonSecondary__button___fNmae')]")));	
			driver.findElement(By.xpath("/html/body/div[14]/div/div/div/div[2]/div[2]/div[2]/div/p")).click();
			targettabname(targetName);
			audienceCondition(driver,incexc,value,targetName);
	        //saving the target
			Thread.sleep(500);
		    WebElement element = driver.findElement(ObjectLibrary.saveTarget());
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
			element.click();
            //updating the tag
			Thread.sleep(2000);			
	        driver.findElement(ObjectLibrary.Tagupdate()).click();
	        verifyTest();
	        //deleting the target
	        CommonLibrary.hoverAndClick(driver,driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div[1]/div[1]/div/div[1]/div[2]")),driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div[1]/div[1]/div/div[1]/div[2]/span")));
	   	   // driver.findElement(objectlibrary.Targetdelete()).click();
	        Assert.assertEquals(expectedvalue,"true", message);
}

			//for device 
            @Test(priority = 5)
			public static void setDevice() throws Exception {
	        String targetName = "Device";
   	     
			Thread.sleep(1000);
	        driver.findElement(ObjectLibrary.Targetfilter()).click();
			Thread.sleep(1000);
			driver.findElement(ObjectLibrary.Targetselect()).click();
			driver.findElement(ObjectLibrary.Targetselect()).sendKeys(targetName);
			
			//CommonLibrary.HoverAndClick(driver,driver.findElement(By.xpath("//div[contains(@class, 'styles__fadeOnHover___32GOV')]")),driver.findElement(By.xpath("//button[contains(@class, 'ButtonSecondary__button___fNmae')]")));	
			driver.findElement(By.xpath("/html/body/div[14]/div/div/div/div[2]/div[2]/div/div/p")).click();
			targettabname(targetName);
			audienceCondition(driver,incexc,value,targetName);
			//saving the target
			Thread.sleep(500);
		    WebElement element = driver.findElement(ObjectLibrary.saveTarget());
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
			Thread.sleep(1000);
			element.click();
            //updating the tag
			Thread.sleep(2000);
			  driver.findElement(ObjectLibrary.Tagupdate()).click();
		      verifyTest();
		    //deleting the target
		    CommonLibrary.hoverAndClick(driver,driver.findElement(ObjectLibrary.Targetdeletehover()),driver.findElement(ObjectLibrary.Targetdelete()));
	        Assert.assertEquals(expectedvalue,"false", message);		
}		
            
			//for browser language
            @Test(priority = 6)
            public static void setBrowserLang() throws Exception {

	        String targetName = "Browser Language";
			Thread.sleep(1000);
	        driver.findElement(ObjectLibrary.Targetfilter()).click();
			Thread.sleep(1000);
			driver.findElement(ObjectLibrary.Targetselect()).click();
			driver.findElement(ObjectLibrary.Targetselect()).sendKeys(targetName);
			
			driver.findElement(By.xpath("/html/body/div[14]/div/div/div/div[2]/div[2]/div/div/p")).click();
			targettabname(targetName);
			audienceCondition(driver,incexc,value,targetName);
			driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div[1]/div[1]/div/div[2]/div[1]/ul/div/div/div[2]/div/div/div/div/div/div[2]")).click();
			driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div[1]/div[1]/div/div[2]/div[1]/ul/div/div/div[2]/div/div/div/div/div/div[1]/div[2]/div/input")).sendKeys("Engl");
			Thread.sleep(1000);
			driver.findElement((By.xpath("//*[text()='English']"))).click();
			//driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div[1]/div[1]/div/div[2]/div[1]/ul/div/div/div[2]/div/div/div/div/div/div[1]/div[2]/div/input")).sendKeys(Keys.RETURN);
			//saving the target
			Thread.sleep(500);
		    WebElement element = driver.findElement(ObjectLibrary.saveTarget());
			element.click();
            //updating the tag
			Thread.sleep(2000);
			driver.findElement(ObjectLibrary.Tagupdate()).click();
		    verifyTest();
		    //deleting the target
		    CommonLibrary.hoverAndClick(driver,driver.findElement(ObjectLibrary.Targetdeletehover()),driver.findElement(ObjectLibrary.Targetdelete()));
	        Assert.assertEquals(expectedvalue,"true", message);	
}

			//for Geolocation
                @Test(priority = 7)
                public static void setGeolocation() throws Exception {
        	        
		        String targetName = "Geolocation";
	
				Thread.sleep(1000);
		        driver.findElement(ObjectLibrary.Targetfilter()).click();
				Thread.sleep(1000);
				driver.findElement(ObjectLibrary.Targetselect()).click();
				driver.findElement(ObjectLibrary.Targetselect()).sendKeys(targetName);
				
				CommonLibrary.hoverAndClick(driver,driver.findElement(By.xpath("//div[contains(@class, 'styles__fadeOnHover___32GOV')]")),driver.findElement(By.xpath("//button[contains(@class, 'ButtonSecondary__button___fNmae')]")));
				targettabname(targetName);
				audienceCondition(driver,incexc,value,targetName);
				driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div[1]/div[1]/div/div[2]/div[1]/ul/div/div/div[2]/div/div/div/div/div/div[1]/div[1]")).click();
				driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div[1]/div[1]/div/div[2]/div[1]/ul/div/div/div[2]/div/div/div[1]/div/div[1]/div[1]/div[2]/div/input")).sendKeys("Franc");
				driver.findElement((By.xpath("//*[text()='France']"))).click();
				Thread.sleep(1000);
				driver.findElement(ObjectLibrary.trigger()).click();
				//saving the target
				Thread.sleep(500);
			    WebElement element = driver.findElement(ObjectLibrary.saveTarget());
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
				Thread.sleep(1000);
				element.click();
	            //updating the tag
				Thread.sleep(1000);
				  driver.findElement(ObjectLibrary.Tagupdate()).click();
			        verifyTest();
			      //deleting the target
				 CommonLibrary.hoverAndClick(driver,driver.findElement(ObjectLibrary.Targetdeletehover()),driver.findElement(ObjectLibrary.Targetdelete()));
		        Assert.assertEquals(expectedvalue,"false", message);
}
                
    			//for Landing Page
                @Test(priority = 8)
                public static void setLandingPage() throws Exception {

		        String targetName = "Landing Page";
    	      
				Thread.sleep(1000);
		        driver.findElement(ObjectLibrary.Targetfilter()).click();
				Thread.sleep(1000);
				driver.findElement(ObjectLibrary.Targetselect()).click();
				driver.findElement(ObjectLibrary.Targetselect()).sendKeys(targetName);

				driver.findElement(By.xpath("/html/body/div[14]/div/div/div/div[2]/div[2]/div/div/p")).click();
				targettabname(targetName);
				audienceCondition(driver,incexc,value,targetName);
				driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div[1]/div[1]/div/div[2]/div[1]/ul/div/div/div[3]/div/div/label/input")).sendKeys(CommonLibrary.input_data_excel(testrowno, "URL"));
				//saving the target
				Thread.sleep(500);
			    WebElement element = driver.findElement(ObjectLibrary.saveTarget());
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
				Thread.sleep(1000);
				element.click();
	            //updating the tag
				Thread.sleep(2000);
				driver.findElement(ObjectLibrary.Tagupdate()).click();
			    verifyTest();
			    //deleting the target
			    CommonLibrary.hoverAndClick(driver,driver.findElement(ObjectLibrary.Targetdeletehover()),driver.findElement(ObjectLibrary.Targetdelete()));
		        Assert.assertEquals(expectedvalue,"true", message);
}	
                
              //IP
                @Test(priority = 9)
                public static void setIP() throws Exception {

                String targetName = "IP address";

                Thread.sleep(1000);
                driver.findElement(ObjectLibrary.Targetfilter()).click();
                Thread.sleep(1000);
                driver.findElement(ObjectLibrary.Targetselect()).click();
                driver.findElement(ObjectLibrary.Targetselect()).sendKeys(targetName);


                driver.findElement(By.xpath("/html/body/div[14]/div/div/div/div[2]/div[2]/div/div/p")).click();
    			targettabname(targetName);
    			audienceCondition(driver,incexc,value,targetName);
    			//saving the target
    			Thread.sleep(500);
    		    WebElement element = driver.findElement(ObjectLibrary.saveTarget());
    			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
    			Thread.sleep(1000);
    			element.click();
                //updating the tag
    			Thread.sleep(2000);
    			  driver.findElement(ObjectLibrary.Tagupdate()).click();
    		      verifyTest();
    		    //deleting the target
    		    CommonLibrary.hoverAndClick(driver,driver.findElement(ObjectLibrary.Targetdeletehover()),driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div[1]/div[1]/div/div[1]/div[2]/span[2]")));
    	        Assert.assertEquals(expectedvalue,"true", message);			  
                }

              //previous page
                @Test(priority = 10)
                    public static void setpreviousPage() throws Exception {

                    String targetName = "Previous page";
                	Thread.sleep(1000);
                    driver.findElement(ObjectLibrary.Targetfilter()).click();
                	Thread.sleep(1000);
                	driver.findElement(ObjectLibrary.Targetselect()).click();
                	driver.findElement(ObjectLibrary.Targetselect()).sendKeys(targetName);

                	driver.findElement(By.xpath("/html/body/div[14]/div/div/div/div[2]/div[2]/div/div/p")).click();
    				targettabname(targetName);
    				audienceCondition(driver,incexc,value,targetName);
    				driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div[1]/div[1]/div/div[2]/div[1]/ul/div/div/div[3]/div/div/label/input")).sendKeys(CommonLibrary.input_data_excel(testrowno, "URL"));
    				//saving the target
    				Thread.sleep(500);
    			    WebElement element = driver.findElement(ObjectLibrary.saveTarget());
    				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
    				Thread.sleep(1000);
    				element.click();
    	            //updating the tag
    				Thread.sleep(2000);
    				driver.findElement(ObjectLibrary.Tagupdate()).click();
    			    verifyTest();
    			    //deleting the target
    			    CommonLibrary.hoverAndClick(driver,driver.findElement(ObjectLibrary.Targetdeletehover()),driver.findElement(ObjectLibrary.Targetdelete()));
    		        Assert.assertEquals(expectedvalue,"false", message);
                }	

              //screen size
                @Test(priority = 11)
                public static void setScreensize() throws Exception {

                String targetName = "Screen size";

                Thread.sleep(1000);
                driver.findElement(ObjectLibrary.Targetfilter()).click();
                Thread.sleep(1000);
                driver.findElement(ObjectLibrary.Targetselect()).click();
                driver.findElement(ObjectLibrary.Targetselect()).sendKeys(targetName);


                driver.findElement(By.xpath("/html/body/div[14]/div/div/div/div[2]/div[2]/div/div/p")).click();
				targettabname(targetName);
				audienceCondition(driver,incexc,value,targetName);
                    
                	driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div[1]/div[1]/div/div[2]/div[1]/ul/div/div/div[2]/div/div/label/input")).click();
                	driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div[1]/div[1]/div/div[2]/div[1]/ul/div/div/div[2]/div/div/label/input")).sendKeys("30");
                	driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div[1]/div[1]/div/div[2]/div[1]/ul/div/div/div[3]/div/div/label/input")).click();
                	driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div[1]/div[1]/div/div[2]/div[1]/ul/div/div/div[3]/div/div/label/input")).sendKeys("40");
                	//saving the target
                	Thread.sleep(500);
                    WebElement element = driver.findElement(ObjectLibrary.saveTarget());
                	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
                	Thread.sleep(1000);
                	element.click();
                    //updating the tag
                	Thread.sleep(1000);
                    driver.findElement(ObjectLibrary.Tagupdate()).click();
                    verifyTest();
                  //deleting the target
                    CommonLibrary.hoverAndClick(driver,driver.findElement(ObjectLibrary.Targetdeletehover()),driver.findElement(ObjectLibrary.Targetdelete()));
    		        Assert.assertEquals(expectedvalue,"false", message);
                }


			//for Minimum Page Viewed
                @Test(priority = 12)
                public static void setMinpageViewed() throws Exception {
                	
		        String targetName = "Minimum Page Viewed";
				
				Thread.sleep(1000);
		        driver.findElement(ObjectLibrary.Targetfilter()).click();
				Thread.sleep(1000);
				driver.findElement(ObjectLibrary.Targetselect()).click();
				driver.findElement(ObjectLibrary.Targetselect()).sendKeys(targetName);
				
				driver.findElement(By.xpath("/html/body/div[14]/div/div/div/div[2]/div[2]/div/div/p")).click();
				targettabname(targetName);
				audienceCondition(driver,incexc,value,targetName);
				driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div[1]/div[1]/div/div[2]/div/ul/div/div/div[2]/div/div/label/input")).click();
				driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div[1]/div[1]/div/div[2]/div/ul/div/div/div[2]/div/div/label/input")).sendKeys("1");
				//saving the target
				Thread.sleep(500);
			    WebElement element = driver.findElement(ObjectLibrary.saveTarget());
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
				Thread.sleep(1000);
				element.click();
	            //updating the tag
				Thread.sleep(1000);
				  driver.findElement(ObjectLibrary.Tagupdate()).click();
			        verifyTest();
			      //deleting the target
			        CommonLibrary.hoverAndClick(driver,driver.findElement(ObjectLibrary.Targetdeletehover()),driver.findElement(ObjectLibrary.Targetdelete()));
    		        Assert.assertEquals(expectedvalue,"true", message);
		        
}						
			
			//Same Day Visit
                @Test(priority = 13)
                public static void setsamedayvisit() throws Exception {

		        String targetName = "Same Day Visit";
    	 
				Thread.sleep(1000);
		        driver.findElement(ObjectLibrary.Targetfilter()).click();
				Thread.sleep(1000);
				driver.findElement(ObjectLibrary.Targetselect()).click();
				driver.findElement(ObjectLibrary.Targetselect()).sendKeys(targetName);
				
				driver.findElement(By.xpath("/html/body/div[14]/div/div/div/div[2]/div[2]/div/div/p")).click();
			//saving the target
			Thread.sleep(500);
		    WebElement element = driver.findElement(ObjectLibrary.saveTarget());
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
			Thread.sleep(1000);
			element.click();
            //updating the tag
			Thread.sleep(1000);
			  driver.findElement(ObjectLibrary.Tagupdate()).click();
		        verifyTest();
		      //deleting the target
		        CommonLibrary.hoverAndClick(driver,driver.findElement(ObjectLibrary.Targetdeletehover()),driver.findElement(ObjectLibrary.Targetdelete()));
		        Assert.assertEquals(expectedvalue,"true", message);
}			
            
			//Adblocker
            @Test(priority = 14)
            public static void setadblocker() throws Exception {

	        String targetName = "Adblocker";

			Thread.sleep(1000);
	        driver.findElement(ObjectLibrary.Targetfilter()).click();
			Thread.sleep(1000);
			driver.findElement(ObjectLibrary.Targetselect()).click();
			driver.findElement(ObjectLibrary.Targetselect()).sendKeys(targetName);

			
			driver.findElement(By.xpath("/html/body/div[14]/div/div/div/div[2]/div[2]/div/div/p")).click();
			targettabname(targetName);
			audienceCondition(driver,incexc,value,targetName);
				//saving the target
				Thread.sleep(500);
			    WebElement element = driver.findElement(ObjectLibrary.saveTarget());
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
				Thread.sleep(1000);
				element.click();
	            //updating the tag
				Thread.sleep(1000);
				  driver.findElement(ObjectLibrary.Tagupdate()).click();
			        verifyTest();
			        //deleting the target
			        CommonLibrary.hoverAndClick(driver,driver.findElement(ObjectLibrary.Targetdeletehover()),driver.findElement(ObjectLibrary.Targetdelete()));
			        Assert.assertEquals(expectedvalue,"false", message);		  
}



//source
@Test(priority = 15)
public static void setSource() throws Exception {

String targetName = "Source";
Thread.sleep(1000);
driver.findElement(ObjectLibrary.Targetfilter()).click();
Thread.sleep(1000);
driver.findElement(ObjectLibrary.Targetselect()).click();
driver.findElement(ObjectLibrary.Targetselect()).sendKeys(targetName);


driver.findElement(By.xpath("/html/body/div[14]/div/div/div/div[2]/div[2]/div[2]/div/p")).click();
targettabname(targetName);
audienceCondition(driver,incexc,value,targetName);
	driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div[1]/div[1]/div/div[2]/div[1]/ul/div/div/div[2]/div/div/label/input")).click();
	driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div[1]/div[1]/div/div[2]/div[1]/ul/div/div/div[2]/div/div/label/input")).sendKeys("www.google.com");
	//saving the target
	Thread.sleep(500);
  WebElement element = driver.findElement(ObjectLibrary.saveTarget());
	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
	Thread.sleep(1000);
	element.click();
  //updating the tag
	Thread.sleep(1000);
  driver.findElement(ObjectLibrary.Tagupdate()).click();
  verifyTest();
//deleting the target
  CommonLibrary.hoverAndClick(driver,driver.findElement(ObjectLibrary.Targetdeletehover()),driver.findElement(ObjectLibrary.Targetdelete()));
  Assert.assertEquals(expectedvalue,"false", message);				  
}

//source type
@Test(priority = 16)
public static void setsourcetype() throws Exception {

String targetName = "Source Type";
Thread.sleep(1000);
driver.findElement(ObjectLibrary.Targetfilter()).click();
Thread.sleep(1000);
driver.findElement(ObjectLibrary.Targetselect()).click();
driver.findElement(ObjectLibrary.Targetselect()).sendKeys(targetName);

driver.findElement(By.xpath("/html/body/div[14]/div/div/div/div[2]/div[2]/div/div/p")).click();
targettabname(targetName);
audienceCondition(driver,incexc,value,targetName);
//closing the target tab
Thread.sleep(500);
WebElement element = driver.findElement(ObjectLibrary.trigger());
((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
Thread.sleep(1000);
element.click();
//driver.findElement(objectlibrary.trigger()).click();
//saving the target
Thread.sleep(500);
element = driver.findElement(ObjectLibrary.saveTarget());
element.click();
//updating the tag
  driver.findElement(ObjectLibrary.Tagupdate()).click();
  verifyTest();
 //deleting the target
 //CommonLibrary.HoverAndClick(driver,driver.findElement(objectlibrary.Targetdeletehover()),driver.findElement(objectlibrary.Targetdelete()));
  Assert.assertEquals(expectedvalue,"false", message);		
}	


//cookie segment
@Test(priority = 18)
public static void setcookie() throws Exception {
	
Thread.sleep(1000);
driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div[1]/div/input")).click();
driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div[1]/div/input")).sendKeys("cookie");
Thread.sleep(1000);
driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div[2]/div/div[1]/div/span")).click();
//saving the target
Thread.sleep(500);
WebElement element = driver.findElement(ObjectLibrary.saveTarget());
((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
Thread.sleep(1000);
element.click();
//updating the tag
   driver.findElement(ObjectLibrary.Tagupdate()).click();
   verifyTest();
   Assert.assertEquals(expectedvalue,"false", message);
}

//no of sessions segment
@Test(priority = 19)
public static void setnoofsessions() throws Exception {

Thread.sleep(1000);
driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div[1]/div[1]/span[2]")).click();
Thread.sleep(1000);
driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div[1]/div/input")).click();
driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div[1]/div/input")).sendKeys("Noofsessions");
Thread.sleep(1000);
driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div[2]/div/div[1]/div/span")).click();
//saving the target
Thread.sleep(500);
WebElement element = driver.findElement(ObjectLibrary.saveTarget());
((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
Thread.sleep(1000);
element.click();
//updating the tag
 driver.findElement(ObjectLibrary.Tagupdate()).click();
 verifyTest();
 Assert.assertEquals(expectedvalue,"false", message);
}

//Days Since Last Session segment
@Test(priority = 20)
public static void setdayssincelastsession() throws Exception {

	Thread.sleep(1000);
	driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div[1]/div[1]/span[2]")).click();
	Thread.sleep(1000);
	driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div[1]/div/input")).click();
	driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div[1]/div/input")).sendKeys("lastsession audience");
	Thread.sleep(1000);
	driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div[2]/div/div[1]/div/span")).click();
	//saving the target
	Thread.sleep(500);
	WebElement element = driver.findElement(ObjectLibrary.saveTarget());
	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
	Thread.sleep(1000);
	element.click();
	//updating the tag
	 driver.findElement(ObjectLibrary.Tagupdate()).click();
	 verifyTest();
Assert.assertEquals(expectedvalue,"false", message);
}

//Days Since First Session segment
@Test(priority = 21)
public static void setdayssincefirstsession() throws Exception {

	Thread.sleep(1000);
	driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div[1]/div[1]/span[2]")).click();
	Thread.sleep(1000);
	driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div[1]/div/input")).click();
	driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div[1]/div/input")).sendKeys("firstsession audience");
	Thread.sleep(1000);
	driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div[2]/div/div[1]/div/span")).click();
	//saving the target
	Thread.sleep(500);
	WebElement element = driver.findElement(ObjectLibrary.saveTarget());
	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
	Thread.sleep(1000);
	element.click();
	//updating the tag
	 driver.findElement(ObjectLibrary.Tagupdate()).click();
	 verifyTest();
Assert.assertEquals(expectedvalue,"false", message);
}

//Action tracking segment
@Test(priority = 22)
public static void setactiontracking() throws Exception {

Thread.sleep(1000);
driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div[1]/div[1]/span[2]")).click();
Thread.sleep(1000);
driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div[1]/div/input")).click();
driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div[1]/div/input")).sendKeys("actiontracking segment");
Thread.sleep(1000);
driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div[2]/div/div[1]/div/span")).click();
//saving the target
Thread.sleep(500);
WebElement element = driver.findElement(ObjectLibrary.saveTarget());
((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
Thread.sleep(1000);
element.click();
//updating the tag
 driver.findElement(ObjectLibrary.Tagupdate()).click();
 verifyTest();
Assert.assertEquals(expectedvalue,"false", message);
}

//Pucrchase frequency segment
@Test(priority = 23)
public static void setpurchasefreq() throws Exception {

	Thread.sleep(1000);
	driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div[1]/div[1]/span[2]")).click();
	Thread.sleep(1000);
	driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div[1]/div/input")).click();
	driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div[1]/div/input")).sendKeys("purchasefreq audience");
	Thread.sleep(1000);
	driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div[2]/div/div[1]/div/span")).click();
	//saving the target
	Thread.sleep(500);
	WebElement element = driver.findElement(ObjectLibrary.saveTarget());
	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
	Thread.sleep(1000);
	element.click();
	//updating the tag
	 driver.findElement(ObjectLibrary.Tagupdate()).click();
	 verifyTest();
Assert.assertEquals(expectedvalue,"false", message);
}

//Last purchase segment
@Test(priority = 24)
public static void setlastpurchase() throws Exception {

	Thread.sleep(1000);
	driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div[1]/div[1]/span[2]")).click();
	Thread.sleep(1000);
	driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div[1]/div/input")).click();
	driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div[1]/div/input")).sendKeys("Recent buyer");
	Thread.sleep(1000);
	driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div[2]/div/div[1]/div/span")).click();
	//saving the target
	Thread.sleep(500);
	WebElement element = driver.findElement(ObjectLibrary.saveTarget());
	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
	Thread.sleep(1000);
	element.click();
	//updating the tag
	 driver.findElement(ObjectLibrary.Tagupdate()).click();
	 verifyTest();
Assert.assertEquals(expectedvalue,"false", message);
}

//Visitor status segment
@Test(priority = 25)
public static void setvisitorstatus() throws Exception {

	Thread.sleep(1000);
	driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div[1]/div[1]/span[2]")).click();
	Thread.sleep(1000);
	driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div[1]/div/input")).click();
	driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div[1]/div/input")).sendKeys("Visitor status");
	Thread.sleep(1000);
	driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div[2]/div/div[1]/div/span")).click();
	//saving the target
	Thread.sleep(500);
	WebElement element = driver.findElement(ObjectLibrary.saveTarget());
	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
	Thread.sleep(1000);
	element.click();
	//updating the tag
	 driver.findElement(ObjectLibrary.Tagupdate()).click();
	 verifyTest();
Assert.assertEquals(expectedvalue,"false", message);
}
//engagement level
//content interest

//*************************************************************************************************************
//*	                               Selecting the audience condition                                           *
//*************************************************************************************************************	

	public static void audienceCondition(WebDriver driver, String incexc, String value, String targetName) throws InterruptedException{ 
		
		Thread.sleep(1000);
//		driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div[1]/div[1]/div/div[2]/div[1]/ul/div/div/div[1]/div/div/div/div/div/div/span")).click();
	    //to select the include/exclude
//		WebElement element = driver.findElement(By.xpath("//*[text()='is']"));
//		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
//		Thread.sleep(2000);
//		element.click();
		//to select the value
//    driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[3]/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div[1]/div[1]/div/div[2]/div[1]/ul/div/div/div[2]/div/div/div/div/div/span")).click();
//	    Thread.sleep(1000);
//	    driver.findElement(By.xpath("//*[text()='Chrome']")).click();;
			
	}
	
	
	//*************************************************************************************************************
	//*	                               Selecting the sheet of the target                                          *
	//*************************************************************************************************************	
	
	public static void targettabname(String targetName) throws IOException {
	//to select the target tab
	File src = new File(excelpath);
	// Load the file
	FileInputStream fis = new FileInputStream(src);
	// load the workbook
	XSSFWorkbook wb = new XSSFWorkbook(fis);
	// get the sheet which you want to modify or create
	for (int i = 0; i < wb.getNumberOfSheets(); i++) {
	if (wb.getSheetName(i).equals(targetName)) {
	XSSFSheet sh2 = wb.getSheetAt(i);
	//r is the row no of the ith sheet
	for (int r = 1; r <= sh2.getLastRowNum(); r++) {
		String rowvalue = sh2.getRow(r).getCell(0).getStringCellValue();
		if(rowvalue.equals("Y")) {
			incexc = sh2.getRow(r).getCell(1).getStringCellValue();
			value  = sh2.getRow(r).getCell(2).getStringCellValue();
			//System.out.println(incexc + value);
			if(sh2.getRow(1).getLastCellNum()>3) {
			    value2 = sh2.getRow(r).getCell(3).getStringCellValue();
			}
			
			if(sh2.getRow(1).getLastCellNum()>4) {
				value3 = sh2.getRow(r).getCell(4).getStringCellValue();
			}
		}
	}
	}
	}
	}
	
	//Opening the segment tab
	@Test(priority = 17)
	public static void openSegment() throws InterruptedException {

		//setting no trigger in the trigger tab
		Thread.sleep(3000);
		driver.findElement(ObjectLibrary.trigger()).click();
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.triggeraudience()).click();
		driver.findElement(By.xpath("//*[@id=\"react-select-3-option-0\"]")).click();
		
		 
		//to select a saved segment
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.whocampaign()).click();
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.Audience()).click();
		driver.findElement(By.xpath("//*[@id=\"react-select-2-option-1\"]")).click();	
	}		

	//Opening the trigger tab
	@Test(priority = 3)
	public static void openTrigger() throws InterruptedException {
	
	//to select the All Visitors
	int tabno = driver.getWindowHandles().size();
	ArrayList<String> windowHandles = new ArrayList<String> (driver.getWindowHandles());
	driver.switchTo().window(windowHandles.get(tabno-2));
	Thread.sleep(2000);
	driver.findElement(ObjectLibrary.whocampaign()).click();
	Thread.sleep(1000);
	driver.findElement(ObjectLibrary.Audience()).click();
	driver.findElement(By.xpath("//*[@id=\"react-select-2-option-0\"]")).click();
	
	//to select the custom audience
	Thread.sleep(1000);
	driver.findElement(ObjectLibrary.trigger()).click();
	Thread.sleep(1000);
	
	driver.findElement(ObjectLibrary.triggeraudience()).click();
	WebElement element = driver.findElement(By.xpath("//*[@id=\"react-select-3-option-2\"]"));
	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
	Thread.sleep(1000);
	element.click();
	//driver.findElement(By.xpath("//*[@id=\"react-select-3-option-2\"]")).click();
	}
	
	//*************************************************************************************************************
	//*	                                Verify the test                                                           *
	//*************************************************************************************************************	

	public static void verifyTest() throws Exception {
	    Thread.sleep(20000);
	    //opening the URL on a different tab
		String URL = CommonLibrary.input_data_excel(testrowno, "URL");
		((JavascriptExecutor) driver).executeScript("window.open(arguments[0])", URL);
		int tabno = driver.getWindowHandles().size();
		ArrayList<String> windowHandles = new ArrayList<String> (driver.getWindowHandles());
	    driver.switchTo().window(windowHandles.get(tabno-1));
	    //refreshing the page 
	    Thread.sleep(3000);
	    driver.navigate().refresh();
	    Thread.sleep(3000);
		if (driver.getPageSource().contains("#3DFFA5!important")) {
			 expectedvalue = "true";
		}
		else {
			 expectedvalue = "false";
		}
		Thread.sleep(1000);
		driver.switchTo().window(parentWindow);
		Thread.sleep(1000);
		
		}
	}


