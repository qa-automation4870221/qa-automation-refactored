
package regression;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class TargetingTest {

	private WebDriver driver;
	private By by;

	static String chromedriverpath = "C:\\abTastySelenium\\drivers\\chromedriver.exe";

	@Test(priority = 0)
	public void setUp() throws Exception {
		By by;
		System.setProperty("webdriver.chrome.driver", chromedriverpath);
		driver = new ChromeDriver();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get("https://app2.abtasty.com/login");
		driver.manage().window().maximize();
		driver.findElement(By.id("email")).sendKeys("mathumitha.murugan@abtasty.com");
		driver.findElement(By.cssSelector("#password")).sendKeys("Pikachu@1996");
		driver.findElement(By.cssSelector(".FormButtonRow__buttonRow___3k8xJ")).click();

		Thread.sleep(4000);

	}

	@Test(priority = 1)
	public void editCampaign() throws Exception {

		if (driver.getPageSource().contains("Welcome to the new navigation experience!")) {
			driver.findElement(By.xpath("//*[@data-testid= 'onboardingModalCloseIcon']")).click();
			System.out.println("Onboarding closed.......");
		}

		// Click 'Search...'
		Thread.sleep(500);
		by = By.xpath("//input[@placeholder = 'Search...']");
		driver.findElement(by).click();

		// Type '691451' in 'Search...'
		Thread.sleep(500);
		by = By.xpath("//input[@placeholder = 'Search...']");
		driver.findElement(by).sendKeys("691451");

		Thread.sleep(2000);

		// clicking on edit option
		Thread.sleep(2000);
		CommonLibrary.hoverAndClick(driver,
				driver.findElement(By.xpath("//div[1]/div[9]/div/div[1]/div/*[name()='svg']")),
				driver.findElement(By.xpath("//div[1]/div[9]/div/div[1]/div/*[name()='svg']")));
		Thread.sleep(1000);

		// click targeting
		driver.findElement(By.xpath("//div[normalize-space()='Targeting']")).click();
		Thread.sleep(2000);

		if (driver.getWindowHandles().size() > 1) {
			// Switch to new window opened
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
			}
		}

		// click Targeting
		driver.findElement(By.xpath("//p[normalize-space()='Targeting']")).click();

		// click dropdown
		driver.findElement(By.xpath(
				"//div[@class='Accordion__accordionBody___2lROb']//div[@class='ValueContainer__container___1skfC']"))
				.click();

		// selects the first option
		driver.findElement(By.xpath("//div[3]/div/div[@class='Option__tooltipTarget___1zHrk']")).click();

		// click segment
		driver.findElement(By.xpath("//p[normalize-space()='Who will see the test']")).click();

		// click trigger
		driver.findElement(By.xpath("//p[normalize-space()='How the test will be triggered']")).click();

		// click dropdown
		driver.findElement(By.xpath(
				"//div[@class='Accordion__accordionBody___2lROb']//div[@class='ValueContainer__container___1skfC']"))
				.click();

		// click first value
		driver.findElement(By.xpath("//div[3][@data-testid='audiencesDropdownOption']")).click();

	}

	@Test(priority = 2)
	public void TestWarning() throws Exception {

		// click Targeting
		driver.findElement(By.xpath("//p[normalize-space()='Targeting']")).click();

		// verify warning
		if (driver.findElement(By.xpath("//div[@class='Dialog__dialogContent___Ce_ls']")).isDisplayed()) {
			System.out.println("warning displayed");
		}

		// click stay
		driver.findElement(By.xpath("//button[normalize-space()='Stay']")).click();

		// click save
		driver.findElement(By.xpath("//button[normalize-space()='Save step']")).click();

		// verify sucess message
		driver.findElement(By.xpath("//div[@class='Toast__text___2I_H0']")).isDisplayed();

	}
// ****************************************************************************************
// * Closing the driver *
// ****************************************************************************************

	@AfterSuite
	public void closedriver() throws IOException {
		driver.quit();
	}
}
