
package regression;

import org.testng.annotations.Test;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

import java.io.IOException;
import java.time.Duration;
import java.util.Collections;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import pageObject.ObjectLibrary;

public class DashboardTest {

	static WebDriver driver;
	private By by;
	static String parentWindow;
	public static String ReportURL;
	static String variationid;
	static String Customerid;
	static String testrowno;
	static String typeoftest;
	static String testenvironment;
	public static String browserName;
	public static String os;
	//static String chromedriverpath = "/usr/local/bin/chromedriver";
	static String chromedriverpath = "C:\\abTastySelenium\\drivers\\chromedriver.exe";
	static String browserpath = "--user-data-dir=/Users/monikapanda/Documents/Selenium/BrowserProfile/Profilenew";
	static String excelpath = System.getProperty("user.dir") + "/ABTastyData1.xlsx";
	

	@BeforeSuite
	public static void initialslackmsg() throws IOException {
		String testername = CommonLibrary.main_data_excel("tester", "Name");
		// EyeSlack.initialpost(testername ,
		// "https://hooks.slack.com/services/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");
	}

//*************************************************************************************************************
//*	                                Logging in into ABTasty                                                   *
//*************************************************************************************************************

	@Parameters({ "testenvironment" })
	@SuppressWarnings("deprecation")
	@Test(priority = 0)
	public static void Login(String testenv) throws InterruptedException, IOException {

		System.out.println("Login started");

		System.setProperty("webdriver.chrome.driver", chromedriverpath);

		//To check whether to test in staging or production

		testrowno = "1st";
		testenvironment = testenv;

		//String testtype = CommonLibrary.input_data_excel(testrowno, "typeofenv");
		if (testenv.equalsIgnoreCase("staging")) {
			//Initialize browser
			ChromeOptions options = new ChromeOptions();
			//	options.addArguments("-incognito");
			options.setExperimentalOption("useAutomationExtension", false);
			options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));

			// to start chrome in headless mode
			options.addArguments("headless");
			options.addArguments("--window-size=1920,1080");
			options.addArguments("--no-sandbox");
			options.addArguments("--disable-dev-shm-usage");

			//	DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			//	capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			//  driver=new ChromeDriver(capabilities);
			//	options.addArguments(browserpath);
			System.out.println("staging");

			//Open the editor
			driver = new ChromeDriver(options);
			driver.get("https://staging-app2.abtasty.com/experiments");
			System.out.println("login " + driver.getCurrentUrl());
			if (driver.getPageSource().contains("403 ERROR")) {
				System.out.println("403 error on staging");
			}
		} else {
			// closing the extra line at top
			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("useAutomationExtension", false);
			options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
			// to start chrome in headless mode
			options.addArguments("headless");
			options.addArguments("--window-size=1920,1080");
			options.addArguments("--no-sandbox");
			options.addArguments("--disable-dev-shm-usage");

			driver = new ChromeDriver(options);
			driver.get("https://app2.abtasty.com/login");
			System.out.println("prod");
			System.out.println("login " + driver.getCurrentUrl());
		}
		//deriving the browser and os
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		browserName = caps.getBrowserName();
		String browserVersion = caps.getVersion();

		//Get OS name.
		os = System.getProperty("os.name").toLowerCase();
		System.out.println("browserName" + browserName + "browserVersion" + browserVersion + "os" + os);

		//login to the editor
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.id()).click();
		driver.findElement(ObjectLibrary.id()).sendKeys(CommonLibrary.main_data_excel("tester", "userid"));
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.password()).click();
		driver.findElement(ObjectLibrary.password()).sendKeys(CommonLibrary.main_data_excel("tester", "password"));
		driver.findElement(ObjectLibrary.signIn()).click();
		driver.manage().window().maximize();

		System.out.println("Login successfully done");
	}

//*************************************************************************************************************
//*	                                Changing account names                                                    *
//*************************************************************************************************************

	@Test(priority = 1)
	public static void selectAccount() throws Exception {

		System.out.println("select account names started");

		// Locate 'Account Names' drop down
		Thread.sleep(4000);
		if (driver.getPageSource().contains("Welcome to the new navigation experience!")) {
			driver.findElement(ObjectLibrary.onboardingModalCloseIcon()).click();
		}
		// change account
		CommonLibrary.ChangeAccount(driver, "Mathumitha");

		if (driver.getPageSource().contains("Mathumitha MURUGAN")) {
			System.out.println("Given Account selected");
		} else {
			System.out.println("Selected other Account");
		}

		System.out.println("Select account names successfully done");
	}

//*************************************************************************************************************
//*	                                Dashboard Archive Test                                    	              *
//*************************************************************************************************************
	@Test(priority = 3)
	public void archiveTest() throws Exception {
		System.out.println("Archive test started");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//div[@aria-label='Experimentation']")).click();
		Thread.sleep(2000);
		//Scroll up
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,-350)", "");
		driver.findElement(ObjectLibrary.searchDashboard()).click();
		driver.findElement(ObjectLibrary.searchDashboard()).clear();
		Thread.sleep(500);
		driver.findElement(ObjectLibrary.searchDashboard()).sendKeys("691451");
		Thread.sleep(2000);

		WebElement campaign = driver.findElement(By.xpath("//div[@data-testid='row-test-691451']"));

		if (campaign.isDisplayed()) {
			System.out.println("Campaign is Displayed");

			CommonLibrary.hoverAndClick(driver, driver.findElement(ObjectLibrary.moreActions()),
					driver.findElement(ObjectLibrary.moreActions()));
			Thread.sleep(500);

			driver.findElement(ObjectLibrary.archiveDashboard()).click();
			Thread.sleep(1000);
			driver.findElement(ObjectLibrary.archiveButton()).click();

//			WebElement Toaster = driver
//					.findElement(By.xpath("//div[@class='Toast__toast___1gBoS Toast__success___WkLAW']"));
//
//			if (Toaster.isDisplayed()) {
//				System.out.println("Element archived");
//			} else {
//				System.out.println("Failed to find Toaster");
//			}

			Thread.sleep(1000);
			driver.findElement(By.xpath("//button[contains(text(),'Archived')]")).click();
			Thread.sleep(1000);

			driver.findElement(ObjectLibrary.searchDashboard()).click();
			Thread.sleep(500);
			driver.findElement(ObjectLibrary.searchDashboard()).sendKeys("691451");
			Thread.sleep(2000);

			driver.findElement(By.xpath("//div[contains(text(),'Archived')]")).click();

//			driver.findElement(By
//					.xpath("//div[@class='OverflowMenu__container___qSGcI StatusButton__overflowMenuButton___2Uxyx']"))
//					.click();

			driver.findElement(By.xpath("//*[text()='Unarchive']")).click();
			Thread.sleep(500);

			driver.findElement(ObjectLibrary.searchDashboard()).click();
			driver.findElement(ObjectLibrary.searchDashboard()).clear();
			Thread.sleep(500);
			driver.findElement(ObjectLibrary.searchDashboard()).sendKeys("691451");
			Thread.sleep(2000);

			WebElement Archivedcampaign = driver.findElement(By.xpath("//p[text() = \"Oops...\"]"));

			if (Archivedcampaign.isDisplayed()) {
				System.out.println("Element unarchived");
			}
		} else {
			System.out.println("Campaign not Displayed");
		}
		
		Thread.sleep(1000);
		driver.findElement(By.xpath("//button[contains(text(),'All tests')]")).click();
		Thread.sleep(1000);

		System.out.println("Archive test successfully ended");
	}
	
	
//*************************************************************************************************************
//*	                                Dashboard Search Test                                   	              *
//*************************************************************************************************************
	@Test(priority = 4)
	public void searchTest() throws Exception {

		System.out.println("Search Test started");

		// click Reset button
		driver.findElement(ObjectLibrary.newFilter()).click();

		JavascriptExecutor js = (JavascriptExecutor) driver;
		//scroll to end
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.resetFilter()).click();
		Thread.sleep(500);
		driver.findElement(ObjectLibrary.applyFilter()).click();
		Thread.sleep(1000);
		// scroll up
		js.executeScript("window.scrollBy(0,-600)", "");

		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));

		wait.until(ExpectedConditions.elementToBeClickable((ObjectLibrary.searchDashboard())));

		// Click 'Search...'
		driver.findElement(ObjectLibrary.searchDashboard()).click();
		Thread.sleep(500);
		driver.findElement(ObjectLibrary.searchDashboard()).clear();
		Thread.sleep(500);
		driver.findElement(ObjectLibrary.searchDashboard()).sendKeys("691451");

		Thread.sleep(2000);

		// Is 691451 Displayed?
		if (driver.findElement(By.xpath("//p[@data-testid='testId-691451']")).isDisplayed()) {
			System.out.println("searched test is Displayed");
		}
		driver.findElement(ObjectLibrary.searchDashboard()).click();
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.searchDashboard()).clear();
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.searchDashboard()).clear();

		System.out.println("Search Test successfully completed");
	}

//*************************************************************************************************************
//*	                                Closing the drive                                        	              *
//*************************************************************************************************************	

	@AfterSuite
	public void closedriver() throws IOException {
		driver.quit();
	}
}
