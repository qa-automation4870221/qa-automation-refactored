package regression;

import java.util.Optional;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.devtools.DevTools;
import org.openqa.selenium.devtools.v100.network.Network;

public class networkreading {
	static int consent = 0;

	public static void networkread (ChromeDriver driver) throws InterruptedException {

        DevTools devTool = driver.getDevTools();

        devTool.createSession();

        devTool.send(Network.enable(Optional.empty(), Optional.empty(), Optional.empty()));

        System.out.println("method calling");
        devTool.addListener(Network.requestWillBeSent(), requestSent -> {
        	
        	if(requestSent.getRequest().getUrl().contains("ariane")) {

              System.out.println("Request URL => " + requestSent.getRequest().getUrl());

              System.out.println("Request Method => " + requestSent.getRequest().getMethod());

              System.out.println("Request Headers => " + requestSent.getRequest().getHeaders().toString());
              
              System.out.println("ariane found");

              System.out.println("------------------------------------------------------");
              
              consent++;
        	}
        	else {
        		System.out.println("no ariane");
        		System.out.println("Request URL => " + requestSent.getRequest().getUrl());
        	}
        	System.out.println(consent);
        });
  }


}

