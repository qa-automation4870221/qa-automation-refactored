
package regression;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import pageObject.ObjectLibrary;

import org.testng.AssertJUnit;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

public class scheduler {

	static WebDriver driver;
	private By by;
	static String parentWindow;
	public static String ReportURL;
	static String variationid;
	static String Customerid;
	static String testrowno;
	static String typeoftest;
	static String testenvironment;
	public static String browserName;
	public static String os;
	static String chromedriverpath = "/usr/local/bin/chromedriver";
//static String chromedriverpath = "/Users/mathumitha.muruganabtasty.com/QA/chromedriver";
	static String browserpath = "--user-data-dir=/Users/monikapanda/Documents/Selenium/BrowserProfile/Profilenew";
	static String excelpath = System.getProperty("user.dir") + "/ABTastyData1.xlsx";

	@BeforeSuite
	public static void initialslackmsg() throws IOException {
		String testername = CommonLibrary.main_data_excel("tester", "Name");
		// EyeSlack.initialpost(testername ,
		// "https://hooks.slack.com/services/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");
	}

//*************************************************************************************************************
//*	                                Logging in into ABTasty                                                   *
//*************************************************************************************************************

	@Parameters({ "testenvironment" })
	@SuppressWarnings("deprecation")
	@Test(priority = 0)
	public static void Login(String testenv) throws InterruptedException, IOException {

		System.out.println("Login started");

		System.setProperty("webdriver.chrome.driver", chromedriverpath);

		// To check whether to test in staging or production

		testrowno = "1st";
		testenvironment = testenv;

		// String testtype = CommonLibrary.input_data_excel(testrowno, "typeofenv");
		if (testenv.equalsIgnoreCase("staging")) {
			// Initialize browser
			ChromeOptions options = new ChromeOptions();
			// options.addArguments("-incognito");
			options.setExperimentalOption("useAutomationExtension", false);
			options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));

			// to start chrome in headless mode
			options.addArguments("headless");
			options.addArguments("--window-size=1920,1080");
			options.addArguments("--no-sandbox");
			options.addArguments("--disable-dev-shm-usage");

			// DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			// capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			// driver=new ChromeDriver(capabilities);
			// options.addArguments(browserpath);
			System.out.println("staging");

			// Open the editor
			driver = new ChromeDriver(options);
			driver.get("https://staging-app2.abtasty.com/experiments");
			System.out.println("login " + driver.getCurrentUrl());
			if (driver.getPageSource().contains("403 ERROR")) {
				System.out.println("403 error on staging");
			}
		} else {
			// closing the extra line at top
			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("useAutomationExtension", false);
			options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
			// to start chrome in headless mode
			options.addArguments("headless");
			options.addArguments("--window-size=1920,1080");
			options.addArguments("--no-sandbox");
			options.addArguments("--disable-dev-shm-usage");

			driver = new ChromeDriver(options);
			driver.get("https://app2.abtasty.com/experiments");
			System.out.println("prod");
			System.out.println("login " + driver.getCurrentUrl());
		}
		// deriving the browser and os
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		browserName = caps.getBrowserName();
		String browserVersion = caps.getVersion();

		// Get OS name.
		os = System.getProperty("os.name").toLowerCase();
		System.out.println("browserName" + browserName + "browserVersion" + browserVersion + "os" + os);

		// login to the editor
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.id()).click();
		driver.findElement(ObjectLibrary.id()).sendKeys(CommonLibrary.main_data_excel("tester", "userid"));
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.password()).click();
		driver.findElement(ObjectLibrary.password()).sendKeys(CommonLibrary.main_data_excel("tester", "password"));
		driver.findElement(ObjectLibrary.signIn()).click();
		driver.manage().window().maximize();

		System.out.println("Login successfully done");
	}

//*************************************************************************************************************
//*	                                Changing account names                                                    *
//*************************************************************************************************************

	@Test(priority = 1)
	public static void selectAccount() throws Exception {

		System.out.println("select account names started");

		// Locate 'Account Names' drop down
		Thread.sleep(4000);
		if (driver.getPageSource().contains("Welcome to the new navigation experience!")) {
			driver.findElement(By.xpath("//*[@data-testid=\"onboardingModalCloseIcon\"]")).click();
		}
		// change account
		CommonLibrary.ChangeAccount(driver, "Mathumitha Murugan");
		Thread.sleep(1000);

		System.out.println("Select account names successfully done");
	}

//*************************************************************************************************************
//*	                             Test for Doesnt end scheduler option    	 				                  *
//*************************************************************************************************************

	@Test(priority = 2)
	public void defaultscheduler() throws Exception {

		System.out.println("  Test for Doesnt end scheduler option started");

		// click play/pause
		driver.findElement(By.xpath("//div[1]/div[10]/div/div[@class=\'StatusButton__statusButton___2VxJ2\']")).click();

		// click Schedule
		driver.findElement(By.xpath("//div[contains(text(),'Schedule')]")).click();

		// Turn ON start date
		driver.findElement(By.xpath("//div[@data-testid='switch-start']")).click();

		// Turn ON end date
		driver.findElement(By.xpath("//div[@data-testid='switch-end']")).click();

		// click save
		driver.findElement(By.xpath("//button[normalize-space()='Save']")).click();

		// Check success msg
		WebElement sucessToast = driver.findElement(By.xpath("//div[@class='Toast__text___2I_H0']"));
		AssertJUnit.assertEquals(true, sucessToast.isDisplayed());
		Thread.sleep(500);

		// close success msg
		driver.findElement(By.xpath("//div[@class='Toast__toast___1gBoS Toast__success___WkLAW']//*[name()='svg']"))
				.click();

		boolean scheduler = driver.findElement(By.xpath("//div[1]/div[10]//*[@data-testid='scheduleIcon']"))
				.isDisplayed();

		if (scheduler) {
			System.out.println("scheduler is displayed");
		}
		System.out.println("  Test for Doesnt end scheduler option ended");
	}
//*************************************************************************************************************
//*	                             Test for edit Doesnt end scheduler option    	 				              *
//*************************************************************************************************************

		@Test(priority = 3)
	public void editScheduler() throws Exception {

		System.out.println("  Test for edit Doesnt end scheduler option started");
		
		// click play/pause
		driver.findElement(By.xpath("//div[1]/div[10]/div/div[@class=\'StatusButton__statusButton___2VxJ2\']")).click();

		// click Schedule
		driver.findElement(By.xpath("//div[contains(text(),'Schedule')]")).click();

		// Turn off end date
		driver.findElement(By.xpath("//div[@data-testid='switch-end']")).click();

		// click Edit
		driver.findElement(By.xpath("//button[normalize-space()='Edit']")).click();

		// Check success msg
		WebElement sucessToast = driver.findElement(By.xpath("//div[@class='Toast__text___2I_H0']"));
		AssertJUnit.assertEquals(true, sucessToast.isDisplayed());
		Thread.sleep(500);

		// close success msg
		driver.findElement(By.xpath("//div[@class='Toast__toast___1gBoS Toast__success___WkLAW']//*[name()='svg']"))
				.click();

		// to perform Scroll on application using Selenium
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,350)", "");

		// click play/pause
		driver.findElement(By.xpath("//div[1]/div[10]/div/div[@class=\'StatusButton__statusButton___2VxJ2\']")).click();

		// click Schedule
		driver.findElement(By.xpath("//div[contains(text(),'Schedule')]")).click();

		// verify end date disabled
		boolean endTime = driver
				.findElement(By.xpath("//div[@data-testid='end-bloc']/div/div/label/input[@type='time']")).isEnabled();
		if (!endTime) {
			System.out.println("endTime is disbled");

		}
		// Click close
		driver.findElement(By.xpath("//*[@data-testid='closeIcon']")).click();

		// verify scheduler icon
		boolean scheduler = driver.findElement(By.xpath("//div[1]/div[10]//*[@data-testid='scheduleIcon']"))
				.isDisplayed();
		if (scheduler) {
			System.out.println("scheduler is displayed");
		}
		System.out.println("  Test for edit Doesnt end scheduler option ended");
	}
		
//*************************************************************************************************************
//*	                             Test for delete Doesnt end scheduler option    	 				          *
//*************************************************************************************************************

		
	@Test(priority = 4)
	public void deleteScheduler() throws Exception {
		
		System.out.println("  Test for delete Doesnt end scheduler option started");

		Thread.sleep(1000);

		// click play/pause
		driver.findElement(By.xpath("//div[1]/div[10]/div/div[@class=\'StatusButton__statusButton___2VxJ2\']")).click();

		// click Schedule
		driver.findElement(By.xpath("//div[contains(text(),'Schedule')]")).click();

		// click delete
		driver.findElement(By.xpath("//button[normalize-space()='Stop schedule']")).click();

		// Check success msg
		WebElement sucessToast = driver.findElement(By.xpath("//div[@class='Toast__text___2I_H0']"));
		AssertJUnit.assertEquals(true, sucessToast.isDisplayed());
		Thread.sleep(500);

		// close success msg
		driver.findElement(By.xpath("//div[@class='Toast__toast___1gBoS Toast__success___WkLAW']//*[name()='svg']"))
				.click();

		// verify scheduleIcon
		List<WebElement> s = driver.findElements(By.xpath("//div[1]/div[10]//*[@data-testid='scheduleIcon']"));

		if (s.size() > 0) {
			System.out.println("scheduler is displayed");
		} else {
			System.out.println("scheduler not is displayed");
		}
		System.out.println("  Test for delete Doesnt end scheduler option ended");
	}

//*************************************************************************************************************
//*	                             Test for recurrenceScheduler option    	 				                  *
//*************************************************************************************************************

	@Test(priority = 5)
	public void recurrenceScheduler() throws Exception {
		
		System.out.println(" Test for recurrence Scheduler option  started");

		Thread.sleep(500);

		// click play/pause
		driver.findElement(By.xpath("//div[1]/div[10]/div/div[@class=\'StatusButton__statusButton___2VxJ2\']")).click();

		// click Schedule
		driver.findElement(By.xpath("//div[contains(text(),'Schedule')]")).click();

		driver.findElement(By.xpath("//span[normalize-space()=\"Doesn't repeat\"]")).click();

		driver.findElement(By.xpath("//div[contains(text(),'Every day')]")).click();

		driver.findElement(By.xpath("//button[normalize-space()='Save']")).click();

		// Check success msg
		WebElement sucessToast = driver.findElement(By.xpath("//div[@class='Toast__text___2I_H0']"));
		AssertJUnit.assertEquals(true, sucessToast.isDisplayed());

		boolean scheduler = driver.findElement(By.xpath("//div[1]/div[10]//*[@data-testid='scheduleIcon']"))
				.isDisplayed();
		if (scheduler) {
			System.out.println("scheduler is displayed");
		}
		System.out.println(" Test for recurrence Scheduler option  ended");
	}

//*************************************************************************************************************
//*	                             Test for edit recurrence Scheduler option    	 				              *
//*************************************************************************************************************
	@Test(priority = 6)
	public void editRecurrenceScheduler() throws Exception {
		
		System.out.println(" Test for edit recurrence Scheduler option  started");

		// click play/pause
		driver.findElement(By.xpath("//div[1]/div[10]/div/div[@class=\'StatusButton__statusButton___2VxJ2\']")).click();

		// click Schedule
		driver.findElement(By.xpath("//div[contains(text(),'Schedule')]")).click();

		// Click doesn't end
		driver.findElement(By.xpath("//span[normalize-space()='Does not end']")).click();

		// click Edit
		driver.findElement(By.xpath("//button[normalize-space()='Edit']")).click();

		// Check success msg
		WebElement sucessToast = driver.findElement(By.xpath("//div[@class='Toast__text___2I_H0']"));
		AssertJUnit.assertEquals(true, sucessToast.isDisplayed());
		Thread.sleep(1000);

		// close success msg
		driver.findElement(By.xpath("//div[@class='Toast__toast___1gBoS Toast__success___WkLAW']//*[name()='svg']"))
				.click();

		// click play/pause
		driver.findElement(By.xpath("//div[1]/div[10]/div/div[@class=\'StatusButton__statusButton___2VxJ2\']")).click();

		// click Schedule
		driver.findElement(By.xpath("//div[contains(text(),'Schedule')]")).click();

		WebElement radioElement = driver
				.findElement(By.xpath("//div[3]/div/label/input[@class='RadioBoxOption__radioButton___2lVxW']"));
		boolean selectState = radioElement.isSelected();
		if (selectState) {
			System.out.println("Does not end is selected");
		}

		// Click close
		driver.findElement(By.xpath("//*[@data-testid='closeIcon']")).click();

		// verify scheduler icon
		boolean scheduler = driver.findElement(By.xpath("//div[1]/div[10]//*[@data-testid='scheduleIcon']"))
				.isDisplayed();
		if (scheduler) {
			System.out.println("scheduler is displayed");
		}
		System.out.println(" Test for edit recurrence Scheduler option  ended");
	}

//*************************************************************************************************************
//*	                             Test for delete recurrence Scheduler option    	 				           *
//*************************************************************************************************************
	@Test(priority = 7)
	public void deleteRecurrenceScheduler() throws Exception {
		
		System.out.println(" Test for delete recurrence Scheduler option  ended");

		Thread.sleep(500);

		// click play/pause
		driver.findElement(By.xpath("//div[1]/div[10]/div/div[@class=\'StatusButton__statusButton___2VxJ2\']")).click();

		// click Schedule
		driver.findElement(By.xpath("//div[contains(text(),'Schedule')]")).click();

		// click delete
		driver.findElement(By.xpath("//button[normalize-space()='Stop schedule']")).click();

		// Check success msg
		WebElement sucessToast = driver.findElement(By.xpath("//div[@class='Toast__text___2I_H0']"));
		AssertJUnit.assertEquals(true, sucessToast.isDisplayed());
		Thread.sleep(500);

		// close success msg
		driver.findElement(By.xpath("//div[@class='Toast__toast___1gBoS Toast__success___WkLAW']//*[name()='svg']"))
				.click();

		// verify scheduleIcon
		List<WebElement> s = driver.findElements(By.xpath("//div[1]/div[10]//*[@data-testid='scheduleIcon']"));

		if (s.size() > 0) {
			System.out.println("scheduler is displayed");
		} else {
			System.out.println("scheduler not is displayed");
		}
		System.out.println(" Delete Recurrence Scheduler Test ended");
	}

//*************************************************************************************************************
//*	                                Closing the drive                                        	              *
//*************************************************************************************************************	

	@AfterSuite
	public void closedriver() throws IOException {
		driver.quit();
	}

}
