package regression;

import org.testng.annotations.Test;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

import java.io.IOException;
import java.util.Collections;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import pageObject.ObjectLibrary;

//*************************************************************************************************************
//*	                                AB Duplication to different account     		                           *
//*************************************************************************************************************

public class DuplicationABTest2 {

    static WebDriver driver;
    private By by;
    static String parentWindow;
    public static String ReportURL;
    static String variationid;
    static String Customerid;
    static String testrowno;
    static String typeoftest;
    static String testenvironment;
    public static String browserName;
    public static String os;
    //static String chromedriverpath = "/usr/local/bin/chromedriver";
    static String chromedriverpath = "C:\\abTastySelenium\\drivers\\chromedriver.exe";
    static String browserpath = "--user-data-dir=/Users/monikapanda/Documents/Selenium/BrowserProfile/Profilenew";
    static String excelpath = System.getProperty("user.dir") + "/ABTastyData1.xlsx";

    @BeforeSuite
    public static void initialslackmsg() throws IOException {
        String testername = CommonLibrary.main_data_excel("tester", "Name");
        // EyeSlack.initialpost(testername ,
        // "https://hooks.slack.com/services/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");
    }

//*************************************************************************************************************
//*	                                Logging in into ABTasty                                                   *
//*************************************************************************************************************

    @Parameters({"testenvironment"})
    @SuppressWarnings("deprecation")
    @Test(priority = 0)
    public static void Login(String testenv) throws InterruptedException, IOException {

        System.out.println("Login started");

        System.setProperty("webdriver.chrome.driver", chromedriverpath);

        // To check whether to test in staging or production

        testrowno = "1st";
        testenvironment = testenv;

        // String testtype = CommonLibrary.input_data_excel(testrowno, "typeofenv");
        if (testenv.equalsIgnoreCase("staging")) {
            // Initialize browser
            ChromeOptions options = new ChromeOptions();
            // options.addArguments("-incognito");
            options.setExperimentalOption("useAutomationExtension", false);
            options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));

            // to start chrome in headless mode
            options.addArguments("headless");
            options.addArguments("--window-size=1920,1080");
            options.addArguments("--no-sandbox");
            options.addArguments("--disable-dev-shm-usage");

            // DesiredCapabilities capabilities = DesiredCapabilities.chrome();
            // capabilities.setCapability(ChromeOptions.CAPABILITY, options);
            // driver=new ChromeDriver(capabilities);
            // options.addArguments(browserpath);
            System.out.println("staging");

            // Open the editor
            driver = new ChromeDriver(options);
            driver.get("https://staging-app2.abtasty.com/experiments");
            System.out.println("login " + driver.getCurrentUrl());
            if (driver.getPageSource().contains("403 ERROR")) {
                System.out.println("403 error on staging");
            }
        } else {
            // closing the extra line at top
            ChromeOptions options = new ChromeOptions();
            options.setExperimentalOption("useAutomationExtension", false);
            options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
            // to start chrome in headless mode
            options.addArguments("headless");
            options.addArguments("--window-size=1920,1080");
            options.addArguments("--no-sandbox");
            options.addArguments("--disable-dev-shm-usage");

            driver = new ChromeDriver(options);
            driver.get("https://app2.abtasty.com/experiments");
            System.out.println("prod");
            System.out.println("login " + driver.getCurrentUrl());
        }
        // deriving the browser and os
        Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
        browserName = caps.getBrowserName();
        String browserVersion = caps.getVersion();

        // Get OS name.
        os = System.getProperty("os.name").toLowerCase();
        System.out.println("browserName" + browserName + "browserVersion" + browserVersion + "os" + os);

        // login to the editor
        Thread.sleep(1000);
        driver.findElement(ObjectLibrary.id()).click();
        driver.findElement(ObjectLibrary.id()).sendKeys(CommonLibrary.main_data_excel("tester", "userid"));
        Thread.sleep(1000);
        driver.findElement(ObjectLibrary.password()).click();
        driver.findElement(ObjectLibrary.password()).sendKeys(CommonLibrary.main_data_excel("tester", "password"));
        driver.findElement(ObjectLibrary.signIn()).click();
        driver.manage().window().maximize();

        System.out.println("Login successfully done");
    }

//*************************************************************************************************************
//*	                                Changing account names                                                    *
//*************************************************************************************************************

    @Test(priority = 1)
    public static void selectAccount() throws Exception {

        System.out.println("select account names started");

        // Locate 'Account Names' drop down
        Thread.sleep(4000);
        if (driver.getPageSource().contains("Welcome to the new navigation experience!")) {
            driver.findElement(By.xpath("//*[@data-testid=\"onboardingModalCloseIcon\"]")).click();
        }
        // change account
        CommonLibrary.ChangeAccount(driver, "Mathumitha");

        if (driver.getPageSource().contains("Mathumitha MURUGAN")) {
            System.out.println("Given Account selected");
        } else {
            System.out.println("Selected other Account");
        }

        System.out.println("Select account names successfully done");
    }

//*************************************************************************************************************
//*	                                AB To AB Duplication       		                            	          *
//*************************************************************************************************************

    @Test(priority = 2)
    public void ABToAB() throws Exception {

        System.out.println("checking.......AB To AB Duplication 2 ");

        // Scroll window by ('0','287')
        Thread.sleep(500);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,287)", "");

        System.out.println("check.......scroll");

        // click filter
        driver.findElement(By.xpath("//button[contains(text(),'Filter')]")).click();
        // Click A/B Test
        driver.findElement(By.xpath("//span[normalize-space()='A/B Test']")).click();

        driver.findElement(By.xpath("//button[normalize-space()='Apply']")).click();
        Thread.sleep(2000);

        // click 'action menu(3 dots)'
        CommonLibrary.hoverAndClick(driver, driver.findElement(By.xpath("//*[@data-testid=\"moreActions\"]")),
                driver.findElement(By.xpath("//*[@data-testid=\"moreActions\"]")));

        // Click 'Duplicate'
        Thread.sleep(500);
        by = By.xpath("//div[1]/div[9]/div/div[2]//div[2][. = 'Duplicate']");
        driver.findElement(by).click();

        // Click 'cancel'
        Thread.sleep(500);
        by = By.xpath("//button[contains(text(),'Cancel')]");
        driver.findElement(by).click();
        System.out.println("check.......Cancel");

        // click 'action menu(3 dots)'
        CommonLibrary.hoverAndClick(driver, driver.findElement(By.xpath("//*[@data-testid=\"moreActions\"]")),
                driver.findElement(By.xpath("//*[@data-testid=\"moreActions\"]")));

        // Click 'Duplicate'
        driver.findElement(By.xpath("//*[(text()='Duplicate')]")).click();

        // change account for duplication
        CommonLibrary.DuplicationChangeAccount(driver, "Jérémie Franchomme");

        //getCurrentDateTime
        String currentdate = CommonLibrary.getCurrentDateTime();
        System.out.println(currentdate);

        String ABTestDuplicate = "ABTestDuplicate" + currentdate;

        // clear and enter name
        Thread.sleep(500);
        by = By.xpath("//div[2]/label[1]/input[@class = 'Input__commonInput___35r8X']");
        driver.findElement(by).clear();
        driver.findElement(by).sendKeys(ABTestDuplicate);

        // Click 'Duplicate'
        Thread.sleep(500);
        by = By.xpath("//button[2][. = 'Duplicate']");
        driver.findElement(by).click();

        // Click 'success
        Thread.sleep(500);
        by = By.xpath("//ul/div");
        driver.findElement(by).click();

        // change account
        CommonLibrary.ChangeAccount(driver, "Jérémie Franchomme");

        // Scroll window by ('0','287')
        Thread.sleep(500);
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollBy(0,287)", "");

        // Is ABTestDuplicate present?
        if (driver.getPageSource().contains(ABTestDuplicate)) {
            System.out.println("AB to AB Duplicated Test is visible");

        } else {
            System.out.println("AB to AB Duplicated Test is not visible");
        }

        System.out.println("AB To AB Duplication2 ended");
    }

//*************************************************************************************************************
//*	                                AB To MultiPageTest Duplication       		                              *
//*************************************************************************************************************

    @Test(priority = 3)
    public void ABToMPTest() throws Exception {

        System.out.println("checking.......ABToMPTest");

        // change account
        CommonLibrary.ChangeAccount(driver, "Mathumitha Murugan");

        // click filter
        driver.findElement(By.xpath("//button[contains(text(),'Filter')]")).click();
        // Click A/B Test
        driver.findElement(By.xpath("//span[normalize-space()='A/B Test']")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//button[normalize-space()='Apply']")).click();
        Thread.sleep(2000);

        // click 'action menu(3 dots)'
        CommonLibrary.hoverAndClick(driver, driver.findElement(By.xpath("//*[@data-testid=\"moreActions\"]")),
                driver.findElement(By.xpath("//*[@data-testid=\"moreActions\"]")));

        // Click 'Duplicate'
        Thread.sleep(500);
        by = By.xpath("//div[1]/div[9]/div/div[2]//div[2][. = 'Duplicate']");
        driver.findElement(by).click();

        // Click 'dropdown'
        Thread.sleep(500);
        by = By.xpath("//div[1]/div[2][@data-testid = 'subType-dropdown']");
        driver.findElement(by).click();
        System.out.println("check.......dropdown");

        // Click Multipage test
        Thread.sleep(500);
        by = By.xpath("//div[contains(text(), 'Multipage test')]");
        driver.findElement(by).click();

        // change account for duplication
        CommonLibrary.DuplicationChangeAccount(driver, "Jérémie Franchomme");

        //getCurrentDateTime
        String currentdate = CommonLibrary.getCurrentDateTime();
        System.out.println(currentdate);

        String ABToMPTest = "ABToMPTest" + currentdate;

        // clear and enter name
        Thread.sleep(500);
        by = By.xpath("//div[2]/label[1]/input[@class = 'Input__commonInput___35r8X']");
        driver.findElement(by).clear();
        Thread.sleep(500);
        driver.findElement(by).sendKeys(ABToMPTest);

        // Click 'Duplicate'
        Thread.sleep(500);
        by = By.xpath("//button[2][. = 'Duplicate']");
        driver.findElement(by).click();

//		// Click 'success
//		Thread.sleep(500);
//		by = By.xpath("//ul/div");
//		driver.findElement(by).click();

        // change account
        CommonLibrary.ChangeAccount(driver, "Jérémie Franchomme");

        // Scroll window by ('0','-287')
        Thread.sleep(500);
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,-287)", "");

        // Is ABToMPTest present?
        if (driver.getPageSource().contains(ABToMPTest)) {
            System.out.println("AB To MultiPage Duplicated Test is visible");
        } else {
            System.out.println("AB To MultiPage Duplicated Test is not visible");
        }

        System.out.println("AB To MPTest ended");
    }

//*************************************************************************************************************
//*	                                AB To PatchTest Duplication       	    	                              *
//*************************************************************************************************************

    @Test(priority = 4)
    public void ABToPatchTest() throws Exception {

        System.out.println("checking.......ABToPatchTest");

        // change account
        CommonLibrary.ChangeAccount(driver, "Mathumitha Murugan");

        // click filter
        driver.findElement(By.xpath("//button[contains(text(),'Filter')]")).click();
        // Click A/B Test
        driver.findElement(By.xpath("//span[normalize-space()='A/B Test']")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//button[normalize-space()='Apply']")).click();
        Thread.sleep(2000);

        // click 'action menu(3 dots)'
        CommonLibrary.hoverAndClick(driver, driver.findElement(By.xpath("//*[@data-testid=\"moreActions\"]")),
                driver.findElement(By.xpath("//*[@data-testid=\"moreActions\"]")));

        // Click 'Duplicate'
        driver.findElement(By.xpath("//*[(text()='Duplicate')]")).click();

        // Click 'dropdown'
        Thread.sleep(500);
        by = By.xpath("//div[1]/div[2][@data-testid = 'subType-dropdown']");
        driver.findElement(by).click();
        System.out.println("check.......dropdown");

        // Click Patch test
        Thread.sleep(500);
        by = By.xpath("//div[contains(text(), 'Patch')]");
        driver.findElement(by).click();

        // change account for duplication
        CommonLibrary.DuplicationChangeAccount(driver, "Jérémie Franchomme");


        //getCurrentDateTime
        String currentdate = CommonLibrary.getCurrentDateTime();
        System.out.println(currentdate);

        String ABToPatchDuplicate = "ABToPatchDuplicate" + currentdate;

        // clear and enter name
        by = By.xpath("//div[2]/label[1]/input[@class = 'Input__commonInput___35r8X']");
        Thread.sleep(500);
        driver.findElement(by).clear();
        Thread.sleep(500);
        driver.findElement(by).sendKeys(ABToPatchDuplicate);

        // Click 'Duplicate'
        Thread.sleep(500);
        by = By.xpath("//button[2][. = 'Duplicate']");
        driver.findElement(by).click();

//		// Click 'success
//		Thread.sleep(500);
//		by = By.xpath("//ul/div");
//		driver.findElement(by).click();

        // change account
        CommonLibrary.ChangeAccount(driver, "Jérémie Franchomme");

        // Scroll window by ('0','-287')
        Thread.sleep(500);
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,287)", "");

        // Is ABToPatchDuplicatepresent?
        if (driver.getPageSource().contains(ABToPatchDuplicate)) {
            System.out.println("AB to Patch Duplicated test is Visible");
        } else {
            System.out.println("AB to Patch Duplicated test is InVisible");
        }
        System.out.println("AB To Patch Test Duplication ended");
    }

//*************************************************************************************************************
//*	                                AB To SimplePerso Duplication       	    	                          *
//*************************************************************************************************************

    @Test(priority = 5)
    public void ABToSimplePerso() throws Exception {

        System.out.println("checking.......ABToSimplePerso");

        // change account
        CommonLibrary.ChangeAccount(driver, "Mathumitha Murugan");

        // click filter
        driver.findElement(By.xpath("//button[contains(text(),'Filter')]")).click();
        // Click A/B Test
        driver.findElement(By.xpath("//span[normalize-space()='A/B Test']")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//button[normalize-space()='Apply']")).click();
        Thread.sleep(2000);

        // click 'action menu(3 dots)'
        CommonLibrary.hoverAndClick(driver, driver.findElement(By.xpath("//*[@data-testid=\"moreActions\"]")),
                driver.findElement(By.xpath("//*[@data-testid=\"moreActions\"]")));

        // Click 'Duplicate'
        driver.findElement(By.xpath("//*[(text()='Duplicate')]")).click();

        // Click 'dropdown'
        Thread.sleep(500);
        by = By.xpath("//div[1]/div[2][@data-testid = 'subType-dropdown']");
        driver.findElement(by).click();
        System.out.println("check.......dropdown");

        // Click Simple personalization
        Thread.sleep(500);
        by = By.xpath("//div[contains(text(), 'Simple personalization')]");
        driver.findElement(by).click();

        // change account for duplication
        CommonLibrary.DuplicationChangeAccount(driver, "Jérémie Franchomme");

        //getCurrentDateTime
        String currentdate = CommonLibrary.getCurrentDateTime();
        System.out.println(currentdate);

        String ABToSimplePersoDuplicate = "ABToSimplePersoDuplicate" + currentdate;

        // clear and enter name
        by = By.xpath("//div[2]/label[1]/input[@class = 'Input__commonInput___35r8X']");
        Thread.sleep(500);
        driver.findElement(by).clear();
        Thread.sleep(500);
        driver.findElement(by).sendKeys(ABToSimplePersoDuplicate);

        // Click 'Duplicate'
        Thread.sleep(500);
        by = By.xpath("//button[2][. = 'Duplicate']");
        driver.findElement(by).click();

//		// Click 'success
//		Thread.sleep(500);
//		by = By.xpath("//ul/div");
//		driver.findElement(by).click();

        // change account
        CommonLibrary.ChangeAccount(driver, "Jérémie Franchomme");

        // click perso
        driver.findElement(By.xpath("//div[contains(text(), 'Personalization')]")).click();

        // Scroll window by ('0','287')
        Thread.sleep(500);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,287)", "");

        // Is 'ABToSimplePersoDuplicate' present?
        if (driver.getPageSource().contains(ABToSimplePersoDuplicate)) {
            System.out.println("Duplicated Simple Perso is visible");
        } else {
            System.out.println("Duplicated Simple Perso is not visible");
        }
        System.out.println("AB To SimplePerso Duplication ended");
    }

//*************************************************************************************************************
//*	                                AB To MultiPage Perso Duplication       	    	                      *
//*************************************************************************************************************

    @Test(priority = 6)
    public void ABToMPPerso() throws Exception {

        System.out.println("checking.......ABToMPPerso");

        // change account
        CommonLibrary.ChangeAccount(driver, "Mathumitha Murugan");


        // click filter
        driver.findElement(By.xpath("//button[contains(text(),'Filter')]")).click();
        // Click A/B Test
        driver.findElement(By.xpath("//span[normalize-space()='A/B Test']")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//button[normalize-space()='Apply']")).click();
        Thread.sleep(2000);

        // click 'action menu(3 dots)'
        CommonLibrary.hoverAndClick(driver, driver.findElement(By.xpath("//*[@data-testid=\"moreActions\"]")),
                driver.findElement(By.xpath("//*[@data-testid=\"moreActions\"]")));


        // Click 'Duplicate'
        driver.findElement(By.xpath("//*[(text()='Duplicate')]")).click();

        // Click 'dropdown'
        Thread.sleep(500);
        by = By.xpath("//div[1]/div[2][@data-testid = 'subType-dropdown']");
        driver.findElement(by).click();
        System.out.println("check.......dropdown");

        // Click Multipage personalization
        Thread.sleep(500);
        by = By.xpath("//div[contains(text(), 'Multipage personalization')]");
        driver.findElement(by).click();

        // change account for duplication
        CommonLibrary.DuplicationChangeAccount(driver, "Jérémie Franchomme");

        //getCurrentDateTime
        String currentdate = CommonLibrary.getCurrentDateTime();
        System.out.println(currentdate);

        String ABtoMPPDuplicate = "ABtoMPPDuplicate" + currentdate;

        // clear and enter name
        by = By.xpath("//div[2]/label[1]/input[@class = 'Input__commonInput___35r8X']");
        Thread.sleep(500);
        driver.findElement(by).clear();
        Thread.sleep(500);
        driver.findElement(by).sendKeys(ABtoMPPDuplicate);

        // Click 'Duplicate'
        Thread.sleep(500);
        by = By.xpath("//button[2][. = 'Duplicate']");
        driver.findElement(by).click();

//		// Click 'success
//		Thread.sleep(500);
//		by = By.xpath("//ul/div");
//		driver.findElement(by).click();

        // change account
        CommonLibrary.ChangeAccount(driver, "Jérémie Franchomme");

        // click perso
        driver.findElement(By.xpath("//div[contains(text(), 'Personalization')]")).click();

        // Scroll window by ('0','287')
        Thread.sleep(500);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,287)", "");

        // Is 'MultiPagePersoDuplicate' present?
        if (driver.getPageSource().contains(ABtoMPPDuplicate)) {
            System.out.println("AB to Multipage Duplicated Perso is Visible");
        } else {
            System.out.println("AB to Multipage Duplicated Perso is InVisible");
        }

        System.out.println("AB to Multipage Perso Duplication ended");
    }

//*************************************************************************************************************
//*	                                Closing the drive                                        	              *
//*************************************************************************************************************	

    @AfterSuite
    public void closedriver() throws IOException {
        driver.quit();
    }

}