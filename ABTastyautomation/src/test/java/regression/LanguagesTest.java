
package regression;

import org.testng.annotations.Test;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.AssertJUnit;

import java.io.IOException;
import java.util.Collections;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import pageObject.ObjectLibrary;

//*************************************************************************************************************
//*	                               TEst laanguges on the profile page                       		          *
//*************************************************************************************************************
//works only when the account is in English

public class LanguagesTest {

	static WebDriver driver;
	private By by;
	static String parentWindow;
	public static String ReportURL;
	static String variationid;
	static String Customerid;
	static String testrowno;
	static String typeoftest;
	static String testenvironment;
	public static String browserName;
	public static String os;
	//static String chromedriverpath = "/usr/local/bin/chromedriver";
	static String chromedriverpath = "C:\\abTastySelenium\\drivers\\chromedriver.exe";
	static String browserpath = "--user-data-dir=/Users/monikapanda/Documents/Selenium/BrowserProfile/Profilenew";
	static String excelpath = System.getProperty("user.dir") + "/ABTastyData1.xlsx";

	@BeforeSuite
	public static void initialslackmsg() throws IOException {
		String testername = CommonLibrary.main_data_excel("tester", "Name");
		// EyeSlack.initialpost(testername ,
		// "https://hooks.slack.com/services/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");
	}

//*************************************************************************************************************
//*	                                Logging in into ABTasty                                                   *
//*************************************************************************************************************

	@Parameters({ "testenvironment" })
	@SuppressWarnings("deprecation")
	@Test(priority = 0)
	public static void Login(String testenv) throws InterruptedException, IOException {

		System.out.println("Login started");

		System.setProperty("webdriver.chrome.driver", chromedriverpath);

		// To check whether to test in staging or production

		testrowno = "1st";
		testenvironment = testenv;

		// String testtype = CommonLibrary.input_data_excel(testrowno, "typeofenv");
		if (testenv.equalsIgnoreCase("staging")) {
			// Initialize browser
			ChromeOptions options = new ChromeOptions();
			// options.addArguments("-incognito");
			options.setExperimentalOption("useAutomationExtension", false);
			options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));

			// to start chrome in headless mode
			options.addArguments("headless");
			options.addArguments("--window-size=1920,1080");
			options.addArguments("--no-sandbox");
			options.addArguments("--disable-dev-shm-usage");

			// DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			// capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			// driver=new ChromeDriver(capabilities);
			// options.addArguments(browserpath);
			System.out.println("staging");

			// Open the editor
			driver = new ChromeDriver(options);
			driver.get("https://staging-app2.abtasty.com/experiments");
			System.out.println("login " + driver.getCurrentUrl());
			if (driver.getPageSource().contains("403 ERROR")) {
				System.out.println("403 error on staging");
			}
		} else {
			// closing the extra line at top
			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("useAutomationExtension", false);
			options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
			// to start chrome in headless mode
			options.addArguments("headless");
			options.addArguments("--window-size=1920,1080");
			options.addArguments("--no-sandbox");
			options.addArguments("--disable-dev-shm-usage");

			driver = new ChromeDriver(options);
			driver.get("https://app2.abtasty.com/experiments");
			System.out.println("prod");
			System.out.println("login " + driver.getCurrentUrl());
		}
		// deriving the browser and os
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		browserName = caps.getBrowserName();
		String browserVersion = caps.getVersion();

		// Get OS name.
		os = System.getProperty("os.name").toLowerCase();
		System.out.println("browserName" + browserName + "browserVersion" + browserVersion + "os" + os);

		// login to the editor
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.id()).click();
		driver.findElement(ObjectLibrary.id()).sendKeys(CommonLibrary.main_data_excel("tester", "userid"));
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.password()).click();
		driver.findElement(ObjectLibrary.password()).sendKeys(CommonLibrary.main_data_excel("tester", "password"));
		driver.findElement(ObjectLibrary.signIn()).click();
		driver.manage().window().maximize();

		System.out.println("Login successfully done");
	}

//*************************************************************************************************************
//*	                                Changing account names                                                    *
//*************************************************************************************************************

	@Test(priority = 1)
	public static void selectAccount() throws Exception {

		System.out.println("select account names started");

		// Locate 'Account Names' drop down
		Thread.sleep(4000);
		if (driver.getPageSource().contains("Welcome to the new navigation experience!")) {
			driver.findElement(By.xpath("//*[@data-testid=\"onboardingModalCloseIcon\"]")).click();
		}
		// change account
		CommonLibrary.ChangeAccount(driver, "Mathumitha Murugan");
		Thread.sleep(1000);

		System.out.println("Select account names successfully done");
	}

//*************************************************************************************************************
//*	                               open settings Page     	       	              						      *
//*************************************************************************************************************


	@Test(priority = 2)
	public void opensettings() throws Exception {
		
		System.out.println("Click AccountIcon");

		// Click 'AccountIcon'
		by = By.xpath("//header/div[3]/div[2]/div[1]/*[1]");
		driver.findElement(by).click();
		
		System.out.println("Click Profile");
		
		// Click 'Profile'
		Thread.sleep(500);
		by = By.xpath("//span[. = 'Profile']");
		driver.findElement(by).click();
		
	}

//*************************************************************************************************************
//*	                              Test French title in profile page   	          						      *
//*************************************************************************************************************
	@Test(priority = 3)
	public void testFrench() throws Exception {
		
		System.out.println("Test French title in profile page started");
		
		// Click 'svg'
		Thread.sleep(500);
		by = By.xpath("//div[2]/*[name()='svg']");
		driver.findElement(by).click();
		
		// Click 'French'
		Thread.sleep(500);
		by = By.xpath("//div[. = 'French']"); // span[. = 'Profile']"
		driver.findElement(by).click();

		// Click 'Save1'
		Thread.sleep(500);
		by = By.xpath("//button[. = 'Save']");
		driver.findElement(by).click();

		Thread.sleep(1000);
		driver.navigate().refresh();

		WebElement element = driver.findElement(By.xpath("//h1[@data-testid = 'title']"));
		String title = element.getText();
		System.out.println(title);
		AssertJUnit.assertEquals("Informations personnelles", title);
		
		System.out.println("Test French title in profile page ended");
	}

//*************************************************************************************************************
//*	                              Test German title in profile page   	          						      *
//*************************************************************************************************************
	@Test(priority = 4)
	public void testGerman() throws Exception {
		
		System.out.println("Test German title in profile page started");
		
		// Click 'svg'
		Thread.sleep(500);
		by = By.xpath("//div[2]/*[name()='svg']");
		driver.findElement(by).click();
		
		// Click 'German'
		Thread.sleep(500);
		by = By.xpath("//div[. = 'Allemand']"); // span[. = 'Profile']"
		driver.findElement(by).click();

		// Click 'Save'
		Thread.sleep(500);
		by = By.xpath("//button[. = 'Enregistrer']");
		driver.findElement(by).click();

		driver.navigate().refresh();

		WebElement element = driver.findElement(By.xpath("//h1[@data-testid = 'title']"));
		String title = element.getText();
		System.out.println(title);
		AssertJUnit.assertEquals("Persönliche Information", title);
		
		System.out.println("Test German title in profile page ended");
	}

//*************************************************************************************************************
//*	                              Test Spanish title in profile page   	          						      *
//*************************************************************************************************************

	@Test(priority = 5)
	public void testSpainish() throws Exception {
		
		System.out.println("Test Spanish title in profile page started");
		
		// Click 'svg'
		Thread.sleep(500);
		by = By.xpath("//div[2]/*[name()='svg']");
		driver.findElement(by).click();
		
		// Click 'spanish'
		Thread.sleep(500);
		by = By.xpath("//div[. = 'Spanisch']");
		driver.findElement(by).click();

		// Click 'Save'
		Thread.sleep(500);
		by = By.xpath("//button[. = 'Speichern']");
		driver.findElement(by).click();

		driver.navigate().refresh();

		WebElement element = driver.findElement(By.xpath("//h1[@data-testid = 'title']"));
		String title = element.getText();
		System.out.println(title);
		AssertJUnit.assertEquals("Información personal", title);
		
		System.out.println("Test German title in profile page ended");
	}
	
//*************************************************************************************************************
//*	                                Closing the drive                                        	              *
//*************************************************************************************************************	

		@AfterSuite
		public void closedriver() throws IOException {
			driver.quit();
		}

	}