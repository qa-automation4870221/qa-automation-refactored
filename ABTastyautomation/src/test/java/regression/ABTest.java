package regression;

import core.BaseTest;
import core.DriverInitializer;
import lombok.SneakyThrows;
import org.testng.annotations.*;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

import testdata.InputData;


public class ABTest extends BaseTest {

    static String parentWindow;
    public static String ReportURL;
    static String variationId;
    private final String customerId = "5f02dd192aceb559426b2173cdd69517";
    static String testrowno;
    static String typeoftest;
    static String testenvironment;
    public static String browserName;
    public static String os;
    //static String chromedriverpath = "/usr/local/bin/chromedriver";
    static String chromedriverpath = "C:\\abTastySelenium\\drivers\\chromedriver.exe";

    static String browserpath = "--user-data-dir=/Users/monikapanda/Documents/Selenium/BrowserProfile/Profilenew";
    static String excelpath = System.getProperty("user.dir") + "/ABTastyData1.xlsx";
    static ExtentTest test;
    static ExtentReports report;

    private InputData testData;

    @BeforeClass
    public void initTestData() {
        testData = InputData.builder()
                .environment("staging")
                .accountName("QA automation")
                .typeOffset("AB Test")
                .build();
    }



    @BeforeTest
    public void auth() {
        if (driver != null) {
            driver.get(DriverInitializer.getInstance().getAppUrl());
            if (!objectLibrary.isElementExist(objectLibrary.authWelcome)) {
                testLoginSuccess();
            }
        }
    }


    @SneakyThrows
    public void testLoginSuccess() {
        testrowno = "1st";
        objectLibrary
                .enterEmail("monika.panda+automation@abtasty.com")
                .enterPassword("D7ECEWzW")
                .clickSignIn()
                .assertElementDisplayed("//div[contains(@class,'UserNameAndRole__welcome')]", "User not authorized");
        objectLibrary
                .selectAccount()
                .isAccSelected();
    }


//*************************************************************************************************************
//*	                                Check A/B Test creation                                                      *
//*************************************************************************************************************

    @Test(priority = 1, description = "Create a usual AB test")
    public void createABtestCheck() {
        System.out.println("Create A/B test campaign started");
        typeoftest = "AB Test";
            objectLibrary
                    .createABTest("AutotestAB","http://abtastylab.com/5f02dd192aceb559426b2173cdd69517/");
           System.out.println("Create A/B test campaign successfully done");

    }


//*************************************************************************************************************
//*	                                Check A/B Test creation                                                      *
//*************************************************************************************************************

    /*@Test(description = "Create a usual AB test")
    public void runAllTests(){
        System.out.println("Create A/B test campaign started");
        typeoftest = "AB Test";
            objectLibrary
                    .createABTest("AutotestAB","http://abtastylab.com/5f02dd192aceb559426b2173cdd69517/")
                    .createBanner()
                    .editStyleCheck()
                    .addTrack("mousedown")
                    .addDwellTimeTracking("Dwell")
                    .createVariation()
                    .addGoals()
                    .selectTargetAllVisitors()
                    .createSegmentGeolocation("Ireland", "Munster", "County Clare", "Aglish")
                    .allocateTrafic("70");
            System.out.println("Create A/B test campaign successfully done");
    }
*/

//*************************************************************************************************************
//*	                                Check Banner Widget creation                                                      *
//*************************************************************************************************************

    @Test(description = "Create Banner")
    public void createBannerCheck() {
        testrowno = "1st";
        objectLibrary
                .editABTest()
                .createBanner();

    }

//*************************************************************************************************************
//*	                                Check editing of Title(h1) style                                                 *
//*************************************************************************************************************

    @Test(description = "Edit title style")
    public void editTitleStyleCheck(){
        testrowno = "1";
        objectLibrary
                .editABTest()
                .editStyleCheck();
    }


//*************************************************************************************************************
//*	                               Adding new tracker                                                *
//*************************************************************************************************************

    @Test(description = "Adding new tracker")
    public void addTrackerCheck(){
        testrowno = "1";
        objectLibrary
                .editABTest()
                .addTrack("mousedown");
    }

//*************************************************************************************************************
//*	                                Adding new dwell time Tracking                                              *
//*************************************************************************************************************

    @Test(description = "Adding new dwell time Tracking")
    public void addDwellTimeTrackingCheck(){
        testrowno = "1";
        objectLibrary
                .editABTest()
                .addDwellTimeTracking("Dwell");
    }

//*************************************************************************************************************
//*	                                Creating new Variation                                             *
//*************************************************************************************************************

    @Test(description = "Adding new variation")
    public void createVariationCheck(){
        testrowno = "1";
        objectLibrary
                .editABTest()
                .createVariation();
    }


//*************************************************************************************************************
//*	                                Create Test Goal                                             *
//*************************************************************************************************************

    @Test()
    public void createGoalCheck(){
        testrowno = "1";
        objectLibrary
                .editABTest()
                .addTrack("mousedown")
                .addGoals();
    }


//*************************************************************************************************************
//*	                                    Select Target Test                                                    *
//*************************************************************************************************************

    @Test()
    public void selectTargetCheck(){
        testrowno = "1";
        objectLibrary
                .editABTest()
                .selectTargetAllVisitors();
    }

//*************************************************************************************************************
//*	                                    Create Target Test                                                    *
//*************************************************************************************************************

    @Test()
    public void createTargetCheck(){
        testrowno = "1";
        objectLibrary
                .editABTest()
                .createSegmentGeolocation("Ireland", "Munster", "County Clare", "Aglish");
    }


//*************************************************************************************************************
//*	                                   Traffic allocation Test                                                   *
//*************************************************************************************************************

    @Test()
    public void changeTraficAllocationCheck(){
        testrowno = "1";
        objectLibrary
                .editABTest()
                .allocateTrafic("70");
    }

    /*
//*************************************************************************************************************
//*	                                Play/Pause the test                                                       *
//*************************************************************************************************************

    @Test(priority = 5)
    public void playTest() throws InterruptedException {

        System.out.println("play test started");
        if (driver.getWindowHandles().size() > 1) {
            //Switch to new window opened
            for (String winHandle : driver.getWindowHandles()) {
                driver.switchTo().window(winHandle);
            }
        }
        Thread.sleep(2000);
        driver.findElement(ObjectLibrary.playtest()).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//*[text()= \"Play\"]")).click();
        parentWindow = driver.getWindowHandle();
        System.out.println("Play test successfully ended");
    }

//*************************************************************************************************************
//*	                                Run API Hits                                                              *
//*************************************************************************************************************

  *//*  @Test(priority = 6)
    public void runAPIHits() throws HeadlessException, UnsupportedFlavorException, IOException, InterruptedException {

        System.out.println("Create api hits started");

        Thread.sleep(1000);
        WebElement element = driver.findElement(By.xpath("//*[@class=\"CampaignInformation__testId___3p_1s\"]"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        element.click();
        String campaignid = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor); // extracting the text that was copied to the clipboard
        System.out.println(campaignid);
        Test_api_Post.APIHitsCollection1(campaignid, variationId, customerId);

        System.out.println("Create api hits successfully ended");
    }*//*

//*************************************************************************************************************
//*	                                Verify the test                                                           *
//*************************************************************************************************************

    @Test(priority = 7)
    public void verifyTest() throws Exception {

        System.out.println("verifying test started");
        Thread.sleep(50000);

        String expectedvalue = "";

        String URL = "http://abtastylab.com/5f02dd192aceb559426b2173cdd69517/";
        ((JavascriptExecutor) driver).executeScript("window.open(arguments[0])", URL);
        //driver.get(URL);
        if (testenvironment.equalsIgnoreCase("staging")) {
            //intercepting network calls
            networkmocking.networkmock(driver);
        }

        int tabno = driver.getWindowHandles().size();
        ArrayList<String> windowHandles = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(windowHandles.get(tabno - 1));

        //refreshing the page
        Thread.sleep(1000);
        System.out.println("verify " + driver.getCurrentUrl());
        Thread.sleep(3000);
        //System.out.println(driver.getPageSource());
        driver.navigate().refresh();
        //driver.findElement(By.xpath("//*[@id=\"ab_widget_container_promotional-banner_17e5aca6_914084\"]/div/div/div/a/span")).click();

        //calls the screenshot function
        Thread.sleep(1000);
        String date = CommonLibrary.getDateTime();
        CommonLibrary.takeSnapShot(driver, "Screenshots" + "/" + "Autotest" + date + ".jpg");
        if (driver.getPageSource().contains("promotional-banner")) {
            expectedvalue = "true";
        }
        Assert.assertEquals(expectedvalue, "true", "Test is failed");

        System.out.println("Verifying test successfully done");
    }

    //*************************************************************************************************************
//*	                                Checking the report                                                       *
//*************************************************************************************************************
    //checking just is reporting page exists
    @Test(priority = 8)
    public void checkReport() throws Exception {

        System.out.println("Check report started");

        Thread.sleep(1000);
        driver.switchTo().window(parentWindow);
        //driver.navigate().back();
        Thread.sleep(1000);
        //click the report button
        driver.findElement(ObjectLibrary.report()).click();
        if (driver.getWindowHandles().size() > 1) {
            //Switch to new window opened
            for (String winHandle : driver.getWindowHandles()) {
                driver.switchTo().window(winHandle);
            }
        }
        ReportURL = driver.getCurrentUrl();
        Thread.sleep(6000);
        //to view the realtime report
        System.out.println(ReportURL);
        if (driver.findElement(ObjectLibrary.realtimereport()).isEnabled()) {
            System.out.println("enabled");
        } else {
            System.out.println("disabled");
        }
        driver.findElement(ObjectLibrary.realtimereport()).click();

//    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(60));
//	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[text()='See current activity']")));
//
//	  driver.findElement(By.xpath("//*[text()='See current activity']")).click();
//      if(driver.getPageSource().contains("PageView")) {
//		//calls the screenshot function
//        Thread.sleep(1000);
//    	String date = CommonLibrary.getDateTime();
//    	CommonLibrary.takeSnapShot(driver,"Screenshots" + "/" + "report"+date +".jpg");
//	}
        //to pause the test
        Thread.sleep(2000);
        driver.switchTo().window(parentWindow);
        Thread.sleep(2000);
        driver.findElement(ObjectLibrary.pausetest()).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//*[text()= \"Pause\"]")).click();

        System.out.println("Check report successfully ended");

    }

//*************************************************************************************************************
//*	                                          Delete Created Campaign                                         *
//*************************************************************************************************************

    @Test(priority = 9)
    public void deleteCreatedCampaign() throws Exception {
        System.out.println("Delete created campaign started");

        // Go to the Dashboard
        driver.findElement(ObjectLibrary.arrowBackToDashbaord()).click();
        Thread.sleep(3000);

        // Test if delete the right campaign
        WebElement createdCampaignName = driver.findElements(ObjectLibrary.allCampaignsNameOnTheDashboard()).get(0);

        Assert.assertTrue(
                createdCampaignName.getText().equals(CommonLibrary.input_data_excel(testrowno, "testname"))
                        ||
                        createdCampaignName.getText().equals("Auto MP Test"));

        // Delete the campaign
        WebElement openActionDropodwn = driver.findElements(ObjectLibrary.moreActions()).get(0);
        openActionDropodwn.click();
        Thread.sleep(500);
        driver.findElement(ObjectLibrary.deleteCampaignFromDashboard()).click();
        Thread.sleep(500);
        driver.findElement(ObjectLibrary.confirmDeleteCampaignOnModal()).click();
        Thread.sleep(500);

        System.out.println("Delete created campaig successfully ended");
    }

//for the 3rd row for multipage test
//
//

    @Parameters({"testenvironment"})
    @SuppressWarnings("deprecation")
    @Test(priority = 19)
    public void Login3rd(String testenv) throws InterruptedException, IOException {

        System.out.println("MP Login started");

        System.setProperty("webdriver.chrome.driver", chromedriverpath);

        //To check whether to test in staging or production

        testrowno = "3rd";
        testenvironment = testenv;

        //String testtype = CommonLibrary.input_data_excel(testrowno, "typeofenv");
        if (testenv.equalsIgnoreCase("staging")) {
            //Initialize browser
            ChromeOptions options = new ChromeOptions();
//			options.addArguments("-incognito");
            options.setExperimentalOption("useAutomationExtension", false);
            options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
            //to start chrome in headless mode
            options.addArguments("headless");
            options.addArguments("--window-size=1920,1080");
            options.addArguments("--start-maximized");
            options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
            options.addArguments("--no-sandbox");
            options.addArguments("--disable-dev-shm-usage");

//			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
//			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
            //  driver=new ChromeDriver(capabilities);
//			options.addArguments(browserpath);
            System.out.println("staging");

            //Open the editor
            driver = new ChromeDriver(options);
            driver.get("https://staging-app2.abtasty.com/");
        } else {
            //closing the extra line at top
            ChromeOptions options = new ChromeOptions();
            options.setExperimentalOption("useAutomationExtension", false);
            options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
            //to start chrome in headless mode
            options.addArguments("headless");
            options.addArguments("--window-size=1920,1080");
            options.addArguments("--no-sandbox");
            options.addArguments("--start-maximized");
            options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
            options.addArguments("--disable-dev-shm-usage");
            options.addArguments("--allow-running-insecure-content");

            driver = new ChromeDriver(options);
            driver.get("https://app2.abtasty.com/experiments");
            System.out.println("prod");
        }
        //deriving the browser and os
        Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
        browserName = caps.getBrowserName();
        String browserVersion = caps.getBrowserVersion();

        //Get OS name.
        os = System.getProperty("os.name").toLowerCase();
        System.out.println("browserName" + browserName + "browserVersion" + browserVersion + "os" + os);

        //login to the editor
        Thread.sleep(2000);
        driver.findElement(ObjectLibrary.id()).click();
        driver.findElement(ObjectLibrary.id()).sendKeys(CommonLibrary.main_data_excel("tester", "userid"));
        Thread.sleep(1000);
        driver.findElement(ObjectLibrary.password()).click();
        driver.findElement(ObjectLibrary.password()).sendKeys(CommonLibrary.main_data_excel("tester", "password"));
        driver.findElement(ObjectLibrary.signIn()).click();
        driver.manage().window().maximize();

        System.out.println("Login successfully done");
    }
//*************************************************************************************************************
//*	                                Changing account names                                                    *
//*************************************************************************************************************

    @Test(priority = 20)
    public void selectAccount3rd() throws InterruptedException, IOException {

        System.out.println("MP Select account started");
        //Locate 'Account Names' drop down
        Thread.sleep(6000);
        if (driver.getPageSource().contains("Welcome to the new navigation experience!")) {
            driver.findElement(By.xpath("//*[@data-testid=\"onboardingModalCloseIcon\"]")).click();
        }
        WebElement accountnames = driver.findElement(ObjectLibrary.accountNames());
        accountnames.click();
        WebElement name = driver.findElement(ObjectLibrary.searchAccName());
        //String accountname = CommonLibrary.input_data_excel(testrowno, "accountname");
        String accountname = "QA Automation";
        name.sendKeys(accountname);
        Thread.sleep(1000);
        //Actions actions = new Actions(driver);
        //actions.keyDown(Keys.COMMAND);
        //driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/nav/div[2]/div[2]/div/div/div/div[2]/div")).click();
        //name.sendKeys(Keys.RETURN);
        name.sendKeys(Keys.ARROW_DOWN, Keys.RETURN);
        //if (CommonLibrary.input_data_excel(testrowno, "accountname").equals("QA Automation")) {
        //   customerId = "5f02dd192aceb559426b2173cdd69517";
        //  }

        Thread.sleep(2000);
        //closing the pop in
        if (driver.getPageSource().contains("stroke: rgb(92, 92, 92);")) {
            driver.findElement(By.xpath("/html/body/abtasty-modal/div/div")).click();
        }
        System.out.println("MP Select account successfully ended");
    }

//*************************************************************************************************************
//*	                                Create MP Test                                                            *
//*************************************************************************************************************

    @Test(priority = 21)
    public void createconditions3rd() throws IOException, InterruptedException {

        System.out.println("MP create campaign started");
        typeoftest = CommonLibrary.input_data_excel(testrowno, "typeoftest");
        if (typeoftest.equalsIgnoreCase("Multipage test")) {
            createMultipageTest3rd();
        }
        System.out.println("MP create campaign successfully ended");
    }

//*************************************************************************************************************
//*	                                Create Multipage Test                                                     *
//*************************************************************************************************************

    public void createMultipageTest3rd() throws IOException, InterruptedException {

        if (driver.getWindowHandles().size() > 1) {
            System.out.println(driver.getWindowHandles().size());
            //Switch to new window opened
            for (String winHandle : driver.getWindowHandles()) {
                driver.switchTo().window(winHandle);
            }
        }

        //for creating a Multipage test/campaign
        Thread.sleep(3000);
        driver.findElement(ObjectLibrary.createCampaignButton()).click();
        Thread.sleep(2000);
        driver.findElement(ObjectLibrary.createMPTestButton()).click();
        Thread.sleep(2000);
        driver.findElement(ObjectLibrary.allTestsNameField()).sendKeys("Auto MP Test");

//	if(os.contains("linux"))
//	{
//	driver.findElement(objectlibrary.MPtestname1()).sendKeys("test1");
//	driver.findElement(objectlibrary.MPTestURL1()).click();
//	Thread.sleep(1000);
//	//copying the data to clipboard
//	String str = "http://abtastylab.com/023c2cee0f06c645d5c3d28e61b2e773/";
//	Toolkit toolkit = Toolkit.getDefaultToolkit();
//	Clipboard clipboard = toolkit.getSystemClipboard();
//	StringSelection strSel = new StringSelection(str);
//	clipboard.setContents(strSel, null);
//
//	//pasting it
//	driver.findElement(objectlibrary.MPTestURL1()).sendKeys(Keys.chord(Keys.CONTROL, "v"));
//
//	driver.findElement(objectlibrary.MPtestname2()).sendKeys("test2");
//	driver.findElement(objectlibrary.MPTestURL2()).click();
//	Thread.sleep(1000);
//	//copying the data to clipboard
//    str = "http://abtastylab.com/023c2cee0f06c645d5c3d28e61b2e773/destinations.php";
//	clipboard = toolkit.getSystemClipboard();
//	strSel = new StringSelection(str);
//	clipboard.setContents(strSel, null);
//
//	//pasting it
//	driver.findElement(objectlibrary.MPTestURL2()).sendKeys(Keys.chord(Keys.CONTROL, "v"));
//
//	driver.findElement(objectlibrary.saveMPtest()).click();
//	}
//
//	else {
        WebElement element = driver.findElement(ObjectLibrary.urlInMainInformation());
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        element.sendKeys("http://abtastylab.com/5f02dd192aceb559426b2173cdd69517/");
        driver.findElement(ObjectLibrary.firstMPTestNameField1()).sendKeys("test1");

        element = driver.findElement(ObjectLibrary.secondUrlInMainInformation2());
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        //element.sendKeys("http://abtastylab.com/5f02dd192aceb559426b2173cdd69517/destinations.php");
        element.sendKeys("http://abtastylab.com/5f02dd192aceb559426b2173cdd69517/checkout.php");
        driver.findElement(ObjectLibrary.secondMPTestNameField2()).sendKeys("test2");
        //driver.findElement(objectlibrary.saveMPtest()).click();
        WebElement ele = driver.findElement(ObjectLibrary.saveAndGoButton());
        driver.executeScript("arguments[0].click();", ele);
//	}

        //create variation for multipage 1
        Thread.sleep(12000);

        //Changing background color of a literal
        driver.switchTo().frame(0);
        driver.findElement(ObjectLibrary.Elementtobechanged()).click();
        driver.switchTo().defaultContent();
        Thread.sleep(1000);
        WebElement menu = driver.findElement(ObjectLibrary.Edit());
        Actions act = new Actions(driver);
        act.moveToElement(menu).perform();
        Thread.sleep(1000);
       // CommonLibrary.hoverAndClick(driver, driver.findElement(ObjectLibrary.Edit()), driver.findElement(ObjectLibrary.Editstyle()));
        Thread.sleep(2000);
        driver.findElement(ObjectLibrary.colourtab()).click();
        driver.findElement(ObjectLibrary.greencolor()).clear();
        driver.findElement(ObjectLibrary.greencolor()).sendKeys("#3DFFA5");
        driver.findElement(By.xpath("//*[@id=\"tabs-styleColor\"]/table/tbody/tr[6]/td[1]/label")).click();
        driver.findElement(ObjectLibrary.savechanges()).click();

        //adding an action tracking
        Thread.sleep(2000);
        driver.switchTo().frame(0);
        driver.findElement(ObjectLibrary.Elementtobetracked()).click();
        driver.switchTo().defaultContent();
        Thread.sleep(1000);
        Actions acti = new Actions(driver);
        acti.moveToElement(driver.findElement(ObjectLibrary.actiontracking())).click().perform();
        driver.findElement(ObjectLibrary.actionname()).click();
        driver.findElement(ObjectLibrary.actionname()).sendKeys(CommonLibrary.input_data_excel(testrowno, "Actiontrack"));
        Thread.sleep(1000);
        driver.findElement(ObjectLibrary.saveaction()).click();

        //create variation for multipage 2
        driver.findElement(By.xpath("//*[@id=\"ab-embed-editor__header\"]/div[3]/div/span")).click();
        driver.findElement(By.xpath("//*[@id=\"ab-embed-editor__header\"]/div[3]/ul/li[2]/a/span")).click();

        Thread.sleep(12000);
        //driver.get('https://www.customs.go.jp/toukei/srch/indexe.htm?M=09&P=1,2,,,,,,,,1,0,2020,0,10,0,2,440131,,,,,,,,,,1,,,,,,,,,,,,,,,,,,,,,,200')
//		ChromeDriverWait(driver, 20).until(EC.frame_to_be_available_and_switch_to_it((By.NAME,"TopPage")))
//		print(WebDriverWait(driver, 20).until(EC.visibility_of_element_located((By.XPATH, "//table[@class='value']"))).text)

        //Changing background color of a literal
        driver.switchTo().frame(0);
        driver.findElement(ObjectLibrary.Elementtobechanged()).click();
        driver.switchTo().defaultContent();
        Thread.sleep(1000);
        menu = driver.findElement(ObjectLibrary.Edit());
        act = new Actions(driver);
        act.moveToElement(menu).build().perform();
        Thread.sleep(1000);
       // CommonLibrary.hoverAndClick(driver, driver.findElement(ObjectLibrary.Edit()), driver.findElement(ObjectLibrary.Editstyle()));
        Thread.sleep(2000);
        driver.findElement(ObjectLibrary.colourtab()).click();
        driver.findElement(ObjectLibrary.greencolor()).clear();
        driver.findElement(ObjectLibrary.greencolor()).sendKeys("#FF3B96");
        driver.findElement(By.xpath("//*[@id=\"tabs-styleColor\"]/table/tbody/tr[6]/td[1]/label")).click();
        driver.findElement(ObjectLibrary.savechanges()).click();

        //adding an action tracking
        Thread.sleep(2000);
        driver.switchTo().frame(0);
        driver.findElement(By.xpath("//*[@id=\"submitButton\"]/h3")).click();
        driver.switchTo().defaultContent();
        Thread.sleep(1000);
        acti = new Actions(driver);
        acti.moveToElement(driver.findElement(ObjectLibrary.actiontracking())).click().perform();
        driver.findElement(ObjectLibrary.actionname()).click();
        driver.findElement(ObjectLibrary.actionname()).sendKeys("Click");
        Thread.sleep(1000);
        driver.findElement(ObjectLibrary.saveaction()).click();

        //reading the variation id
        Thread.sleep(2000);

        driver.findElement(By.xpath("//*[@id=\"ab-embed-editor__header\"]/ul/li[2]/a/span")).click();
        variationId = driver.findElement(By.xpath("//*[@id=\"ab-embed-editor__header\"]/ul/li[2]/div/ul/li[5]/span[2]")).getText();
        System.out.println(variationId);
    }

//*************************************************************************************************************
//*	                                Editing an existing test                                                  *
//*************************************************************************************************************	

    @Test(priority = 21)
    public void editABTest3rd() throws InterruptedException, IOException {

        //Searching for the particular test id
        Thread.sleep(1000);
        driver.findElement(ObjectLibrary.searchTestField()).click();
        driver.findElement(ObjectLibrary.searchTestField()).sendKeys(CommonLibrary.input_data_excel(testrowno, "testid"));
        //If the test id is not present, create a new test
        if (driver.getPageSource().contains("No personalisation found") || driver.getPageSource().contains("No campaign found")) {
            //createABTest();
        } else {
            //clicking on edit option
            Thread.sleep(2000);
            //CommonLibrary.hoverAndClick(driver, driver.findElement(ObjectLibrary.editCampaignPencilIcon()), driver.findElement(ObjectLibrary.editCampaignPencilIcon()));
            Thread.sleep(1000);
            driver.findElement(By.xpath("//*[contains(text(),'Visual editor')]")).click();
        }
    }

//*************************************************************************************************************
//*	                                Create Test Goal                                                          *
//*************************************************************************************************************	

    @Test(priority = 22)
    public void createGoal3rd() throws IOException, InterruptedException {

        System.out.println("MP Create goals started");

        if (CommonLibrary.input_data_excel(testrowno, "Goal").equals("Y")) {
            if (driver.getWindowHandles().size() > 1) {
                //Switch to new window opened
                for (String winHandle : driver.getWindowHandles()) {
                    driver.switchTo().window(winHandle);
                }
            }
            //for setting the primary goal
            Thread.sleep(7000);
            driver.findElement(ObjectLibrary.goalsTab()).click();
            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
            wait.until(ExpectedConditions.visibilityOfElementLocated(ObjectLibrary.actionTrackingsMousedown()));
            File src = new File(excelpath);
            // Load the file
            FileInputStream fis = new FileInputStream(src);
            // load the workbook
            XSSFWorkbook wb = new XSSFWorkbook(fis);
            // get the sheet which you want to modify or create
            for (int i = 0; i < wb.getNumberOfSheets(); i++) {
                if (wb.getSheetName(i).equals("Goals")) {
                    XSSFSheet sh2 = wb.getSheetAt(i);
                    //r is the row no of the ith sheet
                    String primgoal = "";
                    String secgoal = "";
                    for (int r = 1; r <= sh2.getLastRowNum(); r++) {
                        String rowvalue = sh2.getRow(r).getCell(0).getStringCellValue();
                        if (rowvalue.equals("Y")) {
                            primgoal = sh2.getRow(r).getCell(2).getStringCellValue();

                            if (primgoal.equalsIgnoreCase("Action Tracking")) {
                                driver.findElement(ObjectLibrary.actionTrackingsMousedown()).click();
                            }

                            if (primgoal.equalsIgnoreCase("Page Tracking")) {
                                driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]")).click();
                                driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[2]/div/div/p")).click();
                                driver.findElement(By.xpath("/html/body/div[11]/div/div/div/div[2]/div[1]/div/label/input")).click();
                                driver.findElement(By.xpath("/html/body/div[11]/div/div/div/div[2]/div[1]/div/label/input")).sendKeys("Page Track");
                                driver.findElement(By.xpath("/html/body/div[11]/div/div/div/div[2]/div[2]/div[1]/div/div[1]/div/div/div[1]/div[1]")).click();
                                Actions action = new Actions(driver);
                                WebElement optionsList = driver.findElement(By.xpath("//div[contains(@class, 'css-11unzgr')]"));
                                action.moveToElement(optionsList);

                                List<WebElement> options = driver.findElements(By.xpath("//div[contains(@class, 'Select__optionValue___2a670')]"));
                                for (WebElement option : options) {
                                    if (option.getText().equals("contains")) {
                                        option.click();
                                        break;
                                    }
                                }
                                driver.findElement(By.xpath("/html/body/div[11]/div/div/div/div[2]/div[2]/div[1]/div/div[2]/div/label/input")).click();
                                driver.findElement(By.xpath("/html/body/div[11]/div/div/div/div[2]/div[2]/div[1]/div/div[2]/div/label/input")).sendKeys(CommonLibrary.input_data_excel(testrowno, "URL"));
                                driver.findElement(By.xpath("/html/body/div[11]/div/div/div/div[2]/div[4]/button[2]")).click();
                                Thread.sleep(1000);
                                driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[2]/div[1]/div/div/span[2]")).click();
                            }

                            if (primgoal.equalsIgnoreCase("Custom Tracking")) {
                                driver.findElement(ObjectLibrary.customTracking()).click();
                            }
                            if (primgoal.equalsIgnoreCase("Browsing metrics Bounce Rate")) {
                                driver.findElement(ObjectLibrary.browsingMetrics()).click();
                                driver.findElement(ObjectLibrary.bounceRate()).click();
                            }
                            if (primgoal.equalsIgnoreCase("Browsing metrics Pages per session")) {
                                driver.findElement(ObjectLibrary.browsingMetrics()).click();
                                driver.findElement(ObjectLibrary.pagesPerSession()).click();
                            }
                            if (primgoal.equalsIgnoreCase("Browsing metrics Revisit")) {
                                driver.findElement(ObjectLibrary.browsingMetrics()).click();
                                driver.findElement(ObjectLibrary.revisitRate()).click();
                            }
                            if (primgoal.equalsIgnoreCase("Transaction")) {
                                driver.findElement(ObjectLibrary.transactionGoal()).click();
                                driver.findElement(By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/span[2]")).click();
                            }
                            break;
                        }
                    }

                    //for setting the secondary goal
                    for (int r = 1; r <= sh2.getLastRowNum(); r++) {
                        String rowvalue = sh2.getRow(r).getCell(1).getStringCellValue();
                        if (rowvalue.equals("Y")) {
                            secgoal = sh2.getRow(r).getCell(2).getStringCellValue();
                            if (secgoal.equalsIgnoreCase(primgoal)) {
                                continue;
                            } else {
                                if (secgoal.equalsIgnoreCase("Action Tracking")) {
                                    driver.findElement(ObjectLibrary.actionTrackingsMousedown()).click();
                                }
                                if (secgoal.equalsIgnoreCase("Page Tracking")) {
                                    driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[1]/div[2]")).click();
                                    driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[2]/div/div/p")).click();
                                    driver.findElement(By.xpath("/html/body/div[11]/div/div/div/div[2]/div[1]/div/label/input")).click();
                                    driver.findElement(By.xpath("/html/body/div[11]/div/div/div/div[2]/div[1]/div/label/input")).sendKeys("Page Track");
                                    driver.findElement(By.xpath("/html/body/div[11]/div/div/div/div[2]/div[2]/div[1]/div/div[1]/div/div/div[1]/div[1]")).click();
                                    Actions action = new Actions(driver);
                                    WebElement optionsList = driver.findElement(By.xpath("//div[contains(@class, 'css-11unzgr')]"));
                                    action.moveToElement(optionsList);

                                    List<WebElement> options = driver.findElements(By.xpath("//div[contains(@class, 'Select__optionValue___2a670')]"));
                                    for (WebElement option : options) {
                                        if (option.getText().equals("contains")) {
                                            option.click();
                                            break;
                                        }
                                    }
                                    driver.findElement(By.xpath("/html/body/div[11]/div/div/div/div[2]/div[2]/div[1]/div/div[2]/div/label/input")).click();
                                    driver.findElement(By.xpath("/html/body/div[11]/div/div/div/div[2]/div[2]/div[1]/div/div[2]/div/label/input")).sendKeys(CommonLibrary.input_data_excel(testrowno, "URL"));
                                    driver.findElement(By.xpath("/html/body/div[11]/div/div/div/div[2]/div[4]/button[2]")).click();
                                    Thread.sleep(1000);
                                    driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[1]/div[2]/div[2]/div[1]/div/div/span[2]")).click();
                                }

                                if (secgoal.equalsIgnoreCase("Custom Tracking")) {
                                    driver.findElement(ObjectLibrary.customTracking()).click();
                                }
                                if (secgoal.equalsIgnoreCase("Browsing metrics Bounce Rate")) {
                                    driver.findElement(ObjectLibrary.browsingMetrics()).click();
                                    driver.findElement(ObjectLibrary.bounceRate()).click();
                                }
                                if (secgoal.equalsIgnoreCase("Browsing metrics Pages per session")) {
                                    driver.findElement(ObjectLibrary.browsingMetrics()).click();
                                    driver.findElement(ObjectLibrary.pagesPerSession()).click();
                                }
                                if (secgoal.equalsIgnoreCase("Browsing metrics Revisit")) {
                                    driver.findElement(ObjectLibrary.browsingMetrics()).click();
                                    driver.findElement(ObjectLibrary.revisitRate()).click();
                                }
                                if (secgoal.equalsIgnoreCase("Transaction")) {
                                    driver.findElement(ObjectLibrary.transactionGoal()).click();
                                    driver.findElement(By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/span[2]")).click();
                                }
                            }
                        }
                    }
                }
            }
            wait = new WebDriverWait(driver, Duration.ofSeconds(20));
            //  wait.until(ExpectedConditions.visibilityOfElementLocated(ObjectLibrary.savegoal()));

            //driver.findElement(objectlibrary.savegoal()).click();
            //    WebElement ele = driver.findElement(ObjectLibrary.savegoal());
            //  driver.executeScript("arguments[0].click();", ele);
            Thread.sleep(2000);
        }
        System.out.println("MP Create goals successfully ended");
    }

    @Test(priority = 23)
    public void createTarget3rd() throws InterruptedException, IOException {

        System.out.println("MP Create target started");
        Thread.sleep(1000);
        //driver.findElement(objectlibrary.targettab()).click();
        target.setTarget(testrowno, driver);
        System.out.println("MP Create target successfully ended");
    }


//*************************************************************************************************************
//*	                                Traffic allocation                                                        *
//*************************************************************************************************************	

    @Test(priority = 24)
    public void trafficAllocate3rd() throws IOException, InterruptedException {
        System.out.println("MP traffic allocation started");
        Thread.sleep(2000);
//		driver.findElement(By.xpath("//*[contains(text(),\"Traffic allocation\")]")).click();
        if (driver.getWindowHandles().size() > 1) {
            //Switch to new window opened
            for (String winHandle : driver.getWindowHandles()) {
                driver.switchTo().window(winHandle);
            }
        }
        //Allocating the traffic
        Thread.sleep(1000);
        driver.findElement(ObjectLibrary.varpercent()).clear();
        //driver.findElement(objectlibrary.varpercent()).sendKeys(CommonLibrary.input_data_excel(testrowno, "trafic"));
        Thread.sleep(1000);
        WebElement element = driver.findElement(ObjectLibrary.varpercent());
        new Actions(driver).sendKeys(element, "100").perform();
        Thread.sleep(1000);
        WebElement ele = driver.findElement(ObjectLibrary.savetraffic());
        driver.executeScript("arguments[0].click();", ele);

        //closing the popin
        if (driver.getPageSource().contains("You’re about to change Traffic Allocation")) {
            driver.findElement(By.xpath("//*[text()=\"Set variation to 100% anyway\"]")).click();
            driver.findElement(By.xpath("//*[@data-testid=\"closeIcon\"]")).click();
        }

        //closing the popin
        Thread.sleep(1000);
        if (driver.getPageSource().contains("You're changing Traffic")) {
            driver.findElement(By.xpath("/html/body/div[10]/div/div/div/div[2]/div[5]/button[2]")).click();
        }
        System.out.println("MP traffic allocation successfully ended");
    }

//*************************************************************************************************************
//*	                                Play/Pause the test                                                       *
//*************************************************************************************************************	

    @Test(priority = 25)
    public void playTest3rd() throws InterruptedException {

        System.out.println("MP play test started");
        if (driver.getWindowHandles().size() > 1) {
            //Switch to new window opened
            for (String winHandle : driver.getWindowHandles()) {
                driver.switchTo().window(winHandle);
            }
        }
        Thread.sleep(2000);
        driver.findElement(ObjectLibrary.playtest()).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//*[text()= \"Play\"]")).click();
        parentWindow = driver.getWindowHandle();
        System.out.println("MP play test successfully ended");
    }

//*************************************************************************************************************
//*	                                Run API Hits                                                              *
//*************************************************************************************************************	

    @Test(priority = 26)
    public void runAPIHits3rd() throws HeadlessException, UnsupportedFlavorException, IOException, InterruptedException {

        System.out.println("MP run api hits started");
        Thread.sleep(1000);
        //WebElement element = driver.findElement(By.xpath("//*[@class=\"CampaignInformation__testId___3p_1s\"]"));
        driver.findElement(By.className("CampaignInformation__testIdContainer___3S9Ha")).click();

        // ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);

        String campaignid = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor); // extracting the text that was copied to the clipboard
        System.out.println(campaignid);
        Test_api_Post.APIHitsCollection1(campaignid, variationId, customerId);
        System.out.println("MP run api hits successfully ended");
    }

//*************************************************************************************************************
//*	                                Verify the test                                                           *
//*************************************************************************************************************	

    @Test(priority = 27)
    public void verifyTest3rd() throws Exception {

        System.out.println("MP verify test started");
        Thread.sleep(50000);

        String expectedvalue = "";

        //String URL = CommonLibrary.input_data_excel(testrowno, "URL");
        String URL = "http://abtastylab.com/5f02dd192aceb559426b2173cdd69517/";
        ((JavascriptExecutor) driver).executeScript("window.open(arguments[0])", URL);
        //driver.get(URL);
        if (testenvironment.equalsIgnoreCase("staging")) {
            //intercepting network calls
            networkmocking.networkmock(driver);
        }

        int tabno = driver.getWindowHandles().size();
        ArrayList<String> windowHandles = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(windowHandles.get(tabno - 1));

        //refreshing the page
        Thread.sleep(1000);
        driver.navigate().refresh();
        Thread.sleep(1000);

        //calls the screenshot function
        Thread.sleep(1000);
        String date = CommonLibrary.getDateTime();
        CommonLibrary.takeSnapShot(driver, "Screenshots" + "/" + "Autotest" + date + ".jpg");
        String colourvalue1 = null;
        if (driver.getPageSource().contains("#3DFFA5!important")) {
            colourvalue1 = "true";
        }

        //going to the next page
//	WebElement element = driver.findElement(By.xpath("//*[@id=\"main\"]/div[3]/a"));
//    ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
//    Thread.sleep(1000);
//    element.click();
        driver.get("http://abtastylab.com/5f02dd192aceb559426b2173cdd69517/checkout.php");
        //intercepting network calls
        //networkmocking.networkmock(driver);
        //refreshing the page
        Thread.sleep(1000);
        driver.navigate().refresh();
        Thread.sleep(1000);
        String colourvalue2 = null;
        Thread.sleep(1000);
        if (driver.getPageSource().contains("#FF3B96!important")) {
            colourvalue2 = "true";
        }
        System.out.println(colourvalue1 + colourvalue2);
        if (colourvalue1.equals("true") && colourvalue2.equals("true")) {
            expectedvalue = "true";
        }
        Assert.assertEquals(expectedvalue, "true", "Test is failed");
        System.out.println("MP verify test successfully ended");
    }

//*************************************************************************************************************
//*	                                Checking the report                                                       *
//*************************************************************************************************************

    @Test(priority = 28)
    public void checkReport3rd() throws Exception {

        System.out.println("MP check report started");
        Thread.sleep(1000);
        driver.switchTo().window(parentWindow);
        //driver.navigate().back();
        //click the report button
        driver.findElement(ObjectLibrary.report()).click();
        if (driver.getWindowHandles().size() > 1) {
            //Switch to new window opened
            for (String winHandle : driver.getWindowHandles()) {
                driver.switchTo().window(winHandle);
            }
        }

        ReportURL = driver.getCurrentUrl();
        Thread.sleep(6000);
        //to view the realtime report
        System.out.println(ReportURL);
        if (driver.findElement(ObjectLibrary.realtimereport()).isEnabled()) {
            System.out.println("enabled");
        } else {
            System.out.println("disabled");
        }
        driver.findElement(ObjectLibrary.realtimereport()).click();

//    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(60));
//    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[text()='See current activity']")));
//   	  
//   	driver.findElement(By.xpath("//*[text()='See current activity']")).click();
//    if(driver.getPageSource().contains("PageView")) {
//		//calls the screenshot function
//        Thread.sleep(1000);
//    	String date = CommonLibrary.getDateTime();
//    	CommonLibrary.takeSnapShot(driver,"Screenshots" + "/" + "report"+date +".jpg");
//	}
        //to pause the test
        Thread.sleep(2000);
        driver.switchTo().window(parentWindow);
        Thread.sleep(2000);
        driver.findElement(ObjectLibrary.pausetest()).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//*[text()= \"Pause\"]")).click();
        System.out.println("MP check report successfully ended");
    }

//*************************************************************************************************************
//*	                                          Delete Created Campaign                                         *
//*************************************************************************************************************	

    @Test(priority = 29)
    public void deleteCreatedCampaign3rd() throws Exception {
        System.out.println("MP delete created campaign started");

        // Go to the Dashboard
        driver.findElement(ObjectLibrary.arrowBackToDashbaord()).click();
        Thread.sleep(3000);

        // Test if delete the right campaign
        WebElement createdCampaignName = driver.findElements(ObjectLibrary.allCampaignsNameOnTheDashboard()).get(0);

        Assert.assertTrue(
                createdCampaignName.getText().equals(CommonLibrary.input_data_excel(testrowno, "testname"))
                        ||
                        createdCampaignName.getText().equals("Auto MP Test"));

        // Delete the campaign
        WebElement openActionDropodwn = driver.findElements(ObjectLibrary.moreActions()).get(0);
        openActionDropodwn.click();
        Thread.sleep(500);
        driver.findElement(ObjectLibrary.deleteCampaignFromDashboard()).click();
        Thread.sleep(500);
        driver.findElement(ObjectLibrary.confirmDeleteCampaignOnModal()).click();
        Thread.sleep(500);

        System.out.println("MP delete created campaig successfully ended");
    }


//*************************************************************************************************************
//*	                                Closing the driver                                                        *
//*************************************************************************************************************
*/

}