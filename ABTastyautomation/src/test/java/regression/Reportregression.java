package regression;

import java.io.IOException;
import java.util.Collections;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import pageObject.ObjectLibrary;


public class Reportregression {

	static WebDriver driver;
	static String testrowno;

	static String chromedriverpath = "C:\\abTastySelenium\\drivers\\chromedriver.exe";
	
public static void main(String[] args) throws InterruptedException, IOException {
		
		Login();
		selectAccount();
		reportABTest();
		filtercondition();
	}
	
	public static void Login() throws InterruptedException, IOException {
		
		System.setProperty("webdriver.chrome.driver", chromedriverpath);

		//To check whether to test in staging or production

		testrowno = "1st";
			
		String testtype = CommonLibrary.input_data_excel(testrowno, "typeofenv");
		if (testtype.equalsIgnoreCase("staging"))
		{
		//Initialize browser
			ChromeOptions options = new ChromeOptions();
			options.addArguments("-incognito");
			options.addArguments("--user-data-dir=/Users/monikapanda/Documents/Selenium/BrowserProfile/Profilenew");
			
		//Open the editor
			driver = new ChromeDriver(options);
		    driver.get("https://staging-app2.abtasty.com/experiments");
		}
		else
		{
			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("useAutomationExtension", false);
			options.setExperimentalOption("excludeSwitches",Collections.singletonList("enable-automation"));    
			driver = new ChromeDriver(options);
			driver.get("https://app2.abtasty.com/experiments");
		}

		//login to the editor
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.id()).click();

		driver.findElement(ObjectLibrary.id()).sendKeys(CommonLibrary.main_data_excel("tester", "userid"));

		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.password()).click();
		driver.findElement(ObjectLibrary.password()).sendKeys(CommonLibrary.main_data_excel("tester", "password"));
		driver.findElement(ObjectLibrary.signIn()).click();
		driver.manage().window().maximize();
		}
	
	public static void selectAccount() throws InterruptedException, IOException {

		//Locate 'Account Names' drop down
		Thread.sleep(4000);
		WebElement accountnames = driver.findElement(ObjectLibrary.accountNames());
		accountnames.click();
		//Thread.sleep(1000);
		WebElement name = driver.findElement(ObjectLibrary.searchAccName());
		name.sendKeys("Gil Da Silva");
		Thread.sleep(1000);
		Actions actions = new Actions(driver); 
		actions.keyDown(Keys.COMMAND);
		driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/nav/div[2]/div[2]/div/div/div/div[2]/div")).click();
		Thread.sleep(5000);
	    if(CommonLibrary.input_data_excel(testrowno, "typeoftest").equalsIgnoreCase("Personalisation")) {
		//CommonLibrary.HoverAndClick(driver,driver.findElement(objectlibrary.campaign()),driver.findElement(objectlibrary.personalisation()));
		driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/nav/div[2]/div[1]/div[2]/a")).click();
	}
	
	Thread.sleep(2000);
	//closing the pop in
	if (driver.getPageSource().contains("stroke: rgb(92, 92, 92);")) {
	       driver.findElement(By.xpath("/html/body/abtasty-modal/div/div")).click();
	  }
	}

	public static void reportABTest() throws InterruptedException, IOException {
		//Searching for the particular test id
			Thread.sleep(500);
			driver.findElement(ObjectLibrary.searchTestField()).click();
			driver.findElement(ObjectLibrary.searchTestField()).sendKeys("499589");

			//clicking on report option
			Thread.sleep(2000);
			CommonLibrary.hoverAndClick(driver,driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[2]/div/div[1]/table/tbody/tr/td[7]/div/div/div[1]/a/div/div/span")),driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[2]/div/div[1]/table/tbody/tr/td[7]/div/div/div[1]/a/div/div/span")));
			//driver.findElement(By.xpath("/html/body/ui-view[1]/div[1]/div/div/div/div/div/div/section/div/test-list/div/div/div/div/div[5]/div/div/i")).click();
			//driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[2]/div/div[1]/table/tbody/tr/td[7]/div/div/div[1]/div[2]/div/a/span")).click();
		 }
	
	public static void filtercondition() throws InterruptedException {
		//clckimg on filter
		Thread.sleep(3000);
		if (driver.getWindowHandles().size() > 1) {
			//Switch to new window opened
			for(String winHandle : driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
		}
	   	driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div/div[3]/div/div[1]/div[2]/button")).click();
	   	Thread.sleep(1000);
		driver.findElement(By.xpath("/html/body/div[14]/div[2]/div/div/ul/li[1]/div/div[1]/span")).click();
		driver.findElement(By.xpath("/html/body/div[14]/div[2]/div/div/ul/li[1]/div/div[2]/div/div/ul/li[1]/div/span")).click();
		driver.findElement(By.xpath("/html/body/div[14]/div[2]/div/div/div[2]/div/div[2]/div[2]/button")).click();
		Thread.sleep(3000);
		String expectedvalue = null;
		if (driver.getPageSource().contains("filtered view")) {
			expectedvalue = "true";	
		}
		Assert.assertEquals(expectedvalue,"true", "Test is failed");
		}
	}
