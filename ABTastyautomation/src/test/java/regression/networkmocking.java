package regression;

import java.util.Optional;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.devtools.DevTools;
import org.openqa.selenium.devtools.v100.fetch.Fetch;
import org.openqa.selenium.devtools.v100.network.Network;

public class networkmocking {
	static int consent = 0;
	static DevTools devtools;

	public static void networkmock(ChromeDriver driver) throws InterruptedException {
		
//		System.setProperty("webdriver.chrome.driver", "/Users/monikapanda/Downloads/chromedriver 6");
//		ChromeDriver driver = new ChromeDriver();

		devtools = driver.getDevTools();
		devtools.createSession();
		devtools.send(Fetch.enable(Optional.empty(), Optional.empty()));
		
		devtools.addListener(Fetch.requestPaused(), request->
		{
			if((request.getRequest().getUrl().contains("//try.abtasty.com/")) || (request.getRequest().getUrl().contains("//staging-try.abtasty.com/")))
			{
				String newUrl = request.getRequest().getUrl().replace("//try.abtasty.com/","//staging-try.abtasty.com/");
				System.out.println(request.getRequest().getUrl());
				System.out.println(newUrl);
				
				devtools.send(Fetch.continueRequest(request.getRequestId(), Optional.of(newUrl), Optional.of(request.getRequest().getMethod()), 
						Optional.empty(), Optional.empty(), Optional.empty()));
			}
			else
				
			{
				devtools.send(Fetch.continueRequest(request.getRequestId(), Optional.of(request.getRequest().getUrl()), Optional.of(request.getRequest().getMethod()), 
						Optional.empty(), Optional.empty(), Optional.empty()));
				System.out.println("no update" + request.getRequest().getUrl());
				System.out.println("no mocked url");
			}
		});
		
	}
		
		public static void networkread (ChromeDriver driver, String testenv) throws InterruptedException {

			if (testenv.equalsIgnoreCase("prod")) {
			   devtools = driver.getDevTools();
			   devtools.createSession();
			}

	        devtools.send(Network.enable(Optional.empty(), Optional.empty(), Optional.empty()));

	        System.out.println("method calling");
	        devtools.addListener(Network.requestWillBeSent(), requestSent -> {
	        	
	        	if(requestSent.getRequest().getUrl().contains("ariane")) {

	              System.out.println("Request URL => " + requestSent.getRequest().getUrl());

	              System.out.println("Request Method => " + requestSent.getRequest().getMethod());

	              System.out.println("Request Headers => " + requestSent.getRequest().getHeaders().toString());
	              
	              System.out.println("ariane found");

	              System.out.println("------------------------------------------------------");
	              
	              consent++;
	        	}
	        	else {
	        		System.out.println("no ariane");
	        		System.out.println("Request URL => " + requestSent.getRequest().getUrl());
	        	}
	        	System.out.println(consent);
	        });
	  }

}
