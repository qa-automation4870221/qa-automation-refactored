package regression;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


public class test {

    // one instance, reuse
    private final CloseableHttpClient httpClient = HttpClients.createDefault(); 
    String access_token = "";
    

    public static void main(String args[]) throws Exception {

        test obj = new test();

        try {
//             System.out.println("Testing 1 - Send Http GET request");
//             obj.sendGet();
              System.out.println("Testing 2 - Send Http POST request");
              obj.sendPost();
        	
        	for(int i = 0; i<1; i++) {
        		Timestamp timestamp = new Timestamp(System.currentTimeMillis());  
        		String time = Long.toString(timestamp.getTime());
            	System.out.println(time);
        		int random = (int )(Math.random() * 10 + 1);
        		String visitorid = random + "AM" + time;
        		System.out.println(visitorid);
        		
//        		sendPostcampaign(time,visitorid);
//        		sendPostPageView1(time,visitorid);
//        		sendPostEvent1(time,visitorid);
//        		sendPostNPS(time,visitorid);
//        		sendPostPageView2(time,visitorid);
//        		sendPostEvent2(time,visitorid);
//        		sendPostEventEco(time,visitorid);
        		
        	}
        } finally {
            obj.close();
        }
    }

  

    private void sendGet() throws Exception {

        HttpGet request = new HttpGet("https://staging-api.abtasty.com/api/audience/accounts/47713/audiences");
        String accesstoken = "Bearer" + access_token;

        // add request headers
        request.addHeader("Authorization", accesstoken);
        request.addHeader("Content-Type", "application/json");

        try (CloseableHttpResponse response = httpClient.execute(request)) {

            // Get HttpResponse Status
            System.out.println(response.getStatusLine().toString());

            HttpEntity entity = response.getEntity();
            Header headers = entity.getContentType();
            System.out.println(headers);

            if (entity != null) {
                // return it as a String
                String result = EntityUtils.toString(entity);
                System.out.println(result);
            }
        }
    }
    
    private void sendPost() throws Exception {

        HttpPost post = new HttpPost("https://staging-api.abtasty.com/oauth/v2/token");
        
        //String accesstoken1 = "Bearer" + access_token;
        // add request headers
        //post.addHeader("Authorization", accesstoken1);
        post.addHeader("Content-Type", "application/json");

        // add request parameter, form parameters
        List<NameValuePair> urlParameters = new ArrayList<>();
        urlParameters.add(new BasicNameValuePair("client_id", "1_5550xg1fpu8ss4cc0c4sscw4sswswoc008sckgw8kocg4w0ogo"));
        urlParameters.add(new BasicNameValuePair("client_secret", "370j0g2u1zacowg88oc080goso8kwcgc8kkg8004ckgs8owgko"));
        urlParameters.add(new BasicNameValuePair("grant_type", "password"));
        urlParameters.add(new BasicNameValuePair("username", "monika.panda@abtasty.com"));
        urlParameters.add(new BasicNameValuePair("password", "Moni@001"));

        post.setEntity(new UrlEncodedFormEntity(urlParameters));
//        post.addHeader("Content-Type", "application/json");

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(post)) {
        	
        	// Get HttpResponse Status
            System.out.println(response.getStatusLine().toString());
            
            String result = EntityUtils.toString(response.getEntity());
            System.out.println(result);
            String finalResult = "[" + result + "]";
            System.out.println(finalResult);

             JSONArray ja = new JSONArray(finalResult);
//            // ITERATE THROUGH AND RETRIEVE CLUB FIELDS
              int n = ja.length();
              System.out.println(n);
            for (int i = 0; i < n; i++) {
                // GET INDIVIDUAL JSON OBJECT FROM JSON ARRAY
                JSONObject jo = ja.getJSONObject(i);

                // RETRIEVE EACH JSON OBJECT'S FIELDS
                
                access_token = jo.getString("access_token");
                System.out.println(access_token);
                System.out.println("Post parameters : " + urlParameters);
        }
        }
    }
    
    private static void sendPostcampaign(String time,String visitorid) throws Exception {

        HttpPost post = new HttpPost("https://ariane.abtasty.com");
       
        // add request parameter, form parameters
        List<NameValuePair> urlParameters = new ArrayList<>();
        urlParameters.add(new BasicNameValuePair("caid", "531355"));
        urlParameters.add(new BasicNameValuePair("cid", "023c2cee0f06c645d5c3d28e61b2e773"));
        urlParameters.add(new BasicNameValuePair("cst", time));
        urlParameters.add(new BasicNameValuePair("de", "UTF-8"));
        urlParameters.add(new BasicNameValuePair("dl", "http%3A%2F%2Fabtastylab.com%2F023c2cee0f06c645d5c3d28e61b2e773%2F"));
        urlParameters.add(new BasicNameValuePair("dr", ""));
        urlParameters.add(new BasicNameValuePair("je", "false"));
        urlParameters.add(new BasicNameValuePair("pt", "Destinations%20%7C%20Luxury%20Travel%20Site"));
        urlParameters.add(new BasicNameValuePair("sd", "24-bits"));
        urlParameters.add(new BasicNameValuePair("sen", "0"));
        urlParameters.add(new BasicNameValuePair("sn", "1"));
        urlParameters.add(new BasicNameValuePair("sr", "1920x1080"));
        urlParameters.add(new BasicNameValuePair("t", "CAMPAIGN"));
        urlParameters.add(new BasicNameValuePair("ul", "fr"));
        urlParameters.add(new BasicNameValuePair("vaid", "662810"));
        urlParameters.add(new BasicNameValuePair("vid", visitorid));
        urlParameters.add(new BasicNameValuePair("vp", "2400x451"));
        

        post.setEntity(new UrlEncodedFormEntity(urlParameters));
        post.addHeader("Content-Type", "application/json");
        post.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36");
        post.addHeader("Origin", "http://abtastylab.com");
        post.addHeader("Referer", "http://abtastylab.com/023c2cee0f06c645d5c3d28e61b2e773/");

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(post)) {

        	// Get HttpResponse Status
            System.out.println(response.getStatusLine().toString());
            
            System.out.println(EntityUtils.toString(response.getEntity()));
            System.out.println("Post parameters : " + urlParameters);
        }

    }
    private static void sendPostPageView1(String time,String visitorid) throws Exception {

        HttpPost post = new HttpPost("https://ariane.abtasty.com");
        String cid = "531355";
        String vaid = "662810";
      
        // add request parameter, form parameters
        List<NameValuePair> urlParameters = new ArrayList<>();
        urlParameters.add(new BasicNameValuePair("c", cid + ": " + vaid));
        urlParameters.add(new BasicNameValuePair("cid", "023c2cee0f06c645d5c3d28e61b2e773"));
        urlParameters.add(new BasicNameValuePair("cst", time));
        urlParameters.add(new BasicNameValuePair("de", "UTF-8"));
        urlParameters.add(new BasicNameValuePair("dl", "http%3A%2F%2Fabtastylab.com%2F023c2cee0f06c645d5c3d28e61b2e773%2F"));
        urlParameters.add(new BasicNameValuePair("dr", ""));
        urlParameters.add(new BasicNameValuePair("je", "false"));
        urlParameters.add(new BasicNameValuePair("pt", ""));
        urlParameters.add(new BasicNameValuePair("sd", "24-bits"));
        urlParameters.add(new BasicNameValuePair("sen", "0"));
        urlParameters.add(new BasicNameValuePair("sn", "1"));
        urlParameters.add(new BasicNameValuePair("sr", "1920x1080"));
        urlParameters.add(new BasicNameValuePair("t", "PAGEVIEW"));
        urlParameters.add(new BasicNameValuePair("ul", "fr"));
        urlParameters.add(new BasicNameValuePair("vaid", vaid));
        urlParameters.add(new BasicNameValuePair("vid", visitorid));
        urlParameters.add(new BasicNameValuePair("vp", "2400x451"));
        

        post.setEntity(new UrlEncodedFormEntity(urlParameters));
        post.addHeader("Content-Type", "application/json");
        post.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36");
        post.addHeader("Origin", "http://abtastylab.com");
        post.addHeader("Referer", "http://abtastylab.com/023c2cee0f06c645d5c3d28e61b2e773/");

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(post)) {

        	// Get HttpResponse Status
            System.out.println(response.getStatusLine().toString());
            
            System.out.println(EntityUtils.toString(response.getEntity()));
            System.out.println("Post parameters : " + urlParameters);
        }

    }
    
    private static void sendPostEvent1(String time,String visitorid) throws Exception {

        HttpPost post = new HttpPost("https://ariane.abtasty.com");

        // add request parameter, form parameters
        List<NameValuePair> urlParameters = new ArrayList<>();
        urlParameters.add(new BasicNameValuePair("c", "531355: 662810"));
        urlParameters.add(new BasicNameValuePair("caid", "531355"));
        urlParameters.add(new BasicNameValuePair("cid", "023c2cee0f06c645d5c3d28e61b2e773"));
        urlParameters.add(new BasicNameValuePair("cst", time));
        urlParameters.add(new BasicNameValuePair("de", "UTF-8"));
        urlParameters.add(new BasicNameValuePair("dl", "http%3A%2F%2Fabtastylab.com%2F023c2cee0f06c645d5c3d28e61b2e773%2F"));
        urlParameters.add(new BasicNameValuePair("dr", ""));
        urlParameters.add(new BasicNameValuePair("ea", "TOP"));
        urlParameters.add(new BasicNameValuePair("ec", "Action Tracking"));
        urlParameters.add(new BasicNameValuePair("je", "false"));
        urlParameters.add(new BasicNameValuePair("pt", "Home%20%7C%20Luxury%20Travel%20Site"));
        urlParameters.add(new BasicNameValuePair("sd", "24-bits"));
        urlParameters.add(new BasicNameValuePair("sen", "0"));
        urlParameters.add(new BasicNameValuePair("sn", "1"));
        urlParameters.add(new BasicNameValuePair("sr", "1920x1080"));
        urlParameters.add(new BasicNameValuePair("t", "EVENT"));
        urlParameters.add(new BasicNameValuePair("ul", "fr-FR"));
        urlParameters.add(new BasicNameValuePair("vaid", "662810"));
        urlParameters.add(new BasicNameValuePair("vid", visitorid));
        urlParameters.add(new BasicNameValuePair("vp", "2400x451"));
        

        post.setEntity(new UrlEncodedFormEntity(urlParameters));

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(post)) {

        	// Get HttpResponse Status
            System.out.println(response.getStatusLine().toString());
            
            System.out.println(EntityUtils.toString(response.getEntity()));
        }

    }
    
    private static void sendPostNPS(String time,String visitorid) throws Exception {

        HttpPost post = new HttpPost("https://ariane.abtasty.com");

        // add request parameter, form parameters
        List<NameValuePair> urlParameters = new ArrayList<>();
        urlParameters.add(new BasicNameValuePair("c", "531355: 662810"));
        urlParameters.add(new BasicNameValuePair("cid", "023c2cee0f06c645d5c3d28e61b2e773"));
        urlParameters.add(new BasicNameValuePair("cst", time));
        urlParameters.add(new BasicNameValuePair("de", "UTF-8"));
        urlParameters.add(new BasicNameValuePair("dl", "http%3A%2F%2Fabtastylab.com%2F023c2cee0f06c645d5c3d28e61b2e773%2F"));
        urlParameters.add(new BasicNameValuePair("dr", ""));
        urlParameters.add(new BasicNameValuePair("je", "false"));
        urlParameters.add(new BasicNameValuePair("nf", "My NPS opinion"));
        urlParameters.add(new BasicNameValuePair("ns", "8"));
        urlParameters.add(new BasicNameValuePair("pt", "Home%20%7C%20Luxury%20Travel%20Site"));
        urlParameters.add(new BasicNameValuePair("sd", "24-bits"));
        urlParameters.add(new BasicNameValuePair("sen", "0"));
        urlParameters.add(new BasicNameValuePair("sn", "1"));
        urlParameters.add(new BasicNameValuePair("sr", "1920x1080"));
        urlParameters.add(new BasicNameValuePair("t", "NPS"));
        urlParameters.add(new BasicNameValuePair("ul", "fr-FR"));
        urlParameters.add(new BasicNameValuePair("vid", visitorid));
        urlParameters.add(new BasicNameValuePair("vp", "2400x451"));
        

        post.setEntity(new UrlEncodedFormEntity(urlParameters));

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(post)) {

        	// Get HttpResponse Status
            System.out.println(response.getStatusLine().toString());
            
            System.out.println(EntityUtils.toString(response.getEntity()));
        }

    }
    
    private static void sendPostPageView2(String time,String visitorid) throws Exception {

        HttpPost post = new HttpPost("https://ariane.abtasty.com");

        // add request parameter, form parameters
        List<NameValuePair> urlParameters = new ArrayList<>();
        urlParameters.add(new BasicNameValuePair("c", "531355: 662810"));
        urlParameters.add(new BasicNameValuePair("cid", "023c2cee0f06c645d5c3d28e61b2e773"));
        urlParameters.add(new BasicNameValuePair("cst", time));
        urlParameters.add(new BasicNameValuePair("de", "UTF-8"));
        urlParameters.add(new BasicNameValuePair("dl", "http%3A%2F%2Fabtastylab.com%2F023c2cee0f06c645d5c3d28e61b2e773%2Fdestinations.php"));
        urlParameters.add(new BasicNameValuePair("dr", ""));
        urlParameters.add(new BasicNameValuePair("je", "false"));
        urlParameters.add(new BasicNameValuePair("pt", ""));
        urlParameters.add(new BasicNameValuePair("sd", "24-bits"));
        urlParameters.add(new BasicNameValuePair("sen", "0"));
        urlParameters.add(new BasicNameValuePair("sn", "1"));
        urlParameters.add(new BasicNameValuePair("sr", "1920x1080"));
        urlParameters.add(new BasicNameValuePair("t", "PAGEVIEW"));
        urlParameters.add(new BasicNameValuePair("ul", "fr"));
        urlParameters.add(new BasicNameValuePair("vid", visitorid));
        urlParameters.add(new BasicNameValuePair("vp", "2400x451"));
        

        post.setEntity(new UrlEncodedFormEntity(urlParameters));

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(post)) {

        	// Get HttpResponse Status
            System.out.println(response.getStatusLine().toString());
            
            System.out.println(EntityUtils.toString(response.getEntity()));
        }

    }
    
    private static void sendPostEvent2(String time,String visitorid) throws Exception {

        HttpPost post = new HttpPost("https://ariane.abtasty.com");

        // add request parameter, form parameters
        List<NameValuePair> urlParameters = new ArrayList<>();
        urlParameters.add(new BasicNameValuePair("c", "531355: 662810"));
        urlParameters.add(new BasicNameValuePair("caid", "531355"));
        urlParameters.add(new BasicNameValuePair("cid", "023c2cee0f06c645d5c3d28e61b2e773"));
        urlParameters.add(new BasicNameValuePair("cst", time));
        urlParameters.add(new BasicNameValuePair("de", "UTF-8"));
        urlParameters.add(new BasicNameValuePair("dl", "http%3A%2F%2Fabtastylab.com%2F023c2cee0f06c645d5c3d28e61b2e773%2F"));
        urlParameters.add(new BasicNameValuePair("dr", ""));
        urlParameters.add(new BasicNameValuePair("ea", "TOP"));
        urlParameters.add(new BasicNameValuePair("ec", "Action Tracking"));
        urlParameters.add(new BasicNameValuePair("je", "false"));
        urlParameters.add(new BasicNameValuePair("pt", "Home%20%7C%20Luxury%20Travel%20Site"));
        urlParameters.add(new BasicNameValuePair("sd", "24-bits"));
        urlParameters.add(new BasicNameValuePair("sen", "0"));
        urlParameters.add(new BasicNameValuePair("sn", "1"));
        urlParameters.add(new BasicNameValuePair("sr", "1920x1080"));
        urlParameters.add(new BasicNameValuePair("t", "EVENT"));
        urlParameters.add(new BasicNameValuePair("ul", "fr-FR"));
        urlParameters.add(new BasicNameValuePair("vaid", "662810"));
        urlParameters.add(new BasicNameValuePair("vid", visitorid));
        urlParameters.add(new BasicNameValuePair("vp", "2400x451"));
        

        post.setEntity(new UrlEncodedFormEntity(urlParameters));

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(post)) {

        	// Get HttpResponse Status
            System.out.println(response.getStatusLine().toString());
            
            System.out.println(EntityUtils.toString(response.getEntity()));
        }

    }
    
    private static void sendPostEventEco(String time,String visitorid) throws Exception {

        HttpPost post = new HttpPost("https://ariane.abtasty.com");

        // add request parameter, form parameters
        List<NameValuePair> urlParameters = new ArrayList<>();
        urlParameters.add(new BasicNameValuePair("c", "531355: 662810"));
        urlParameters.add(new BasicNameValuePair("cid", "023c2cee0f06c645d5c3d28e61b2e773"));
        urlParameters.add(new BasicNameValuePair("cst", time));
        urlParameters.add(new BasicNameValuePair("de", "UTF-8"));
        urlParameters.add(new BasicNameValuePair("dl", "http%3A%2F%2Fabtastylab.com%2F023c2cee0f06c645d5c3d28e61b2e773%2F"));
        urlParameters.add(new BasicNameValuePair("dr", ""));
        urlParameters.add(new BasicNameValuePair("ea", "Eco Key"));
        urlParameters.add(new BasicNameValuePair("ec", "eco"));
        urlParameters.add(new BasicNameValuePair("el", "Eco Value"));
        urlParameters.add(new BasicNameValuePair("je", "false"));
        urlParameters.add(new BasicNameValuePair("pt", "Home%20%7C%20Luxury%20Travel%20Site"));
        urlParameters.add(new BasicNameValuePair("sd", "24-bits"));
        urlParameters.add(new BasicNameValuePair("sen", "0"));
        urlParameters.add(new BasicNameValuePair("sn", "1"));
        urlParameters.add(new BasicNameValuePair("sr", "1920x1080"));
        urlParameters.add(new BasicNameValuePair("t", "EVENT"));
        urlParameters.add(new BasicNameValuePair("ul", "fr-FR"));
        urlParameters.add(new BasicNameValuePair("vaid", "662810"));
        urlParameters.add(new BasicNameValuePair("vid", visitorid));
        urlParameters.add(new BasicNameValuePair("vp", "2400x451"));
        

        post.setEntity(new UrlEncodedFormEntity(urlParameters));

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(post)) {

        	// Get HttpResponse Status
            System.out.println(response.getStatusLine().toString());
            
            System.out.println(EntityUtils.toString(response.getEntity()));
        }

    }
    
    private void close() throws IOException {
        httpClient.close();
    }

}