package regression;

import org.testng.annotations.Test;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;

import pageObject.ObjectLibrary;

//*************************************************************************************************************
//*	                                Personalization Creation                   						          *
//*************************************************************************************************************

public class PersonalizationCreation {

	static ChromeDriver driver;
	private By by;
	static String parentWindow;
	public static String ReportURL;
	static String variationid;
	static String Customerid;
	static String testrowno;
	static String typeoftest;
	static String testenvironment;
	public static String browserName;
	public static String os;
	//static String chromedriverpath = "/usr/local/bin/chromedriver";
	static String chromedriverpath = "C:\\abTastySelenium\\drivers\\chromedriver.exe";
	static String browserpath = "--user-data-dir=/Users/monikapanda/Documents/Selenium/BrowserProfile/Profilenew";
	static String excelpath = System.getProperty("user.dir") + "/ABTastyData1.xlsx";

	@BeforeSuite
	public static void initialslackmsg() throws IOException {
		String testername = CommonLibrary.main_data_excel("tester", "Name");
		// EyeSlack.initialpost(testername ,
		// "https://hooks.slack.com/services/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");
	}

//*************************************************************************************************************
//*	                                Logging in into ABTasty                                                   *
//*************************************************************************************************************

	@Parameters({ "testenvironment" })
	@SuppressWarnings("deprecation")
	@Test(priority = 0)
	public static void Login(String testenv) throws InterruptedException, IOException {

		System.out.println("Login started");

		System.setProperty("webdriver.chrome.driver", chromedriverpath);

		// To check whether to test in staging or production

		testrowno = "1st";
		testenvironment = testenv;

		// String testtype = CommonLibrary.input_data_excel(testrowno, "typeofenv");
		if (testenv.equalsIgnoreCase("staging")) {
			// Initialize browser
			ChromeOptions options = new ChromeOptions();
			// options.addArguments("-incognito");
			options.setExperimentalOption("useAutomationExtension", false);
			options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));

			// to start chrome in headless mode
			options.addArguments("headless");
			options.addArguments("--window-size=1920,1080");
			options.addArguments("--no-sandbox");
			options.addArguments("--disable-dev-shm-usage");

			// DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			// capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			// driver=new ChromeDriver(capabilities);
			// options.addArguments(browserpath);
			System.out.println("staging");

			// Open the editor
			driver = new ChromeDriver(options);
			driver.get("https://staging-app2.abtasty.com/experiments");
			System.out.println("login " + driver.getCurrentUrl());
			if (driver.getPageSource().contains("403 ERROR")) {
				System.out.println("403 error on staging");
			}
		} else {
			// closing the extra line at top
			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("useAutomationExtension", false);
			options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
			// to start chrome in headless mode
			options.addArguments("headless");
			options.addArguments("--window-size=1920,1080");
			options.addArguments("--no-sandbox");
			options.addArguments("--disable-dev-shm-usage");

			driver = new ChromeDriver(options);
			driver.get("https://app2.abtasty.com/experiments");
			System.out.println("prod");
			System.out.println("login " + driver.getCurrentUrl());
		}
		// deriving the browser and os
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		browserName = caps.getBrowserName();
		String browserVersion = caps.getVersion();

		// Get OS name.
		os = System.getProperty("os.name").toLowerCase();
		System.out.println("browserName" + browserName + "browserVersion" + browserVersion + "os" + os);

		// login to the editor
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.id()).click();
		driver.findElement(ObjectLibrary.id()).sendKeys(CommonLibrary.main_data_excel("tester", "userid"));
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.password()).click();
		driver.findElement(ObjectLibrary.password()).sendKeys(CommonLibrary.main_data_excel("tester", "password"));
		driver.findElement(ObjectLibrary.signIn()).click();
		driver.manage().window().maximize();

		System.out.println("Login successfully done");
	}

//*************************************************************************************************************
//*	                                Changing account names                                                    *
//*************************************************************************************************************

	@Test(priority = 1)
	public static void selectAccount() throws Exception {

		System.out.println("select account names started");

		// Locate 'Account Names' drop down
		Thread.sleep(4000);
		if (driver.getPageSource().contains("Welcome to the new navigation experience!")) {
			driver.findElement(By.xpath("//*[@data-testid=\"onboardingModalCloseIcon\"]")).click();
		}
		// change account
		CommonLibrary.ChangeAccount(driver, "Mathumitha Murugan");
		Thread.sleep(1000);

		System.out.println("Select account names successfully done");
	}

//*************************************************************************************************************
//*	                               Simple Personalization Creation      	       	                          *
//*************************************************************************************************************

	@Test(priority = 2)
	public void testSimplePersonalization() throws Exception {

		System.out.println("Simple Personalization Creation.......");

		// click perso
		driver.findElement(By.xpath("//div[contains(text(), 'Personalization')]")).click();

		// create
		driver.findElement(By.cssSelector(".ButtonAction__button___35wKr")).click();
		// SP
		driver.findElement(By.cssSelector(":nth-child(1) > .MenuDropdown__subMenu___3K-FQ")).click();
		Thread.sleep(1000);
		driver.findElement(By.name("name")).sendKeys("SimplePersoAuto");

		driver.findElement(By.cssSelector("[data-testid=sampleURLField] > .Input__label___1E1EQ"))
				.sendKeys("http://abtastylab.com/48a0332f7e374f9c4437ce46f6f456bf/");
		Thread.sleep(1000);
		// Save&next
		driver.findElement(By.xpath("//button[contains(text(),'Save & Go to next step')]")).click();

		Thread.sleep(1000);
		System.out.println("check.......where");

		// Targeting
		driver.findElement(By.cssSelector("[data-testid=sectionOverview] > span")).click();
		driver.findElement(By.xpath("//*[text()= \"Target by events\"]")).click();
		Thread.sleep(1000);
		// target checkbox
		driver.findElement(By.xpath("//*[text()= \"Target by events\"]")).click();

		// save n next
		driver.findElement(ObjectLibrary.saveTarget()).click();

		// Editor
		Thread.sleep(7000);

		driver.findElement(ObjectLibrary.Elementtobechanged()).click();
		driver.switchTo().defaultContent();
		Thread.sleep(1000);
		WebElement menu = driver.findElement(ObjectLibrary.Edit());
		Actions act = new Actions(driver); 
		act.moveToElement(menu).perform(); 
		CommonLibrary.hoverAndClick(driver,driver.findElement(ObjectLibrary.Edit()),driver.findElement(ObjectLibrary.Editstyle()));
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.colourtab()).click();
		driver.findElement(ObjectLibrary.greencolor()).clear();
		driver.findElement(ObjectLibrary.greencolor()).sendKeys("#FFA1F2");
		driver.findElement(By.xpath("//*[@id=\"tabs-styleColor\"]/table/tbody/tr[6]/td[1]/label")).click();
		driver.findElement(ObjectLibrary.savechanges()).click();

		//adding an action tracking
		Thread.sleep(2000);
		driver.switchTo().frame(0);
		driver.findElement(By.cssSelector("#main .lead")).click();
		driver.switchTo().defaultContent();
		Thread.sleep(1000);
		Actions acti =  new Actions(driver);
		acti.moveToElement(driver.findElement(ObjectLibrary.actiontracking())).click().perform();
		driver.findElement(ObjectLibrary.actionname()).click();
		driver.findElement(ObjectLibrary.actionname()).sendKeys("Test_Action_SP");
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.saveaction()).click();

		// Goals
		driver.findElement(By.xpath("//p[contains(text(),'Goals')]")).click();
		driver.findElement(By.xpath("//span[contains(text(),'Test_Action_SP')]")).click();
		// save n next
		driver.findElement(By.xpath("//*[contains(@data-testid,'experimentBottomBar')]//button[2]")).click();

		// Traffic allocation
		driver.findElement(By.xpath("//input[@type = 'number']")).clear();
		driver.findElement(By.xpath("//input[@type = 'number']")).sendKeys("100");

		// click play button
		driver.findElement(By.xpath("//div[contains(text(),'Paused')]")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[text()= \"Play\"]")).click();

		Thread.sleep(40000);

		System.out.println("Simple Personalization Creation successfully done");
	}
//*************************************************************************************************************
//*	                                Verify the test                                                           *
//*************************************************************************************************************	

	@Test(priority = 3)
	public static void verifyTest() throws Exception {

		System.out.println("verifying test started");
		Thread.sleep(50000);

		String URL = CommonLibrary.input_data_excel(testrowno, "URL");
		((JavascriptExecutor) driver).executeScript("window.open(arguments[0])", URL);
		if (testenvironment.equalsIgnoreCase("staging")) {
			// intercepting network calls
			networkmocking.networkmock(driver);
		}

		int tabno = driver.getWindowHandles().size();
		ArrayList<String> windowHandles = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(windowHandles.get(tabno - 1));

		// refreshing the page
		Thread.sleep(1000);
		driver.navigate().refresh();
		Thread.sleep(1000);

		// calls the screenshot function
		Thread.sleep(1000);
		String date = CommonLibrary.getDateTime();
		CommonLibrary.takeSnapShot(driver, "Screenshots" + "/" + "Autotest" + date + ".jpg");

		if (driver.getPageSource().contains("#FFA1F2")) {
			System.out.println("color is present");
		} else {
			System.out.println("color is absent");

		}

		if (driver.getPageSource().contains("Test_Action_SP")) {
			System.out.println("Action tracker is present");
		} else {
			System.out.println("Action tracker is absent");
		}

		System.out.println("Verifying test successfully done");
	}

//*************************************************************************************************************
//*	                                Logging in into ABTasty                                                   *
//*************************************************************************************************************

	@Test(priority = 4)
	public static void Login1(String testenv) throws InterruptedException, IOException {

		System.out.println("Login started");

		System.setProperty("webdriver.chrome.driver", chromedriverpath);

		// To check whether to test in staging or production

		testrowno = "1st";
		testenvironment = testenv;

		// String testtype = CommonLibrary.input_data_excel(testrowno, "typeofenv");
		if (testenv.equalsIgnoreCase("staging")) {
			// Initialize browser
			ChromeOptions options = new ChromeOptions();
			// options.addArguments("-incognito");
			options.setExperimentalOption("useAutomationExtension", false);
			options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));

			// to start chrome in headless mode
			options.addArguments("headless");
			options.addArguments("--window-size=1920,1080");
			options.addArguments("--no-sandbox");
			options.addArguments("--disable-dev-shm-usage");

			// DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			// capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			// driver=new ChromeDriver(capabilities);
			// options.addArguments(browserpath);
			System.out.println("staging");

			// Open the editor
			driver = new ChromeDriver(options);
			driver.get("https://staging-app2.abtasty.com/experiments");
			System.out.println("login " + driver.getCurrentUrl());
			if (driver.getPageSource().contains("403 ERROR")) {
				System.out.println("403 error on staging");
			}
		} else {
			// closing the extra line at top
			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("useAutomationExtension", false);
			options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
			// to start chrome in headless mode
			options.addArguments("headless");
			options.addArguments("--window-size=1920,1080");
			options.addArguments("--no-sandbox");
			options.addArguments("--disable-dev-shm-usage");

			driver = new ChromeDriver(options);
			driver.get("https://app2.abtasty.com/experiments");
			System.out.println("prod");
			System.out.println("login " + driver.getCurrentUrl());
		}
		// deriving the browser and os
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		browserName = caps.getBrowserName();
		String browserVersion = caps.getVersion();

		// Get OS name.
		os = System.getProperty("os.name").toLowerCase();
		System.out.println("browserName" + browserName + "browserVersion" + browserVersion + "os" + os);

		// login to the editor
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.id()).click();
		driver.findElement(ObjectLibrary.id()).sendKeys(CommonLibrary.main_data_excel("tester", "userid"));
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.password()).click();
		driver.findElement(ObjectLibrary.password()).sendKeys(CommonLibrary.main_data_excel("tester", "password"));
		driver.findElement(ObjectLibrary.signIn()).click();
		driver.manage().window().maximize();

		System.out.println("Login successfully done");
	}

//*************************************************************************************************************
//*	                                Changing account names                                                    *
//*************************************************************************************************************

	@Test(priority = 5)
	public static void selectAccount1() throws Exception {

		System.out.println("select account names started");

		// Locate 'Account Names' drop down
		Thread.sleep(4000);
		if (driver.getPageSource().contains("Welcome to the new navigation experience!")) {
			driver.findElement(By.xpath("//*[@data-testid=\"onboardingModalCloseIcon\"]")).click();
		}
		// change account
		CommonLibrary.ChangeAccount(driver, "Mathumitha Murugan");
		Thread.sleep(1000);

		System.out.println("Select account names successfully done");
	}

//*************************************************************************************************************
//*	                               MultiPage Personalization Creation      	       	                          *
//*************************************************************************************************************

	@Test(priority = 6)
	public void testMultiPagePersonalization() throws Exception {

		// click perso
		driver.findElement(By.xpath("//div[contains(text(), 'Personalization')]")).click();
		Thread.sleep(2000);

		// create
		driver.findElement(By.cssSelector(".ButtonAction__button___35wKr")).click();
		// MPP
		driver.findElement(By.cssSelector(":nth-child(2) > .MenuDropdown__subMenu___3K-FQ")).click();

		Thread.sleep(1000);
		driver.findElement(By.name("name")).sendKeys("MPP_auto");
		// page1
		driver.findElement(By.xpath("//li[1]/div[1]/div[1]/div[1]/div[2]/label/input[@name = 'url']"))
				.sendKeys("http://abtastylab.com/48a0332f7e374f9c4437ce46f6f456bf/");
		driver.findElement(By.xpath("//li[1]//*[@name = 'name']")).sendKeys("Page 1");

		// Page2
		driver.findElement(By.xpath("//li[2]//*[@name = 'url']"))
				.sendKeys("http://abtastylab.com/48a0332f7e374f9c4437ce46f6f456bf/destinations.php");
		driver.findElement(By.xpath("//li[2]//*[@name = 'name']")).sendKeys("Page 2");

		Thread.sleep(1000);

		// Save&next
		driver.findElement(By.xpath("//button[contains(text(),'Save & Go to next step')]")).click();

		Thread.sleep(1000);
		System.out.println("check.......where");

		// Targeting
		driver.findElement(By.cssSelector("[data-testid=sectionOverview] > span")).click();
		driver.findElement(By.xpath("//*[text()= \"Target by events\"]")).click();
		Thread.sleep(1000);
		// target checkbox
		driver.findElement(By.xpath("//*[text()= \"Target by events\"]")).click();

		// save n next
		driver.findElement(By.xpath("//button[contains(text(),'Save & Go to next step')]")).click();

		// Page 1 Editor
		Thread.sleep(6000);
		driver.switchTo().frame(0);
		driver.findElement(ObjectLibrary.Elementtobechanged()).click();
		driver.switchTo().defaultContent();
		Thread.sleep(1000);
		WebElement menu = driver.findElement(ObjectLibrary.Edit());
		Actions act = new Actions(driver); 
		act.moveToElement(menu).perform(); 
		CommonLibrary.hoverAndClick(driver,driver.findElement(ObjectLibrary.Edit()),driver.findElement(ObjectLibrary.Editstyle()));
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.colourtab()).click();
		driver.findElement(ObjectLibrary.greencolor()).clear();
		driver.findElement(ObjectLibrary.greencolor()).sendKeys("#FFA1F2");
		driver.findElement(By.xpath("//*[@id=\"tabs-styleColor\"]/table/tbody/tr[6]/td[1]/label")).click();
		driver.findElement(ObjectLibrary.savechanges()).click();

		//adding an action tracking
		Thread.sleep(2000);
		driver.switchTo().frame(0);
		driver.findElement(By.cssSelector("#main .lead")).click();
		driver.switchTo().defaultContent();
		Thread.sleep(1000);
		Actions actTrack =  new Actions(driver);
		actTrack.moveToElement(driver.findElement(ObjectLibrary.actiontracking())).click().perform();
		driver.findElement(ObjectLibrary.actionname()).click();
		driver.findElement(ObjectLibrary.actionname()).sendKeys("Test_Action1");
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.saveaction()).click();

		// Page 2 editor
		driver.findElement(By.xpath("//span[contains(text(),'Page 2')]")).click();
		Thread.sleep(7000);
		driver.switchTo().frame(0);
		driver.findElement(ObjectLibrary.Elementtobechanged()).click();
		driver.switchTo().defaultContent();
		Thread.sleep(1000);
		WebElement menu2 = driver.findElement(ObjectLibrary.Edit());
		Actions act2 = new Actions(driver); 
		act2.moveToElement(menu2).perform(); 
		CommonLibrary.hoverAndClick(driver,driver.findElement(ObjectLibrary.Edit()),driver.findElement(ObjectLibrary.Editstyle()));
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.colourtab()).click();
		driver.findElement(ObjectLibrary.greencolor()).clear();
		driver.findElement(ObjectLibrary.greencolor()).sendKeys("#FFA1F2");
		driver.findElement(By.xpath("//*[@id=\"tabs-styleColor\"]/table/tbody/tr[6]/td[1]/label")).click();
		driver.findElement(ObjectLibrary.savechanges()).click();

		//adding an action tracking
		Thread.sleep(2000);
		driver.switchTo().frame(0);
		driver.findElement(By.cssSelector("#main .lead")).click();
		driver.switchTo().defaultContent();
		Thread.sleep(1000);
		Actions actTrack2 =  new Actions(driver);
		actTrack2.moveToElement(driver.findElement(ObjectLibrary.actiontracking())).click().perform();
		driver.findElement(ObjectLibrary.actionname()).click();
		driver.findElement(ObjectLibrary.actionname()).sendKeys("Test_Action2");
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.saveaction()).click();

		// Goals
		driver.findElement(By.xpath("//p[contains(text(),'Goals')]")).click();
		driver.findElement(By.xpath("//span[contains(text(),'Test_Action1')]")).click();
		driver.findElement(By.xpath("//span[contains(text(),'Test_Action2')]")).click();
		// save n next
		driver.findElement(By.xpath("//button[contains(text(),'Save & Go to next step')]")).click();

		// Traffic allocation
		driver.findElement(By.xpath("//input[@type = 'number']")).clear();
		driver.findElement(By.xpath("//input[@type = 'number']")).sendKeys("100");

		// click play button
		driver.findElement(By.xpath("//div[contains(text(),'Paused')]")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[text()= \"Play\"]")).click();

		Thread.sleep(40000);

		System.out.println(" MultiPage Personalization creation ended");
	}

//*************************************************************************************************************
//*	                                Verify the test                                                           *
//*************************************************************************************************************	

	@Test(priority = 7)
	public static void verifyTest1() throws Exception {

		System.out.println("verifying test started");
		Thread.sleep(50000);

		String URL = CommonLibrary.input_data_excel(testrowno, "URL");
		((JavascriptExecutor) driver).executeScript("window.open(arguments[0])", URL);
		if (testenvironment.equalsIgnoreCase("staging")) {
			// intercepting network calls
			networkmocking.networkmock(driver);
		}

		int tabno = driver.getWindowHandles().size();
		ArrayList<String> windowHandles = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(windowHandles.get(tabno - 1));

		// refreshing the page
		Thread.sleep(1000);
		driver.navigate().refresh();
		Thread.sleep(1000);

		// calls the screenshot function
		Thread.sleep(1000);
		String date = CommonLibrary.getDateTime();
		CommonLibrary.takeSnapShot(driver, "Screenshots" + "/" + "Autotest" + date + ".jpg");

		if (driver.getPageSource().contains("#FFA1F2")) {
			System.out.println("color is present");
		} else {
			System.out.println("color is absent");

		}

		if (driver.getPageSource().contains("Test_Action1")) {
			System.out.println("Action tracker is present");
		} else {
			System.out.println("Action tracker is absent");

		}

		driver.findElement(By.xpath("//a[normalize-space()='Destinations']")).click();
		Thread.sleep(2000);
		if (driver.getPageSource().contains("#FFA1F2")) {
			System.out.println("color is present");
		} else {
			System.out.println("color is absent");

		}

		if (driver.getPageSource().contains("Test_Action2")) {
			System.out.println("Action tracker is present");
		} else {
			System.out.println("Action tracker is absent");

		}

	System.out.println("Verifying test successfully done");

	}

//*************************************************************************************************************
//*	                                Closing the drive                                        	              *
//*************************************************************************************************************	

	@AfterSuite
	public void closedriver() throws IOException {
		driver.quit();
	}

}
