package regression;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

public class EyeSlack {
	  
	  public static void post(String testResults , String testDuration, String testername, int steps, String slackWebhookURL, String reportURL, int passedTC, int failedTC, String OS, String browsername)
	  {
		  // Define the color of the slack message 
		  String messageColor = "";
		  
		  // Define the default message for the Slack notification
		  String fallback = "Applitools Test Results";
		  
		  // Check if test was aborted
		  if(testResults.equals("Failed")) {
			  messageColor = "FF0000"; // Red
		      fallback = "Test Failed: ";
		  }
		  else {
			  messageColor = "36a64f"; // Green
		      fallback = "Test Successfully Completed: ";
		  }
		  System.out.println(fallback);

		  // build the httpClient object which will send our request to Slack Webhook
		  HttpClient httpClient = HttpClientBuilder.create().build();

		  try {

			  // build the HttpPost request object
		      HttpPost request = new HttpPost(slackWebhookURL);
     
		      // build the HTTP request 
		      StringEntity params =new StringEntity("{\n" + 
			      		"\"attachments\": [\n" + 
			      		"{\n" + 
			      		"\"fallback\": \"" + fallback + ".\",\n" + 
			      		"\"color\": \"#" + messageColor + "\",\n" + 
			      		"\"pretext\": \"" + testResults + "\",\n" + 
			      		"\"author_name\": \"Reports\",\n" + 
			      		"\"author_link\": \"" + reportURL + "\",\n" + 
			      		//"\"author_icon\": \"Test by\" + \"" + testername + "\",\n" + 
			      		"\"title\": \"See Results\",\n" + 
			      		"\"title_link\": \"file:///Users/monikapanda/qa-automation/test-output/custom-emailable-report.html\",\n" + 
			      		"\"fields\": [\n" + 
			      		"{\n" + 
			      		"\"title\": \"Test Duration\",\n" + 
			      		"\"value\": \"" + testDuration + "\",\n" + 
			      		"\"short\": true\n" + 
			      		"}\n," +
			      		"{\n" + 
			      		"\"title\": \" Passed Tests\",\n" + 
			      		"\"value\": \"" + passedTC + "\",\n" + 
			      		"\"short\": true\n" + 
			      		"}\n," +
			      		"{\n" + 
			      		"\"title\": \"Failed Tests\",\n" + 
			      		"\"value\": \"" + failedTC + "\",\n" + 
			      		"\"short\": true\n" + 
			      		"}\n," +
			      		/*"{\n" + 
			      		"\"title\": \"Branch\",\n" + 
			      		"\"value\": \"" + testResults.getBranchName() + "\",\n" + 
			      		"\"short\": true\n" + 
			      		"}\n," +*/
			      		 "{\n" + 
			      		"\"title\": \"OS\",\n" + 
			      		"\"value\": \"" + OS + "\",\n" + 
			      		"\"short\": true\n" + 
			      		"}\n," + 
			      		"{\n" + 
			      		"\"title\": \"Browser\",\n" + 
			      		"\"value\": \"" + browsername + "\",\n" + 
			      		"\"short\": true\n" + 
			      		"}\n," +
			      		"{\n" + 
			      		"\"title\": \"Tested by\",\n" + 
			      		"\"value\": \"" + testername + "\",\n" + 
			      		"\"short\": true\n" + 
			      		"}\n," +
			      		/*"{\n" + 
			      		"\"title\": \"Duration\",\n" + 
			      		"\"value\": \"" + testResults.getDuration() + " Seconds\",\n" + 
			      		"\"short\": true\n" + 
			      		"}\n," +*/	      		
			      		"{\n" + 
			      		"\"title\": \"Steps\",\n" + 
			      		"\"value\": \"" + steps + " Steps\",\n" + 
			      		"\"short\": true\n" + 
			      		"}\n" +
			      		"],\n" + 
			      		/*"\"footer\": \"Test Start Time\",\n" + 
			      		"\"footer_icon\": \"https://applitools.com/images/favicon.ico\",\n" + 
			      		"\"ts\": " + testResults.getStartedAt().getTimeInMillis()/1000 + "\n" + */
			      		"}\n" + 
			      		"]\n" + 
			      		"}");
		      request.addHeader("content-type", "application/json");
		      request.setEntity(params);
		      
		      // Executes the HTTP request
		      HttpResponse response = httpClient.execute(request);
		      System.out.println(response.getStatusLine().getStatusCode());
		      
		  }catch (Exception ex) {
		      //handle exception here
		  } 
	  } 
	  
	  public static void initialpost(String testername, String slackWebhookURL)
	  {
		  
		  // build the httpClient object which will send our request to Slack Webhook
		  HttpClient httpClient = HttpClientBuilder.create().build();

		  try {

			  // build the HttpPost request object
		      HttpPost request = new HttpPost(slackWebhookURL);
     
		      // build the HTTP request 
		      StringEntity params =new StringEntity("{\n" + 
			      		"\"attachments\": [\n" + 
			      		"{\n" + 
			      		"\"fallback\": \"" +  "1" + ".\",\n" + 
			      		"\"color\": \"#" + "2" + "\",\n" + 
			      		"\"fields\": [\n" + 
			      		"{\n" + 
			      		"\"title\": \"Test is in progress by\",\n" + 
			      		"\"value\": \"" + testername + "\",\n" + 
			      		"\"short\": true\n" + 
			      		"}\n," +
			      		"],\n" + 
			      		"}\n" + 
			      		"]\n" + 
			      		"}");
		      request.addHeader("content-type", "application/json");
		      request.setEntity(params);
		      
		      // Executes the HTTP request
		      HttpResponse response = httpClient.execute(request);
		      System.out.println(response.getStatusLine().getStatusCode());
		      
		  }catch (Exception ex) {
		      //handle exception here
		  } 
	  } 
	  
	  public static void postwithoutreport(String testResults , String testDuration, String testername, int steps, String slackWebhookURL, int passedTC, int failedTC, String OS, String browsername)
	  {
		  // Define the color of the slack message 
		  String messageColor = "";
		  
		  // Define the default message for the Slack notification
		  String fallback = "Applitools Test Results";
		  
		  // Check if test was aborted
		  if(testResults.equals("Failed")) {
			  messageColor = "FF0000"; // Red
		      fallback = "Test Failed: ";
		  }
		  else {
			  messageColor = "36a64f"; // Green
		      fallback = "Test Successfully Completed: ";
		  }
		  System.out.println(fallback);

		  // build the httpClient object which will send our request to Slack Webhook
		  HttpClient httpClient = HttpClientBuilder.create().build();

		  try {

			  // build the HttpPost request object
		      HttpPost request = new HttpPost(slackWebhookURL);
     
		      // build the HTTP request 
		      StringEntity params =new StringEntity("{\n" + 
			      		"\"attachments\": [\n" + 
			      		"{\n" + 
			      		"\"fallback\": \"" + fallback + ".\",\n" + 
			      		"\"color\": \"#" + messageColor + "\",\n" + 
			      		"\"pretext\": \"" + testResults + "\",\n" + 
			      		//"\"author_name\": \"Reports\",\n" + 
			      		//"\"author_link\": \"" + reportURL + "\",\n" + 
			      		//"\"author_icon\": \"Test by\" + \"" + testername + "\",\n" + 
//			      		"\"title\": \"See Results\",\n" + 
//			      		"\"title_link\": \"file:///Users/monikapanda/qa-automation/test-output/custom-emailable-report.html\",\n" + 
			      		"\"fields\": [\n" + 
			      		"{\n" + 
			      		"\"title\": \"Test Duration\",\n" + 
			      		"\"value\": \"" + testDuration + "\",\n" + 
			      		"\"short\": true\n" + 
			      		"}\n," +
			      		"{\n" + 
			      		"\"title\": \" Passed Tests\",\n" + 
			      		"\"value\": \"" + passedTC + "\",\n" + 
			      		"\"short\": true\n" + 
			      		"}\n," +
			      		"{\n" + 
			      		"\"title\": \"Failed Tests\",\n" + 
			      		"\"value\": \"" + failedTC + "\",\n" + 
			      		"\"short\": true\n" + 
			      		"}\n," +
			      		/*"{\n" + 
			      		"\"title\": \"Branch\",\n" + 
			      		"\"value\": \"" + testResults.getBranchName() + "\",\n" + 
			      		"\"short\": true\n" + 
			      		"}\n," +*/
			      		 "{\n" + 
				      	"\"title\": \"OS\",\n" + 
				      	"\"value\": \"" + OS + "\",\n" + 
				      	"\"short\": true\n" + 
				      	"}\n," + 
				      	//"{\n" + 
//				      	"\"title\": \"Browser\",\n" + 
//				      	"\"value\": \"" + browsername + "\",\n" + 
//				      	"\"short\": true\n" + 
//				      	"}\n," +
//			      		"{\n" + 
//			      		"\"title\": \"Tested by\",\n" + 
//			      		"\"value\": \"" + testername + "\",\n" + 
//			      		"\"short\": true\n" + 
//			      		"}\n," +
			      		/*"{\n" + 
			      		"\"title\": \"Duration\",\n" + 
			      		"\"value\": \"" + testResults.getDuration() + " Seconds\",\n" + 
			      		"\"short\": true\n" + 
			      		"}\n," +*/	      		
//			      		"{\n" + 
//			      		"\"title\": \"Steps\",\n" + 
//			      		"\"value\": \"" + steps + " Steps\",\n" + 
//			      		"\"short\": true\n" + 
//			      		"}\n" +
			      		"],\n" + 
			      		/*"\"footer\": \"Test Start Time\",\n" + 
			      		"\"footer_icon\": \"https://applitools.com/images/favicon.ico\",\n" + 
			      		"\"ts\": " + testResults.getStartedAt().getTimeInMillis()/1000 + "\n" + */
			      		"}\n" + 
			      		"]\n" + 
			      		"}");
		      request.addHeader("content-type", "application/json");
		      request.setEntity(params);
		      
		      // Executes the HTTP request
		      HttpResponse response = httpClient.execute(request);
		      System.out.println(response.getStatusLine().getStatusCode());
		      
		  }catch (Exception ex) {
		      //handle exception here
		  } 
	  } 
}	 