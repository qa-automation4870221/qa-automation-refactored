package regression;

import java.io.IOException;
import java.time.Duration;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObject.ObjectLibrary;

public class target {

	public static void setTarget(String testrowno,ChromeDriver driver) throws InterruptedException, IOException {
		if (driver.getWindowHandles().size() > 1) {
			//Switch to new window opened
			for(String winHandle : driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
		    }
			//Thread.sleep(5000);
			String typeoftest = CommonLibrary.input_data_excel(testrowno, "typeoftest");
			if(typeoftest.equalsIgnoreCase("Personalisation")) {
				
				WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
				wait.until(ExpectedConditions.visibilityOfElementLocated(ObjectLibrary.perstrigger()));
				WebElement element = driver.findElement(ObjectLibrary.perstrigger());
			    ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
			    Thread.sleep(1000);
			    element.click();
			    Thread.sleep(1000);
			    driver.findElement(ObjectLibrary.perstriggeraudience()).click();
//				Actions action = new Actions(driver);    
//				WebElement optionsList = driver.findElement(By.xpath("//div[contains(@class, 'css-1tx5nsb-menu')]"));
//				action.moveToElement(optionsList);
//
//				List<WebElement> options = driver.findElements(By.xpath("//div[contains(@class, 'Select__optionValue___2a670')]"));
//				for(WebElement option : options) {
//				    if (option.getText().equals("All visitors")) {
//				        option.click();
//				        break;
//				    }
//				}
			    element = driver.findElement(By.xpath("//*[text()='Select a trigger']"));
			    ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
			    Thread.sleep(1000);
			    element.click();
				driver.findElement(By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div[2]/div/div[1]/div[3]/div[3]/div[2]/div/div/div/div[2]/div[1]/div/label/input")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div[2]/div/div[1]/div[3]/div[3]/div[2]/div/div/div/div[2]/div[1]/div/label/input")).sendKeys("Device trigger");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div[2]/div/div[1]/div[3]/div[3]/div[2]/div/div/div/div[2]/div[2]/div/div[1]/div/span")).click();
				Thread.sleep(1000);
				//CommonLibrary.HoverAndClick(driver,driver.findElement(By.xpath("//div[contains(@class, 'styles__fadeOnHover___32GOV')]")),driver.findElement(By.xpath("//button[contains(@class, 'ButtonSecondary__button___fNmae')]")));	
				//driver.findElement(By.xpath("/html/body/div[11]/div/div/div/div[2]/div[2]/div[2]/div/p")).click();
				driver.findElement(ObjectLibrary.saveTarget()).click();
			}
			if (typeoftest.equalsIgnoreCase("AB Test")){

				Thread.sleep(2000);
				if (driver.getPageSource().contains("WE'D LIKE TO HEAR FROM YOU")) {
					driver.findElement(By.xpath("//*[contains(@class,\"abtasty-csat-toaster-close\")]")).click();
				}
					
				 @SuppressWarnings("deprecation")
				 WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
			     wait.until(ExpectedConditions.visibilityOfElementLocated(ObjectLibrary.wheresection()));
				 driver.findElement(ObjectLibrary.wheresection()).click();
				 Thread.sleep(1000);
				 driver.findElement(ObjectLibrary.targetevents()).click();
				 Thread.sleep(1000);
				 driver.findElement(ObjectLibrary.targetevents()).click();

//		    WebDriverWait wait = new WebDriverWait(driver, 20);
//			wait.until(ExpectedConditions.visibilityOfElementLocated(objectlibrary.trigger()));
//			driver.findElement(objectlibrary.trigger()).click();
//		    Thread.sleep(1000);
//		    driver.findElement(objectlibrary.triggeraudience()).click();
//		    Thread.sleep(2000);
//		    driver.findElement(By.xpath("//*[@id=\"react-select-2-option-0\"]"));

//		    WebElement element = driver.findElement(By.xpath("//*[contains(text(),\"New trigger\")]"));
//		    //((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
//		    Thread.sleep(1000);
//		    element.click();
//			driver.findElement(objectlibrary.Targetfilter()).click();
//			Thread.sleep(1000);
//			driver.findElement(objectlibrary.Targetselect()).click();
//			driver.findElement(objectlibrary.Targetselect()).sendKeys("Browser");
//			Thread.sleep(1000);
//			//CommonLibrary.HoverAndClick(driver,driver.findElement(By.xpath("//div[contains(@class, 'styles__fadeOnHover___32GOV')]")),driver.findElement(By.xpath("//button[contains(@class, 'ButtonSecondary__button___fNmae')]")));
//			driver.findElement(By.xpath("/html/body/div[12]/div/div/div/div[2]/div[2]/div[2]/div/p")).click();

			Thread.sleep(500);
			WebElement element = driver.findElement(ObjectLibrary.saveTarget());
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
			Thread.sleep(1000);
			//element.click();
			driver.executeScript("arguments[0].click();", element);
	}	
			
			//for MP Test
			if(typeoftest.equalsIgnoreCase("Multipage test")) {
				
				Thread.sleep(2000);
				if (driver.getPageSource().contains("WE'D LIKE TO HEAR FROM YOU")) {
					driver.findElement(By.xpath("//*[contains(@class,\"abtasty-csat-toaster-close\")]")).click();
				}
				
				WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
			     wait.until(ExpectedConditions.visibilityOfElementLocated(ObjectLibrary.MPwheresection()));
				 driver.findElement(ObjectLibrary.MPwheresection()).click();
				 Thread.sleep(1000);
				 driver.findElement(By.xpath("//*[text()= \"Target by events\"]")).click();
				 Thread.sleep(1000);
				 driver.findElement(By.xpath("//*[text()= \"Target by events\"]")).click();
				
			
			//FOR SELECTING 2ND SUBTEST
			   Thread.sleep(1000);
			    WebElement element = driver.findElement(By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div/div[2]/div/div[1]/div[2]/div/div/div/div/div/div/div/div"));
			    element.click();
			    driver.findElement(By.xpath("//*[contains(text(),'test2')]")).click();
			
			//to select the All Visitors
//			    wait = new WebDriverWait(driver, 20);
//				wait.until(ExpectedConditions.visibilityOfElementLocated(objectlibrary.MPtrigger()));
//				element = driver.findElement(objectlibrary.MPtrigger());
//			    ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
//			    Thread.sleep(1000);
//			    driver.findElement(objectlibrary.MPtriggerAudience()).click();
//			    element = driver.findElement(By.xpath("//*[text()='Select a trigger']"));
//			    ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
//			    Thread.sleep(1000);
//			    element.click();
//				driver.findElement(By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div[2]/div/div[1]/div[3]/div[3]/div[2]/div/div/div/div[2]/div[1]/div/div/label/input")).click();
//				Thread.sleep(1000);
//				driver.findElement(By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div[2]/div/div[1]/div[3]/div[3]/div[2]/div/div/div/div[2]/div[1]/div/div/label/input")).sendKeys("Device trigger");
//				Thread.sleep(1000);
//				driver.findElement(By.xpath("//*[@id=\"abtasty-app\"]/div/div/div[1]/div/div/div[2]/div/div[1]/div[3]/div[3]/div[2]/div/div/div/div[2]/div[2]/div/div[1]/div/span")).click();
//				Thread.sleep(1000);
				
				 wait = new WebDriverWait(driver, Duration.ofSeconds(20));
			     wait.until(ExpectedConditions.visibilityOfElementLocated(ObjectLibrary.MPwheresection()));
				 driver.findElement(By.xpath("//*[text()= \"Target by events\"]")).click();
				 Thread.sleep(1000);
				 driver.findElement(By.xpath("//*[text()= \"Target by events\"]")).click();
				// driver.findElement(objectlibrary.saveTarget()).click();
				 WebElement ele = driver.findElement(ObjectLibrary.saveTarget());
				 driver.executeScript("arguments[0].click();", ele);
    }
}
	
	public static void audienceCondition(WebDriver driver, String incexc, String value, String targetName) throws InterruptedException {
		Thread.sleep(1000);
		driver.findElement(By.className("Select__singleValue___2rcn8")).click();
	    //to select the include/exclude
		Actions action = new Actions(driver);    
		WebElement optionsList = driver.findElement(By.xpath("//div[contains(@class, 'css-11unzgr')]"));
		action.moveToElement(optionsList);
	 
		List<WebElement> options = driver.findElements(By.xpath("//div[contains(@class, 'Select__optionValue___2a670')]"));
		for(WebElement option : options) {
		   if (option.getText().equals(incexc)) {
				 option.click();
				 break;
			}
	    }
		//to select the value
	    driver.findElement(By.className("css-1tzmkcy")).click();
	    
//	    if(targetName.equals("Campaign Exposure")) {
	    	optionsList = driver.findElement(By.xpath("//div[contains(@class, 'css-11unzgr')]"));
			action.moveToElement(optionsList);

			options = driver.findElements(By.xpath("//div[contains(@class, 'Select__optionValue___2a670')]"));
			for(WebElement option : options) {
			    if (option.getText().equals(value)) {
			        option.click();
			        break;
			    }	
			}
//	    }
//	    else {
//		optionsList = driver.findElement(By.xpath("//div[contains(@class, 'css-101ifdd-menu')]"));
//		action.moveToElement(optionsList);
//
//		options = driver.findElements(By.xpath("//div[contains(@class, 'Select__optionValue___2a670')]"));
//		for(WebElement option : options) {
//		    if (option.getText().equals(value)) {
//		        option.click();
//		        break;
//		    }
//		}
//	    } 	
}
}
