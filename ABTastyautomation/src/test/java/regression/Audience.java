package regression;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import pageObject.ObjectLibrary;

public class Audience {
    static WebDriver driver;
    static String parentWindow;
    static String incexc = "";
    static String value = "";
    static String value2 = "";
    static String value3 = "";

    public static void main(String[] args) throws InterruptedException, IOException {

        Login();
        audiencetemplate();
    }

    //*************************************************************************************************************
    //*	                                Logging in into ABTasty                                                   *
    //*************************************************************************************************************
    public static void Login() throws InterruptedException, IOException {

        System.setProperty("webdriver.chrome.driver", "C:\\abTastySelenium\\drivers\\chromedriver.exe");

        //To check whether to test in staging or production

        String testrowno = "1st";

        String testtype = CommonLibrary.input_data_excel(testrowno, "typeofenv");
        if (testtype.equalsIgnoreCase("staging")) {
            //Initialize browser
            ChromeOptions options = new ChromeOptions();
            options.addArguments("-incognito");
//			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
//			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
            //  driver=new ChromeDriver(capabilities);
            options.addArguments("--user-data-dir=/Users/monikapanda/Documents/Selenium/BrowserProfile/Profilenew");

            //Open the editor
            driver = new ChromeDriver(options);
            driver.get("https://staging-app2.abtasty.com/experiments");
        } else {
            ChromeOptions options = new ChromeOptions();
            options.setExperimentalOption("useAutomationExtension", false);
            options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
            driver = new ChromeDriver(options);
            driver.get("https://app2.abtasty.com/experiments");
        }
        //deriving the browser and os
//		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
//		browserName = caps.getBrowserName();
//		String browserVersion = caps.getVersion();
//
//		os = System.getProperty("os.name").toLowerCase();
//			System.out.println("browserName" + browserName + "browserVersion" + browserVersion + "os" + os);
//		//Get OS name.
//		os = System.getProperty("os.name").toLowerCase();
//		System.out.println("browserName" + browserName + "browserVersion" + browserVersion + "os" + os);

        //login to the editor
        Thread.sleep(1000);
        driver.findElement(ObjectLibrary.id()).click();

        driver.findElement(ObjectLibrary.id()).sendKeys(CommonLibrary.main_data_excel("tester", "userid"));

        Thread.sleep(1000);
        driver.findElement(ObjectLibrary.password()).click();
        driver.findElement(ObjectLibrary.password()).sendKeys(CommonLibrary.main_data_excel("tester", "password"));
        driver.findElement(ObjectLibrary.signIn()).click();
        driver.manage().window().maximize();
    }
    //*************************************************************************************************************
    //*	                                Changing account names                                                    *
    //*************************************************************************************************************

    public static void selectAccount() throws InterruptedException, IOException {

        //Locate 'Account Names' drop down
        Thread.sleep(3000);
        WebElement accountnames = driver.findElement(ObjectLibrary.accountNames());
        accountnames.click();
        WebElement name = driver.findElement(ObjectLibrary.searchAccName());
        name.sendKeys(CommonLibrary.input_data_excel("1st", "accountname"));
        Thread.sleep(1000);
        name.sendKeys(Keys.RETURN);
        //choosing personalisation test if required
        Thread.sleep(5000);
        if (CommonLibrary.input_data_excel("1st", "typeoftest").equalsIgnoreCase("Personalisation")) {
            CommonLibrary.hoverAndClick(driver, driver.findElement(ObjectLibrary.campaign()), driver.findElement(ObjectLibrary.personalisation()));
        }

        Thread.sleep(2000);
        //closing the pop in
        if (driver.getPageSource().contains("stroke: rgb(92, 92, 92);")) {
            driver.findElement(By.xpath("/html/body/abtasty-modal/div/div")).click();
        }
    }

    //*************************************************************************************************************
    //*	                                Adding audience from blank                                                *
    //*************************************************************************************************************

    public static void audiencecreate() throws InterruptedException {
        Thread.sleep(2000);
        //going to the Audience tab
        driver.findElement(By.xpath("//*[@id=\"turfuHeader\"]/div[2]/div[1]/turfu-menu/div[2]/a/span")).click();
        Thread.sleep(2000);
        if (driver.getPageSource().contains("stroke: rgb(92, 92, 92);")) {
            driver.findElement(By.xpath("/html/body/abtasty-modal/div/div")).click();
        }
        driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[1]/div/div/div[2]/ul/a[1]/li")).click();
        //Click on create from blank
        Thread.sleep(2000);
        driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/ul/li[1]/div/span")).click();
        driver.findElement(By.xpath("//input[contains(@class, 'Input__commonInput___ALaWt')]")).sendKeys("Visitor status");

        //search for the segment name
        driver.findElement(By.xpath("//input[contains(@class, 'Input__search___1mVSy')]")).click();
        driver.findElement(By.xpath("//input[contains(@class, 'Input__search___1mVSy')]")).sendKeys("New");

        //to drag and drop
        Thread.sleep(2000);

        WebElement From = driver.findElement(By.xpath("//span[contains(@class, 'icons__pulsar-drag___3oCNq')]"));
        //<span class="icons__pulsar-drag___3oCNq styles__iconDrag___2h2uA"></span>

        WebElement To = driver.findElement(By.xpath("//div[contains(@class, 'styles__containerDroppableZone___1dph_')]"));
        //<div class="styles__containerDroppableZone___1dph_ styles__fullSize___1KGiM"><div class="styles__msgStartAudience___1dv1A"><figure><img src="/pulsar/091d3420d55b.png"></figure><p>Drag &amp; Drop criteria from the left column to here</p></div></div>
        Actions action = new Actions(driver);

        //Dragged and dropped.
        // action.dragAndDrop(From, To).build().perform();
//		action.dragAndDropBy(From, 894, 390).build().perform();
//		utils.driver.executeScript("alert('Focus window')");
//		utils.driver.switchTo().alert().accept();

        Action dragAndDrop = action.clickAndHold(From)
                .moveToElement(To)
                .release(To)
                .build();

        dragAndDrop.perform();
        Thread.sleep(2000);
        driver.findElement(By.xpath("/html/body/div[12]/div/div/div[1]/div[2]/div[2]/div/div/div[1]/div/div[1]/div/div/div[1]/div/div[2]/div/ul/div/div/div[1]/div/div/div/div[1]/div[1]")).click();
        WebElement optionsList = driver.findElement(By.xpath("//div[contains(@class, 'css-11unzgr')]"));
        action.moveToElement(optionsList);

        List<WebElement> options = driver.findElements(By.xpath("//div[contains(@class, 'Select__optionValue___2a670')]"));
        for (WebElement option : options) {
            if (option.getText().equals("New visitors")) {
                option.click();
                break;
            }
        }
        driver.findElement(By.xpath("/html/body/div[12]/div/div/div[2]/div/div/button")).click();
    }

    //*************************************************************************************************************
    //*	                             Adding audience from template                                                *
    //*************************************************************************************************************

    public static void audiencetemplate() throws InterruptedException {
        Thread.sleep(6000);
        //closing the pop in
        if (driver.getPageSource().contains("stroke: rgb(92, 92, 92);")) {
            driver.findElement(By.xpath("/html/body/abtasty-modal/div/div")).click();
        }
        //going to the Audience tab
        Thread.sleep(1000);
        driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/nav/div[2]/div[1]/div[2]/a")).click();
        //	driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/nav/div[2]/div[1]/div[2]/a")).click();
        Thread.sleep(2000);
        //Click on create from template
        // CommonLibrary.HoverAndClick(driver,driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[1]/div/div/div[1]/div[2]/button/div/div[2]/span")),driver.findElement(By.xpath("//div[contains(@class, 'MenuDropdown__withSubMenu___2bLdo')]")));
        driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[1]/div/div/div[1]/div[2]/button/div/div[2]/span")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[text()='From templates']")).click();
    }

    //*************************************************************************************************************
    //*	                             Creating new campaign from audience                                         *
    //*************************************************************************************************************

    public static void campaignfromaudience() throws InterruptedException {

        Thread.sleep(1000);
        //going to the Audience tab
        driver.findElement(By.xpath("//*[@id=\"turfuHeader\"]/div[2]/div[1]/turfu-menu/div[2]/a/span")).click();
        Thread.sleep(2000);
        CommonLibrary.hoverAndClick(driver, driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[2]/table/tbody/tr[1]/td[5]")), driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[2]/table/tbody/tr[1]/td[6]/div/div/button")));
    }

    //*************************************************************************************************************
    //*	                                Editing an existing test                                                  *
    //*************************************************************************************************************

    public static void editABTest() throws InterruptedException, IOException {
        //Searching for the particular test id
        Thread.sleep(2000);
        driver.findElement(ObjectLibrary.searchTestField()).click();
        driver.findElement(ObjectLibrary.testidNEEDTOCHECK()).sendKeys(CommonLibrary.input_data_excel("regression", "testid"));
        Thread.sleep(1000);
        driver.findElement(ObjectLibrary.testidNEEDTOCHECK()).sendKeys(Keys.RETURN);

        //clicking on edit option
        driver.findElement(ObjectLibrary.editCampaignPencilIcon()).click();
        //going to editor page
        Thread.sleep(1000);
        driver.findElement(ObjectLibrary.visualEditorInSteps()).click();
    }

    //*************************************************************************************************************
    //*	                             Creating new segment                                                       *
    //*************************************************************************************************************

    public static void createsegment() throws InterruptedException {

        if (driver.getWindowHandles().size() > 1) {
            //Switch to new window opened
            for (String winHandle : driver.getWindowHandles()) {
                driver.switchTo().window(winHandle);
            }
        }
        Thread.sleep(10000);
        //issue resolved for chrome version 83 and above
        WebElement element1 = driver.findElement(ObjectLibrary.targetingInSteps());
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", element1);
        Thread.sleep(5000);
        driver.findElement(ObjectLibrary.whocampaign()).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[3]/div[1]/div[2]/div/div/div/div/div/div")).click();


        //to select the create new segment
        Actions action = new Actions(driver);
        WebElement optionsList = driver.findElement(By.xpath("//div[contains(@class, 'css-1if9ko0-menu')]"));
        action.moveToElement(optionsList);

        List<WebElement> options = driver.findElements(By.xpath("//div[contains(@class, 'Select__optionValue___2a670')]"));
        for (WebElement option : options) {
            if (option.getText().equals("Create new segment")) {
                option.click();
                break;
            }
        }
        audiencecreate();
        //saving the target
        Thread.sleep(500);
        WebElement element = driver.findElement(ObjectLibrary.saveTarget());
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
        Thread.sleep(1000);
        element.click();
        //updating the tag
        Thread.sleep(1000);
        driver.findElement(ObjectLibrary.Tagupdate()).click();
    }

    public static void savedsegment() throws InterruptedException {

        if (driver.getWindowHandles().size() > 1) {
            //Switch to new window opened
            for (String winHandle : driver.getWindowHandles()) {
                driver.switchTo().window(winHandle);
            }
        }
        Thread.sleep(10000);
        driver.findElement(ObjectLibrary.targetingInSteps()).click();
        Thread.sleep(3000);
        driver.findElement(ObjectLibrary.whocampaign()).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[3]/div[1]/div[2]/div/div/div/div/div/div")).click();


        //to select the saved segment
        Actions action = new Actions(driver);
        WebElement optionsList = driver.findElement(By.xpath("//div[contains(@class, 'css-1if9ko0-menu')]"));
        action.moveToElement(optionsList);

        List<WebElement> options = driver.findElements(By.xpath("//div[contains(@class, 'Select__optionValue___2a670')]"));
        for (WebElement option : options) {
            if (option.getText().equals("Select a segment")) {
                option.click();
                break;
            }
        }
        Thread.sleep(1000);
        driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[3]/div[1]/div[2]/div/div/div[2]/div[2]/div/div/div[1]/span")).click();
        //saving the target
        Thread.sleep(500);
        WebElement element = driver.findElement(ObjectLibrary.saveTarget());
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
        Thread.sleep(1000);
        element.click();
        //updating the tag
        Thread.sleep(1000);
        driver.findElement(ObjectLibrary.Tagupdate()).click();
    }

    public static void triggerwovisitors() throws InterruptedException {

        if (driver.getWindowHandles().size() > 1) {
            //Switch to new window opened
            for (String winHandle : driver.getWindowHandles()) {
                driver.switchTo().window(winHandle);
            }
        }
        Thread.sleep(10000);
        driver.findElement(ObjectLibrary.targetingInSteps()).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[3]/div[3]/div[1]/div/p")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[3]/div[3]/div[2]/div/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]")).click();
        //to select the without trigger
        Actions action = new Actions(driver);
        WebElement optionsList = driver.findElement(By.xpath("//div[contains(@class, 'css-1if9ko0-menu')]"));
        action.moveToElement(optionsList);

        List<WebElement> options = driver.findElements(By.xpath("//div[contains(@class, 'Select__optionValue___2a670')]"));
        for (WebElement option : options) {
            if (option.getText().equals("Without trigger")) {
                option.click();
                break;
            }
        }
        //saving the target
        Thread.sleep(500);
        WebElement element = driver.findElement(ObjectLibrary.saveTarget());
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
        Thread.sleep(1000);
        element.click();
        //updating the tag
        Thread.sleep(1000);
        driver.findElement(ObjectLibrary.Tagupdate()).click();
    }

    public static void customtrigger() throws InterruptedException, IOException {

        if (driver.getWindowHandles().size() > 1) {
            //Switch to new window opened
            for (String winHandle : driver.getWindowHandles()) {
                driver.switchTo().window(winHandle);
            }
        }
        Thread.sleep(10000);
        driver.findElement(ObjectLibrary.targetingInSteps()).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[3]/div[3]/div[1]/div/p")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[3]/div[3]/div[2]/div/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]")).click();
        //to select the custom trigger
        Actions action = new Actions(driver);
        WebElement optionsList = driver.findElement(By.xpath("//div[contains(@class, 'css-1if9ko0-menu')]"));
        action.moveToElement(optionsList);

        List<WebElement> options = driver.findElements(By.xpath("//div[contains(@class, 'Select__optionValue___2a670')]"));
        for (WebElement option : options) {
            if (option.getText().equals("Custom trigger")) {
                option.click();
                break;
            }
        }
        driver.findElement(By.xpath("/html/body/ui-view[2]/div/react-component/div/div/div[1]/div/div/div/div[2]/div[3]/div[3]/div[2]/div/div/div/div[2]/div[2]/div/div/span[1]")).click();
        String targetName = "Browser";
        Thread.sleep(1000);
        driver.findElement(By.xpath("/html/body/div[14]/div/div/div/div[2]/div[1]/div[2]/div/label/input")).click();
        driver.findElement(By.xpath("/html/body/div[14]/div/div/div/div[2]/div[1]/div[2]/div/label/input")).sendKeys(targetName);

        CommonLibrary.hoverAndClick(driver, driver.findElement(By.xpath("/html/body/div[14]/div/div/div/div[2]/div[2]/div[2]/div/p")), driver.findElement(By.xpath("/html/body/div[14]/div/div/div/div[2]/div[2]/div[2]/div/div[2]/button")));
        targettabname(targetName);
        audienceCondition(driver, incexc, value, targetName);
        //saving the target
        Thread.sleep(500);
        WebElement element = driver.findElement(ObjectLibrary.saveTarget());
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
        Thread.sleep(1000);
        element.click();
        //updating the tag
        Thread.sleep(1000);
        driver.findElement(ObjectLibrary.Tagupdate()).click();
    }

    //*************************************************************************************************************
    //*	                               Selecting the audience condition                                           *
    //*************************************************************************************************************

    public static void audienceCondition(WebDriver driver, String incexc, String value, String targetName) throws InterruptedException {

        Thread.sleep(1000);
        driver.findElement(By.className("Select__singleValue___2rcn8")).click();
        //to select the include/exclude
        Actions action = new Actions(driver);
        WebElement optionsList = driver.findElement(By.xpath("//div[contains(@class, 'css-11unzgr')]"));
        action.moveToElement(optionsList);

        List<WebElement> options = driver.findElements(By.xpath("//div[contains(@class, 'Select__optionValue___2a670')]"));
        for (WebElement option : options) {
            if (option.getText().equals(incexc)) {
                option.click();
                break;
            }
        }
        //to select the value
        driver.findElement(By.className("css-1tzmkcy")).click();
        optionsList = driver.findElement(By.xpath("//div[contains(@class, 'css-11unzgr')]"));
        action.moveToElement(optionsList);

        options = driver.findElements(By.xpath("//div[contains(@class, 'Select__optionValue___2a670')]"));
        for (WebElement option : options) {
            if (option.getText().equals(value)) {
                option.click();
                break;
            }
        }
    }


    //*************************************************************************************************************
    //*	                               Selecting the sheet of the target                                          *
    //*************************************************************************************************************

    public static void targettabname(String targetName) throws IOException {
        //to select the target tab
        File src = new File("/Users/monikapanda/Documents/ABTastyData1.xlsx");
        // Load the file
        FileInputStream fis = new FileInputStream(src);
        // load the workbook
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        // get the sheet which you want to modify or create
        for (int i = 0; i < wb.getNumberOfSheets(); i++) {
            if (wb.getSheetName(i).equals(targetName)) {
                XSSFSheet sh2 = wb.getSheetAt(i);
                //r is the row no of the ith sheet
                for (int r = 1; r <= sh2.getLastRowNum(); r++) {
                    String rowvalue = sh2.getRow(r).getCell(0).getStringCellValue();
                    if (rowvalue.equals("Y")) {
                        incexc = sh2.getRow(r).getCell(1).getStringCellValue();
                        value = sh2.getRow(r).getCell(2).getStringCellValue();
                        //System.out.println(incexc + value);
                        if (sh2.getRow(1).getLastCellNum() > 3) {
                            value2 = sh2.getRow(r).getCell(3).getStringCellValue();
                        }

                        if (sh2.getRow(1).getLastCellNum() > 4) {
                            value3 = sh2.getRow(r).getCell(4).getStringCellValue();
                        }
                    }
                }
            }
        }
    }

}
