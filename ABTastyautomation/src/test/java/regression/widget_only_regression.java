package regression;

import org.testng.annotations.Test;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Parameters;

import pageObject.ObjectLibrary;

public class widget_only_regression {

	static ChromeDriver driver;
	static String parentWindow;
	static String expectedvalue;
	static String testrowno;
	public static String os;
	static String testenvironment;
	static String message = "targetting is not properly applied";
	//static String chromedriverpath = "/usr/local/bin/chromedriver";
	static String chromedriverpath = "C:\\abTastySelenium\\drivers\\chromedriver.exe";
	static String browserpath = "--user-data-dir=/Users/monikapanda/Documents/Selenium/BrowserProfile/Profilenew";

	//*************************************************************************************************************
	//*	                                Logging in into ABTasty                                                   *
	//*************************************************************************************************************	
	   
	@Parameters({"testenvironment"})
	@Test(priority = 0)
	public static void Login(String testenv) throws InterruptedException, IOException {
		
	System.out.println("Login started")	;	
		
	System.setProperty("webdriver.chrome.driver", chromedriverpath);

	//To check whether to test in staging or production

	testrowno = "1st";
	testenvironment = testenv;
		
	//String testtype = CommonLibrary.input_data_excel(testrowno, "typeofenv");
	if (testenv.equalsIgnoreCase("staging"))
	{
	//Initialize browser
		ChromeOptions options = new ChromeOptions();
//		options.addArguments("-incognito");
		options.setExperimentalOption("useAutomationExtension", false);
		options.setExperimentalOption("excludeSwitches",Collections.singletonList("enable-automation")); 
		//to start chrome in headless mode
		options.addArguments("headless");
		options.addArguments("--window-size=1920,1080");
		options.addArguments("--no-sandbox");
		options.addArguments("--start-maximized");
		options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
		options.addArguments("--disable-dev-shm-usage") ;
		
	//Open the editor
		driver = new ChromeDriver(options);
	    driver.get("https://staging-app2.abtasty.com/experiments");
		System.out.println("staging");
	}
	else
	{
		//closing the extra line at top
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("useAutomationExtension", false);
		options.setExperimentalOption("excludeSwitches",Collections.singletonList("enable-automation"));  
		//to start chrome in headless mode
		options.addArguments("headless");
		options.addArguments("--window-size=1920,1080");
		options.addArguments("--no-sandbox");
		options.addArguments("--start-maximized");
		options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
		options.addArguments("--disable-dev-shm-usage") ;
		
		driver = new ChromeDriver(options);
		driver.get("https://app2.abtasty.com/experiments");
		System.out.println("prod");
	}

	os = System.getProperty("os.name").toLowerCase();
	//login to the editor
	Thread.sleep(1000);
	driver.findElement(ObjectLibrary.id()).click();
	driver.findElement(ObjectLibrary.id()).sendKeys(CommonLibrary.main_data_excel("tester", "userid"));
	Thread.sleep(1000);
	driver.findElement(ObjectLibrary.password()).click();
	driver.findElement(ObjectLibrary.password()).sendKeys(CommonLibrary.main_data_excel("tester", "password"));
	driver.findElement(ObjectLibrary.signIn()).click();
	driver.manage().window().maximize();
	
	System.out.println("Login successfully ended")	;
	}

	//*************************************************************************************************************
	//*	                                Changing account names                                                    *
	//*************************************************************************************************************	

	@Test(priority = 1)
	public static void selectAccount() throws InterruptedException, IOException {
		
	System.out.println("select account started")	;	
		
	//Locate 'Account Names' drop down
	Thread.sleep(5000);
	if (driver.getPageSource().contains("Welcome to the new navigation experience!")) {
	    driver.findElement(By.xpath("//*[@data-testid=\"onboardingModalCloseIcon\"]")).click();
	}
	Thread.sleep(1000);
	WebElement accountnames = driver.findElement(ObjectLibrary.accountNames());
	accountnames.click();
	WebElement name = driver.findElement(ObjectLibrary.searchAccName());
	String accountname = CommonLibrary.input_data_excel(testrowno, "accountname");
	name.sendKeys(accountname);
	Thread.sleep(1000);
	name.sendKeys(Keys.ARROW_DOWN, Keys.RETURN);
	Thread.sleep(2000);
	
	System.out.println("select account successfully ended")	;
	}


//*************************************************************************************************************
//*	                                Editing an existing test                                                  *
//*************************************************************************************************************	

@Test(priority = 2)
	public static void editABTest() throws InterruptedException, IOException {
	
	System.out.println("edit test started")	;
	
	//Searching for the particular test id
		Thread.sleep(4000);
		Actions acti =  new Actions(driver);
		acti.moveToElement(driver.findElement(ObjectLibrary.searchTestField())).click().build().perform();
		driver.findElement(ObjectLibrary.searchTestField()).sendKeys("959335");
		//clicking on edit option
		Thread.sleep(4000);
		CommonLibrary.hoverAndClick(driver,driver.findElement(ObjectLibrary.editCampaignPencilIcon()),driver.findElement(ObjectLibrary.editCampaignPencilIcon()));
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[contains(text(),'Visual editor')]")).click();
		
		System.out.println("edit test successfully ended")	;
		}

//*************************************************************************************************************
//*	                                Create widgets                                                            *
//*************************************************************************************************************	

@Test(priority = 3)
public static void createvideopopin() throws Exception {
	
	System.out.println("videopopin started")	;
	
	if (driver.getWindowHandles().size() > 1) {
		//Switch to new window opened
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
	}
	
	//Changing background color of a literal
	Thread.sleep(15000);
	driver.switchTo().frame(0);
	driver.findElement(ObjectLibrary.Elementtobechanged()).click();
	driver.switchTo().defaultContent();
	Thread.sleep(1000);
	WebElement menu = driver.findElement(ObjectLibrary.Edit());
	Actions act = new Actions(driver); 
	act.moveToElement(menu).build().perform(); 
	Thread.sleep(1000);
	CommonLibrary.hoverAndClick(driver,driver.findElement(ObjectLibrary.Edit()),driver.findElement(ObjectLibrary.Editstyle()));
	Thread.sleep(1000);
	driver.findElement(ObjectLibrary.colourtab()).click();
	driver.findElement(ObjectLibrary.greencolor()).clear();
	driver.findElement(ObjectLibrary.greencolor()).sendKeys("#3DFFA5");
	driver.findElement(By.xpath("//*[@id=\"tabs-styleColor\"]/table/tbody/tr[6]/td[1]/label")).click();
	driver.findElement(ObjectLibrary.savechanges()).click();
	
//	//adding Wheel of Fortune component
//	Thread.sleep(2000);
//	Actions acti =  new Actions(driver);
//	acti.moveToElement(driver.findElement(objectlibrary.nudge())).click().perform();
//	Thread.sleep(1000);
//	acti.moveToElement(driver.findElement(objectlibrary.searchnudge())).click().build().perform();
//	driver.findElement(objectlibrary.searchnudge()).sendKeys("Wheel");
//
//	driver.findElement(objectlibrary.comp()).click();
//	driver.findElement(objectlibrary.addwidget()).click();
//	Thread.sleep(3000);
//	driver.findElement(objectlibrary.savecomponent()).click();
//	Thread.sleep(2000);
//	driver.findElement(objectlibrary.Showeditor()).click();
		

//adding video popin

	String widget = "video";

	Thread.sleep(1000);
	Actions acti =  new Actions(driver);
	acti.moveToElement(driver.findElement(ObjectLibrary.nudge())).click().perform();
	Thread.sleep(1000);
	acti.moveToElement(driver.findElement(ObjectLibrary.searchnudge())).click().build().perform();
	driver.findElement(ObjectLibrary.searchnudge()).sendKeys(widget);

	driver.findElement(By.xpath("//*[@id=\"ab-embed-editor__modal__pluginsLibrary\"]/div/div/div/section/div/div[3]/div[3]/div[1]/footer/h3")).click();
	driver.findElement(ObjectLibrary.addwidget()).click();
	Thread.sleep(4000);
	if(driver.getPageSource().contains("preset")) {
		driver.findElement(ObjectLibrary.WidgetLayout()).click();
		Thread.sleep(2000);
	}
	driver.findElement(ObjectLibrary.WidgetContent()).click();
	Thread.sleep(2000);
	driver.findElement(ObjectLibrary.widgetsStyle()).click();
	Thread.sleep(2000);
	driver.findElement(ObjectLibrary.widgetCondition()).click();
	Thread.sleep(3000);
	driver.findElement(ObjectLibrary.saveNPS()).click();
	Thread.sleep(2000);
	driver.findElement(ObjectLibrary.Showeditor()).click();

//updating the tag
Thread.sleep(1000);
if(driver.getPageSource().contains("Live")) {
	driver.findElement(By.xpath("//*[text()=\"Tag\"]")).click();
}
else {
driver.findElement(ObjectLibrary.editorplay()).click();
}
parentWindow= driver.getWindowHandle();
verifyTest();
if (driver.getPageSource().contains("video_popin")) {
	 expectedvalue = "true";
}
else {
	 expectedvalue = "false";
}
Thread.sleep(1000);
Assert.assertEquals(expectedvalue,"true", message);

System.out.println("videopopin successfully ended");
}

@Test(priority = 4)
public static void createBanner() throws Exception {
	
System.out.println("banner started");	

editor();

//removing the widget
	
Thread.sleep(9000);
WebElement ele = driver.findElement(ObjectLibrary.sidebar());
Actions actions = new Actions(driver);
actions.moveToElement(ele).click().perform();
Thread.sleep(1000);
//CommonLibrary.HoverAndClick(driver,driver.findElement(By.xpath("//*[@class=\"el-checkbox\"]")),driver.findElement(By.xpath("//*[@class=\"el-checkbox\"]")));
driver.findElement(By.xpath("//*[@id=\"ab-embed-editor__modal__history\"]/div/div/div/div/div[3]/div/div[1]/button")).click();
Thread.sleep(1000);
driver.findElement(By.xpath("//*[contains(@class,\"redBtn\")]")).click();
Thread.sleep(1000);
driver.findElement(By.xpath("//*[@id=\"ab-embed-editor__modal__history\"]/div/div/div/div/div[4]/div/button[2]")).click();
driver.findElement(By.xpath("//button[@class=\"closeBtn\"]")).click();
//Adding a Banner
String widget = "banner";

Thread.sleep(1000);
Actions acti =  new Actions(driver);
acti.moveToElement(driver.findElement(ObjectLibrary.nudge())).click().perform();
Thread.sleep(1000);
acti.moveToElement(driver.findElement(ObjectLibrary.searchnudge())).click().build().perform();
driver.findElement(ObjectLibrary.searchnudge()).sendKeys(widget);

driver.findElement(ObjectLibrary.selectwidget()).click();
driver.findElement(ObjectLibrary.addwidget()).click();
Thread.sleep(4000);
if(driver.getPageSource().contains("preset")) {
	driver.findElement(ObjectLibrary.WidgetLayout()).click();
	Thread.sleep(2000);
}
driver.findElement(ObjectLibrary.WidgetContent()).click();
Thread.sleep(2000);
driver.findElement(ObjectLibrary.widgetsStyle()).click();
Thread.sleep(2000);
driver.findElement(ObjectLibrary.widgetCondition()).click();
Thread.sleep(3000);
driver.findElement(ObjectLibrary.saveNPS()).click();
Thread.sleep(2000);
driver.findElement(ObjectLibrary.Showeditor()).click();

//updating the tag
Thread.sleep(1000);
if(driver.getPageSource().contains("Live")) {
	driver.findElement(By.xpath("//*[text()=\"Tag\"]")).click();
}
else {
driver.findElement(ObjectLibrary.editorplay()).click();
}
parentWindow= driver.getWindowHandle();
verifyTest1();
if (driver.getPageSource().contains(widget)) {
	 expectedvalue = "true";
}
else {
	 expectedvalue = "false";
}
Thread.sleep(1000);
Assert.assertEquals(expectedvalue,"true", message);

System.out.println("banner successfully ended");
}

@Test(priority = 5)
public static void createcountdown() throws Exception {
	
System.out.println("countdown started");

editor();

//removing the widget
    Thread.sleep(9000);
	WebElement ele = driver.findElement(ObjectLibrary.sidebar());
	Actions actions = new Actions(driver);
	actions.moveToElement(ele).click().perform();
	Thread.sleep(1000);
	//CommonLibrary.HoverAndClick(driver,driver.findElement(By.xpath("//*[@class=\"el-checkbox\"]")),driver.findElement(By.xpath("//*[@class=\"el-checkbox\"]")));
	driver.findElement(By.xpath("//*[@id=\"ab-embed-editor__modal__history\"]/div/div/div/div/div[3]/div/div[1]/button")).click();
	Thread.sleep(1000);
driver.findElement(By.xpath("//*[contains(@class,\"redBtn\")]")).click();
Thread.sleep(1000);
driver.findElement(By.xpath("//*[@id=\"ab-embed-editor__modal__history\"]/div/div/div/div/div[4]/div/button[2]")).click();
driver.findElement(By.xpath("//button[@class=\"closeBtn\"]")).click();
//Adding a Countdown 
String widget = "countdown";

Thread.sleep(1000);
Actions acti =  new Actions(driver);
acti.moveToElement(driver.findElement(ObjectLibrary.nudge())).click().perform();
Thread.sleep(1000);
acti.moveToElement(driver.findElement(ObjectLibrary.searchnudge())).click().build().perform();
driver.findElement(ObjectLibrary.searchnudge()).sendKeys(widget);

driver.findElement(ObjectLibrary.countdown()).click();
driver.findElement(ObjectLibrary.addwidget()).click();
Thread.sleep(4000);
if(driver.getPageSource().contains("preset")) {
	driver.findElement(ObjectLibrary.WidgetLayout()).click();
	Thread.sleep(2000);
}
driver.findElement(ObjectLibrary.WidgetContent()).click();
Thread.sleep(2000);
driver.findElement(ObjectLibrary.widgetsStyle()).click();
Thread.sleep(2000);
driver.findElement(ObjectLibrary.widgetCondition()).click();
Thread.sleep(3000);
driver.findElement(ObjectLibrary.saveNPS()).click();
Thread.sleep(2000);
driver.findElement(ObjectLibrary.Showeditor()).click();

//updating the tag
Thread.sleep(1000);
if(driver.getPageSource().contains("Live")) {
	driver.findElement(By.xpath("//*[text()=\"Tag\"]")).click();
}
else {
driver.findElement(ObjectLibrary.editorplay()).click();
}
parentWindow= driver.getWindowHandle();
verifyTest1();
if (driver.getPageSource().contains(widget)) {
	 expectedvalue = "true";
}
else {
	 expectedvalue = "false";
}
Thread.sleep(1000);
Assert.assertEquals(expectedvalue,"true", message);

System.out.println("countdown successfully ended");
}

@Test(priority = 6)
public static void createSimple() throws Exception {
	
System.out.println("simple popin started");	

editor();

//removing the widget

	Thread.sleep(9000);
	WebElement ele = driver.findElement(ObjectLibrary.sidebar());
	Actions actions = new Actions(driver);
	actions.moveToElement(ele).click().perform();
	Thread.sleep(1000);
	//CommonLibrary.HoverAndClick(driver,driver.findElement(By.xpath("//*[@class=\"el-checkbox\"]")),driver.findElement(By.xpath("//*[@class=\"el-checkbox\"]")));
	driver.findElement(By.xpath("//*[@id=\"ab-embed-editor__modal__history\"]/div/div/div/div/div[3]/div/div[1]/button")).click();
	Thread.sleep(1000);
driver.findElement(By.xpath("//*[contains(@class,\"redBtn\")]")).click();
Thread.sleep(1000);
driver.findElement(By.xpath("//*[@id=\"ab-embed-editor__modal__history\"]/div/div/div/div/div[4]/div/button[2]")).click();
driver.findElement(By.xpath("//button[@class=\"closeBtn\"]")).click();
//Adding an Simple Pop-in
String widget = "Simple Pop-in";

Thread.sleep(1000);
Actions acti =  new Actions(driver);
acti.moveToElement(driver.findElement(ObjectLibrary.nudge())).click().perform();
Thread.sleep(1000);
acti.moveToElement(driver.findElement(ObjectLibrary.searchnudge())).click().build().perform();
driver.findElement(ObjectLibrary.searchnudge()).sendKeys(widget);

driver.findElement(ObjectLibrary.simplepopin()).click();
driver.findElement(ObjectLibrary.addwidget()).click();
Thread.sleep(4000);
if(driver.getPageSource().contains("preset")) {
	driver.findElement(ObjectLibrary.WidgetLayout()).click();
	Thread.sleep(2000);
}
driver.findElement(ObjectLibrary.WidgetContent()).click();
Thread.sleep(2000);
driver.findElement(ObjectLibrary.widgetsStyle()).click();
Thread.sleep(2000);
driver.findElement(ObjectLibrary.widgetCondition()).click();
Thread.sleep(3000);
driver.findElement(ObjectLibrary.saveNPS()).click();
Thread.sleep(2000);
driver.findElement(ObjectLibrary.Showeditor()).click();

//updating the tag
Thread.sleep(1000);
if(driver.getPageSource().contains("Live")) {
	driver.findElement(By.xpath("//*[text()=\"Tag\"]")).click();
}
else {
driver.findElement(ObjectLibrary.editorplay()).click();
}
parentWindow= driver.getWindowHandle();
verifyTest1();
if (driver.getPageSource().contains("simple")) {
	 expectedvalue = "true";
}
else {
	 expectedvalue = "false";
}
Thread.sleep(1000);
Assert.assertEquals(expectedvalue,"true", message);

System.out.println("simple popin successfully ended");
}

@Test(priority = 7)
public static void createNPS() throws Exception {
	
	System.out.println("nps started");
	
	editor();

	//removing the widget

	Thread.sleep(9000);
	WebElement ele = driver.findElement(ObjectLibrary.sidebar());
	Actions actions = new Actions(driver);
	actions.moveToElement(ele).click().perform();
	Thread.sleep(1000);
	//CommonLibrary.HoverAndClick(driver,driver.findElement(By.xpath("//*[@class=\"el-checkbox\"]")),driver.findElement(By.xpath("//*[@class=\"el-checkbox\"]")));
	driver.findElement(By.xpath("//*[@id=\"ab-embed-editor__modal__history\"]/div/div/div/div/div[3]/div/div[1]/button")).click();
	Thread.sleep(1000);
	driver.findElement(By.xpath("//*[contains(@class,\"redBtn\")]")).click();
	Thread.sleep(1000);
	driver.findElement(By.xpath("//*[@id=\"ab-embed-editor__modal__history\"]/div/div/div/div/div[4]/div/button[2]")).click();
	driver.findElement(By.xpath("//button[@class=\"closeBtn\"]")).click();
	
	//Adding a NPS
	String widget = "nps";
	Thread.sleep(2000);
	Actions acti =  new Actions(driver);
	acti.moveToElement(driver.findElement(ObjectLibrary.nudge())).click().build().perform();
	Thread.sleep(500);
	acti.moveToElement(driver.findElement(ObjectLibrary.searchnudge())).click().build().perform();
	driver.findElement(ObjectLibrary.searchnudge()).sendKeys(widget);
	driver.findElement(ObjectLibrary.NPS()).click();
	driver.findElement(ObjectLibrary.addwidget()).click();
	Thread.sleep(4000);
	if(driver.getPageSource().contains("preset")) {
		driver.findElement(ObjectLibrary.WidgetLayout()).click();
		Thread.sleep(2000);
	}
	driver.findElement(ObjectLibrary.WidgetContent()).click();
	Thread.sleep(2000);
	driver.findElement(ObjectLibrary.widgetsStyle()).click();
	Thread.sleep(2000);
	driver.findElement(ObjectLibrary.widgetCondition()).click();
	Thread.sleep(3000);
	driver.findElement(ObjectLibrary.saveNPS()).click();
	Thread.sleep(2000);
	driver.findElement(ObjectLibrary.Showeditor()).click();
	
	//updating the tag
	Thread.sleep(1000);
	if(driver.getPageSource().contains("Live")) {
		driver.findElement(By.xpath("//*[text()=\"Tag\"]")).click();
	}
	else {
	driver.findElement(ObjectLibrary.editorplay()).click();
	}
	parentWindow= driver.getWindowHandle();
	verifyTest1();
	if (driver.getPageSource().contains(widget)) {
		 expectedvalue = "true";
	}
	else {
		 expectedvalue = "false";
	}
	Thread.sleep(1000);
	Assert.assertEquals(expectedvalue,"true", message);
	
	System.out.println("nps successfully ended");
	
}

@Test(priority = 8)
public static void undoredo() throws Exception {
	
	System.out.println("undoredo started");
	
	editor();
	
	//removing the widget

		Thread.sleep(9000);
		WebElement ele = driver.findElement(ObjectLibrary.sidebar());
		Actions actions = new Actions(driver);
		actions.moveToElement(ele).click().perform();
		Thread.sleep(1000);
		//CommonLibrary.HoverAndClick(driver,driver.findElement(By.xpath("//*[@class=\"el-checkbox\"]")),driver.findElement(By.xpath("//*[@class=\"el-checkbox\"]")));
		driver.findElement(By.xpath("//*[@id=\"ab-embed-editor__modal__history\"]/div/div/div/div/div[3]/div/div[1]/button")).click();
		Thread.sleep(1000);
	driver.findElement(By.xpath("//*[contains(@class,\"redBtn\")]")).click();
	Thread.sleep(1000);
	driver.findElement(By.xpath("//*[@id=\"ab-embed-editor__modal__history\"]/div/div/div/div/div[4]/div/button[2]")).click();
	driver.findElement(By.xpath("//button[@class=\"closeBtn\"]")).click();
	
	Thread.sleep(2000);
	driver.switchTo().frame(0);
	driver.findElement(By.xpath("//p[@class=\"lead\"]")).click();
	driver.switchTo().defaultContent();
	Thread.sleep(1000);
	WebElement menu = driver.findElement(By.xpath("//*[text()=\"Hide\"]")); 
	Actions act = new Actions(driver); 
	act.moveToElement(menu).perform(); 
	CommonLibrary.hoverAndClick(driver,menu,driver.findElement(By.xpath("//*[text()=\"Hide the element\"]")));
	Thread.sleep(1000);
	driver.findElement(By.xpath("//*[@id=\"ab-embed-editor__sidebar-btn__undo\"]")).click();
	Thread.sleep(1000);
	driver.switchTo().frame(0);
	if( driver.findElement(By.xpath("//p[@class=\"lead\"]")).isDisplayed()){
		System.out.println("Element is Visible");
		expectedvalue = "Element is Visible";
	}
	else{
		System.out.println("Element is InVisible");
		expectedvalue = "Element is InVisible";
	}
	Assert.assertEquals(expectedvalue,"Element is Visible", message);
	driver.switchTo().defaultContent();
	driver.findElement(By.xpath("//*[@id=\"ab-embed-editor__sidebar-btn__redo\"]")).click();
	Thread.sleep(1000);
	driver.switchTo().frame(0);
	if( driver.findElement(By.xpath("//p[@class=\"lead\"]")).isDisplayed()){
		System.out.println("Element is Visible");
		expectedvalue = "Element is Visible";
	}
	else{
		System.out.println("Element is InVisible");
		expectedvalue = "Element is InVisible";
	}
	driver.switchTo().defaultContent();
	Assert.assertEquals(expectedvalue,"Element is InVisible", message);
	
	System.out.println("undoredo successfully ended");
}

@Test(priority = 10)
public static void createsnowflake() throws Exception {
//Adding Snowflake Animation

System.out.println("snowflake successfully started");	
String widget = "Snowflake Animation";

Thread.sleep(1000);
Actions acti =  new Actions(driver);
acti.moveToElement(driver.findElement(ObjectLibrary.nudge())).click().perform();
Thread.sleep(1000);
acti.moveToElement(driver.findElement(ObjectLibrary.searchnudge())).click().build().perform();
driver.findElement(ObjectLibrary.searchnudge()).sendKeys(widget);

driver.findElement(ObjectLibrary.snowflake()).click();
driver.findElement(ObjectLibrary.addwidget()).click();
Thread.sleep(4000);
if(driver.getPageSource().contains("preset")) {
	driver.findElement(ObjectLibrary.WidgetLayout()).click();
	Thread.sleep(2000);
}
driver.findElement(ObjectLibrary.WidgetContent()).click();
Thread.sleep(2000);
driver.findElement(ObjectLibrary.widgetsStyle()).click();
Thread.sleep(2000);
driver.findElement(ObjectLibrary.widgetCondition()).click();
Thread.sleep(3000);
driver.findElement(ObjectLibrary.saveNPS()).click();
Thread.sleep(2000);
driver.findElement(ObjectLibrary.Showeditor()).click();

//updating the tag
Thread.sleep(1000);
if(driver.getPageSource().contains("Live")) {
	driver.findElement(By.xpath("//*[text()=\"Tag\"]")).click();
}
else {
driver.findElement(ObjectLibrary.editorplay()).click();
}
Thread.sleep(20000);
parentWindow= driver.getWindowHandle();
verifyTest1();
if (driver.getPageSource().contains("snowflake")) {
	 expectedvalue = "true";
}
else {
	 expectedvalue = "false";
}
Thread.sleep(1000);
Assert.assertEquals(expectedvalue,"true", message);

System.out.println("snowflake successfully ended");
}

@Test(priority = 11)
public static void redirection() throws Exception {
	
	System.out.println("redirection started");	
	editor();

	//removing the widget

		Thread.sleep(9000);
		WebElement ele = driver.findElement(ObjectLibrary.sidebar());
		Actions actions = new Actions(driver);
		actions.moveToElement(ele).click().perform();
		Thread.sleep(1000);
		//CommonLibrary.HoverAndClick(driver,driver.findElement(By.xpath("//*[@class=\"el-checkbox\"]")),driver.findElement(By.xpath("//*[@class=\"el-checkbox\"]")));
		driver.findElement(By.xpath("//*[@id=\"ab-embed-editor__modal__history\"]/div/div/div/div/div[3]/div/div[1]/button")).click();
		Thread.sleep(1000);
	driver.findElement(By.xpath("//*[contains(@class,\"redBtn\")]")).click();
	Thread.sleep(1000);
	driver.findElement(By.xpath("//*[@id=\"ab-embed-editor__modal__history\"]/div/div/div/div/div[4]/div/button[2]")).click();
	driver.findElement(By.xpath("//button[@class=\"closeBtn\"]")).click();

	//adding redirection
	driver.findElement(By.xpath("//*[@id=\"ab-embed-editor__header\"]/ul/li[2]/a/span")).click();
	driver.findElement(By.xpath("//*[@id=\"ab-embed-editor__header\"]/ul/li[2]/div/ul/li[3]/a/span[2]")).click();
	driver.findElement(By.xpath("//*[@id=\"redirectTargetInput\"]")).sendKeys("http://abtastylab.com/023c2cee0f06c645d5c3d28e61b2e773/destinations.php");
	driver.findElement(By.xpath("//*[@id=\"ab-embed-editor__modal__redirect-variation\"]/div/div/div[4]/button[2]")).click();
	Thread.sleep(1000);
	driver.switchTo().alert().accept();
	//updating the tag
	Thread.sleep(1000);
	if(driver.getPageSource().contains("Live")) {
		driver.findElement(By.xpath("//*[text()=\"Tag\"]")).click();
	}
	else {
	driver.findElement(ObjectLibrary.editorplay()).click();
	}
	Thread.sleep(20000);
	parentWindow= driver.getWindowHandle();
	verifyTest1();
	if (driver.getCurrentUrl().contains("http://abtastylab.com/023c2cee0f06c645d5c3d28e61b2e773/destinations.php")) {
		 expectedvalue = "true";
	}
	else {
		 expectedvalue = "false";
	}
	Thread.sleep(1000);
	Assert.assertEquals(expectedvalue,"true", message);
	
	//remove redirection
	editor();
	Thread.sleep(8000);
	driver.findElement(By.xpath("//*[@id=\"ab-embed-editor__header\"]/ul/li[2]/a/span")).click();
	driver.findElement(By.xpath("//*[@id=\"ab-embed-editor__header\"]/ul/li[2]/div/ul/li[3]/a/span[2]")).click();
	Thread.sleep(1000);
	WebElement redirection = driver.findElement(By.xpath("//*[@id=\"redirectTargetInput\"]"));
	if(os.contains("linux")) {
	redirection.sendKeys(Keys.CONTROL+"a");
	redirection.sendKeys(Keys.DELETE);
	}
	else {
		redirection.sendKeys(Keys.COMMAND+"a");
		redirection.sendKeys(Keys.DELETE);
	}
	Thread.sleep(1000);
	
//	driver.findElement(By.xpath("//*[text()=\"Check the accessibility of the page\"]")).click();
//	driver.findElement(By.xpath("//*[text()=\"Enable redirection\"]")).click();
	driver.findElement(By.xpath("//*[@id=\"ab-embed-editor__modal__redirect-variation\"]/div/div/div[4]/button[2]")).click();
	
	if(driver.getPageSource().contains("Live")) {
		driver.findElement(By.xpath("//*[text()=\"Tag\"]")).click();
	}
	else {
	driver.findElement(ObjectLibrary.editorplay()).click();
	}
	
	System.out.println("redirection successfully ended");	
}


@Test(priority = 8)
public static void createvideo() throws Exception {
//Adding video pop in
String widget = "Video Pop-in";

Thread.sleep(1000);
driver.findElement(ObjectLibrary.nudge()).click();
Thread.sleep(1000);
driver.findElement(ObjectLibrary.searchnudge()).click();
driver.findElement(ObjectLibrary.searchnudge()).sendKeys(widget);

driver.findElement(ObjectLibrary.videopopin()).click();
driver.findElement(ObjectLibrary.addwidget()).click();
Thread.sleep(3000);
driver.findElement(ObjectLibrary.WidgetContent()).click();
Thread.sleep(2000);
driver.findElement(ObjectLibrary.widgetsStyle()).click();
Thread.sleep(2000);
driver.findElement(ObjectLibrary.widgetCondition()).click();
Thread.sleep(3000);
driver.findElement(ObjectLibrary.saveNPS()).click();
Thread.sleep(2000);
driver.findElement(ObjectLibrary.Showeditor()).click();

//updating the tag
Thread.sleep(1000);
if(driver.getPageSource().contains("Live")) {
	driver.findElement(By.xpath("//*[text()=\"Tag\"]")).click();
}
else {
driver.findElement(ObjectLibrary.editorplay()).click();
}
Thread.sleep(20000);
parentWindow= driver.getWindowHandle();
verifyTest();
if (driver.getPageSource().contains("#3DFFA5!important")) {
	 expectedvalue = "true";
}
else {
	 expectedvalue = "false";
}
Thread.sleep(2000);
driver.switchTo().window(parentWindow);
Assert.assertEquals(expectedvalue,"true", message);

//removing the widget

	Thread.sleep(3000);
	WebElement ele = driver.findElement(ObjectLibrary.sidebar());
	Actions actions = new Actions(driver);
	actions.moveToElement(ele).click().perform();
	Thread.sleep(1000);
CommonLibrary.hoverAndClick(driver,driver.findElement(By.xpath("//*[@class=\"el-checkbox\"]")),driver.findElement(By.xpath("//*[@class=\"el-checkbox\"]")));
//driver.findElement(By.xpath("//*[text()=\"Delete\"]")).click();
Thread.sleep(1000);
driver.findElement(By.xpath("//*[contains(@class,\"redBtn\")]")).click();
Thread.sleep(1000);
driver.findElement(By.xpath("//*[@id=\"ab-embed-editor__modal__history\"]/div/div/div/div/div[4]/div/button[2]")).click();
driver.findElement(By.xpath("//button[@class=\"closeBtn\"]")).click();
}

@Test(priority = 9)
public static void createtooltip() throws Exception {
//Adding tooltip
String widget = "Tooltip";

Thread.sleep(1000);
driver.findElement(ObjectLibrary.nudge()).click();
Thread.sleep(1000);
driver.findElement(ObjectLibrary.searchnudge()).click();
driver.findElement(ObjectLibrary.searchnudge()).sendKeys(widget);

driver.findElement(ObjectLibrary.tooltip()).click();
driver.findElement(ObjectLibrary.addwidget()).click();
Thread.sleep(3000);
driver.findElement(ObjectLibrary.WidgetContent()).click();
Thread.sleep(2000);
driver.findElement(ObjectLibrary.widgetsStyle()).click();
Thread.sleep(2000);
driver.findElement(ObjectLibrary.widgetCondition()).click();
Thread.sleep(3000);
driver.findElement(ObjectLibrary.saveNPS()).click();
Thread.sleep(2000);
driver.findElement(ObjectLibrary.Showeditor()).click();

//updating the tag
Thread.sleep(1000);
if(driver.getPageSource().contains("Live")) {
	driver.findElement(By.xpath("//*[text()=\"Tag\"]")).click();
}
else {
driver.findElement(ObjectLibrary.editorplay()).click();
}
Thread.sleep(20000);
parentWindow= driver.getWindowHandle();
verifyTest();
if (driver.getPageSource().contains("#3DFFA5!important")) {
	 expectedvalue = "true";
}
else {
	 expectedvalue = "false";
}
Thread.sleep(2000);
driver.switchTo().window(parentWindow);
Assert.assertEquals(expectedvalue,"true", message);

//removing the widget

	Thread.sleep(3000);
	WebElement ele = driver.findElement(ObjectLibrary.sidebar());
	Actions actions = new Actions(driver);
	actions.moveToElement(ele).click().perform();
	Thread.sleep(1000);
CommonLibrary.hoverAndClick(driver,driver.findElement(By.xpath("//*[@class=\"el-checkbox\"]")),driver.findElement(By.xpath("//*[@class=\"el-checkbox\"]")));
//driver.findElement(By.xpath("//*[text()=\"Delete\"]")).click();
Thread.sleep(1000);
driver.findElement(By.xpath("//*[contains(@class,\"redBtn\")]")).click();
Thread.sleep(1000);
driver.findElement(By.xpath("//*[@id=\"ab-embed-editor__modal__history\"]/div/div/div/div/div[4]/div/button[2]")).click();
driver.findElement(By.xpath("//button[@class=\"closeBtn\"]")).click();
}


//*************************************************************************************************************
//*	                                Verify the test                                                           *
//*************************************************************************************************************	

	public static void verifyTest() throws Exception {
		
		Thread.sleep(50000); 
	 	String URL = CommonLibrary.input_data_excel(testrowno, "URL");
//	 	((JavascriptExecutor) driver).executeScript("window.open(arguments[0])", URL);
	 	if (testenvironment.equalsIgnoreCase("staging")) {
	 	    //intercepting network calls
	 	    networkmocking.networkmock(driver);
	 	}
	 	driver.get(URL);
	    Thread.sleep(3000);

		int tabno = driver.getWindowHandles().size();
		ArrayList<String> windowHandles = new ArrayList<String> (driver.getWindowHandles());
	    driver.switchTo().window(windowHandles.get(tabno-1));

	    //refreshing the page  
	        Thread.sleep(1000);
	        driver.navigate().refresh();
	        Thread.sleep(1000);

		}
	
	//*************************************************************************************************************
	//*	                                Verify the test                                                           *
	//*************************************************************************************************************	

		public static void verifyTest1() throws Exception {
			
			Thread.sleep(51000); 
			
		 	String URL = CommonLibrary.input_data_excel(testrowno, "URL");
	//	 	((JavascriptExecutor) driver).executeScript("window.open(arguments[0])", URL);
		 	driver.get(URL);
		 	//intercepting network calls
		 	//networkmocking.networkmock(driver);

			int tabno = driver.getWindowHandles().size();
			ArrayList<String> windowHandles = new ArrayList<String> (driver.getWindowHandles());
		    driver.switchTo().window(windowHandles.get(tabno-1));

		    //refreshing the page  
	        Thread.sleep(1000);
	        driver.navigate().refresh();
	        Thread.sleep(2000);
			}
		
	//*************************************************************************************************************
	//*	                                going back to editor                                                      *
	//*************************************************************************************************************	

		
		public static void editor() throws Exception {
			
			if (testenvironment.equalsIgnoreCase("staging")) {	
		       driver.get("http://abtastylab.com/023c2cee0f06c645d5c3d28e61b2e773/?abtasty_editor_preprod=959335");
			}
			else {
		       driver.get("http://abtastylab.com/023c2cee0f06c645d5c3d28e61b2e773/?abtasty_editor=959335");
		    }	
		}	
	
//*************************************************************************************************************
//*	                                Close the chromedriver                                                    *
//*************************************************************************************************************	
	@AfterSuite
	public static void quitdriver() {
		driver.quit();
	}
	}

