package regression;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.TimeZone;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CommonLibrary {

	static String excelpath = System.getProperty("user.dir") + "/ABTastyData1.xlsx";

//reading from an excel sheet	

	public static String input_data_excel(String tcname, String testinput) throws IOException {

// Specify the file path which you want to create or write

		String testvalue = "";

		File src = new File(excelpath);

// Load the file

		FileInputStream fis = new FileInputStream(src);

// load the workbook

		XSSFWorkbook wb = new XSSFWorkbook(fis);

// get the sheet which you want to modify or create

		XSSFSheet sh1 = wb.getSheetAt(0);

//i is the row number
		for (int i = 0; i <= sh1.getLastRowNum(); i++) {
			String rowvalue = sh1.getRow(i).getCell(0).getStringCellValue();
			if (rowvalue.equalsIgnoreCase(tcname)) {
				for (int j = 0; j <= sh1.getRow(0).getLastCellNum() - 1; j++) {
					String colvalue = sh1.getRow(0).getCell(j).getStringCellValue();
// j is the column no 
					if (colvalue.equalsIgnoreCase(testinput)) {
//testvalue = sh1.getRow(i).getCell(j).getStringCellValue();	
						DataFormatter formatter = new DataFormatter();
						testvalue = formatter.formatCellValue(sh1.getRow(i).getCell(j));
					}
				}
			}
		}
//System.out.println(testvalue);
		return (testvalue);
	}

	public static String main_data_excel(String tcname, String testinput) throws IOException {
		String testvalue = "";

		File src = new File(excelpath);

		// Load the file

		FileInputStream fis = new FileInputStream(src);

		// load the workbook

		XSSFWorkbook wb = new XSSFWorkbook(fis);

		// get the sheet which you want to modify or create

		XSSFSheet sh1 = wb.getSheetAt(1);

		for (int i = 0; i <= sh1.getLastRowNum(); i++) {
			String rowvalue = sh1.getRow(i).getCell(0).getStringCellValue();
			// System.out.println(rowvalue);
			if (rowvalue.equalsIgnoreCase(tcname)) {
				// i is the row number
				for (int j = 0; j <= sh1.getRow(0).getLastCellNum() - 1; j++) {
					String colvalue = sh1.getRow(0).getCell(j).getStringCellValue();
					// j is the column no
					if (colvalue.equalsIgnoreCase(testinput)) {
						testvalue = sh1.getRow(i).getCell(j).getStringCellValue();
					}
				}
			}
		}
		return (testvalue);
	}

//Hover on an element
	public static void Hover(WebDriver driver, WebElement element) {
		Actions action = new Actions(driver);
		action.moveToElement(element).perform();
	}

//Hover and Click on an element
	public static void hoverAndClick(WebDriver driver, WebElement elementToHover, WebElement elementToClick) {
		Actions action = new Actions(driver);
		action.moveToElement(elementToHover).click(elementToClick).build().perform();
	}

//get the date and time
	public final static String getDateTime() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd_hh:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("CET"));
		return df.format(new Date());
	}

//Takes screenshot
	public static void takeSnapShot(WebDriver webdriver, String fileWithPath) throws Exception {

		// Convert web driver object to TakeScreenshot

		TakesScreenshot scrShot = ((TakesScreenshot) webdriver);

		// Call getScreenshotAs method to create image file

		File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);

		// Move image file to new destination

		File DestFile = new File(fileWithPath);

		// Copy file at destination

		FileUtils.copyFile(SrcFile, DestFile);
	}

	public static void DuplicationChangeAccount(WebDriver webdriver, String name) throws Exception {

		// Click account dropdown
		Thread.sleep(1000);
		webdriver.findElement(By.xpath("//div[@data-testid = 'account-dropdown']")).click();
		System.out.println("check.......account-dropdown");

		Thread.sleep(1000);
		// Enter account name
		System.out.println("check.......Enter");

		// select account
		webdriver.findElement(By.xpath("//div[@class = 'Option__option___2Gik5']")).click();
	}

	public static void ChangeAccount(WebDriver webdriver, String name) throws Exception {

		WebElement AccountDropdown = new WebDriverWait(webdriver, Duration.ofSeconds(10))
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@role=\"combobox\"]")));

		// Click account and enter name
		AccountDropdown.click();
		System.out.println("check.......account-dropdown");
		Thread.sleep(2000);
		webdriver.findElement(By.xpath("//input[@id='downshift-0-input']")).sendKeys(name);
		Thread.sleep(2000);
		// select account
		webdriver.findElement(By.xpath("//li[contains(.,'Mathumitha MURUGAN')]")).click();

		System.out.println(name + " " + "selected");
		Thread.sleep(6000);
	}

	// get the date and time
	public final static String getCurrentDateTime() {

		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		df.setTimeZone(TimeZone.getTimeZone("CET"));
		// get current date time with Date()
		//LocalDateTime now = LocalDateTime.now();
		Date date = new Date();

		return df.format(date);

	}
}
