package regression;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import java.io.IOException;
import java.sql.Timestamp;

public class Test_api_Post {
	
      	public static void APIHitsCollection1(String campaignid, String variationid, String Customerid) throws IOException {
  //   	public static void main(String args[]) throws IOException {
 		for (int i=0; i<2; i++) {
 			
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());  
		String time = Long.toString(timestamp.getTime());
    	System.out.println(time);
		int random = (int )(Math.random() * 10 + 1);
		String visitorid = random + "AM" + time;
		System.out.println(visitorid);
//		String campaignid = "564973";
//		String variationid = "702306";
//		String Customerid = "023c2cee0f06c645d5c3d28e61b2e773";
		System.out.println(campaignid + "" + variationid + "" + Customerid );
		sendPostCampaign(time,visitorid,campaignid,variationid,Customerid);
		sendPostPageView1(time,visitorid,campaignid,variationid,Customerid);
 		}	
	}

    public static void sendPostCampaign(String time,String visitorid,String campaignid, String variationid, String Customerid) throws IOException {
		
	    OkHttpClient client = new OkHttpClient().newBuilder()
			  .build();
			MediaType mediaType = MediaType.parse("application/json");
			RequestBody body = RequestBody.create(mediaType, "{\n\"caid\": \""+campaignid+"\",\n\"cid\": \""+Customerid+"\",\n\"cst\": 1584096794179,\n\"de\": \"UTF-8\",\n\"dl\": \"http%3A%2F%2Fabtastylab.com%023c2cee0f06c645d5c3d28e61b2e773\",\n\"dr\": \"\",\n\"je\": false,\n\"pt\": \"Destinations%20%7C%20Luxury%20Travel%20Site\",\n\"sd\": \"24-bits\",\n\"sen\": 0,\n\"sn\": 1,\n\"sr\": \"1920x1080\",\n\"t\": \"CAMPAIGN\",\n\"ul\": \"fr\",\n\"vaid\": \""+variationid+"\",\n\"vid\": \""+visitorid+"\",\n\"vp\": \"2400x451\"\n}");
			Request request = new Request.Builder()
			  .url("https://ariane.abtasty.com")
			  .method("POST", body)
			  .addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36")
			  .addHeader("Origin", "http://abtastylab.com")
			  .addHeader("Referer", "http://abtastylab.com/023c2cee0f06c645d5c3d28e61b2e773/")
			  .addHeader("Content-Type", "application/json")
			  .build();
			Response response = client.newCall(request).execute();
				
				int responseCode = response.code();
				String message = response.body().string();
				System.out.println("Response Code:" + responseCode + "Response message" + message);
   }

	public static void sendPostPageView1(String time,String visitorid,String campaignid, String variationid, String Customerid) throws IOException {
		
		OkHttpClient client = new OkHttpClient().newBuilder()
				  .build();
				MediaType mediaType = MediaType.parse("application/json");
				//RequestBody body = RequestBody.create(mediaType, "{\n\"c\": {\""+campaignid+"\": \""+variationid+"\"},\n\"cid\": \""+Customerid+"\",\n\"cst\": 1584096794179,\n\"de\": \"UTF-8\",\n\"dl\": \"http%3A%2F%2Fabtastylab.com%023c2cee0f06c645d5c3d28e61b2e773%2F\",\n\"dr\": \"\",\n\"je\": false,\n\"pt\": \"\",\n\"sd\": \"24-bits\",\n\"sen\": 0,\n\"sn\": 1,\n\"sr\": \"1920x1080\",\n\"t\": \"PAGEVIEW\",\n\"ul\": \"fr\",\n\"vid\": \""+visitorid+"\",\n\"vp\": \"2400x451\"\n}");
				RequestBody body = RequestBody.create(mediaType, "{\n\"c\": {\""+campaignid+"\": \""+variationid+"\"},\n\"cid\": \""+Customerid+"\",\n\"cst\": 1584010883507,\n\"de\": \"UTF-8\",\n\"dl\": \"http%3A%2F%2Fabtastylab.com%2F023c2cee0f06c645d5c3d28e61b2e773%2F\",\n\"dr\": \"\",\n\"je\": false,\n\"pt\": \"\",\n\"sd\": \"24-bits\",\n\"sen\": 0,\n\"sn\": 1,\n\"sr\": \"1920x1080\",\n\"t\": \"PAGEVIEW\",\n\"ul\": \"fr\",\n\"vid\": \""+visitorid+"\",\n\"vp\": \"2400x451\"\n}");
				Request request = new Request.Builder()
				  .url("https://ariane.abtasty.com")
				  .method("POST", body)
				  .addHeader("Content-Type", "application/json")
				  .addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36")
				  .addHeader("Origin", "http://abtastylab.com")
				  .addHeader("Referer", "http://abtastylab.com/023c2cee0f06c645d5c3d28e61b2e773/")
				  .build();
				Response response = client.newCall(request).execute();
				int responseCode = response.code();
				String message = response.body().string();
				System.out.println("Response Code:" + responseCode + "Response message" + message);
		
   }
}