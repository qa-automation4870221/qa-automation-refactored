package regression;

import java.awt.AWTException;
import java.awt.Robot;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import pageObject.ObjectLibrary;

public class draganddrop {
	static String chromedriverpath = "C:\\abTastySelenium\\drivers\\chromedriver.exe";

	public static void main(String[] args) throws InterruptedException, IOException, AWTException {
		System.setProperty("webdriver.chrome.driver", chromedriverpath);
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://app2.abtasty.com/experiments");
		//login to the editor
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.id()).click();
		driver.findElement(ObjectLibrary.id()).sendKeys(CommonLibrary.main_data_excel("tester", "userid"));
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.password()).click();
		driver.findElement(ObjectLibrary.password()).sendKeys(CommonLibrary.main_data_excel("tester", "password"));
		driver.findElement(ObjectLibrary.signIn()).click();
		driver.manage().window().maximize();
		Thread.sleep(4000);
//		if (driver.getPageSource().contains("Welcome to the new navigation experience!")) {
//		    driver.findElement(By.xpath("//*[@data-testid=\"onboardingModalCloseIcon\"]")).click();
//		}
//		Thread.sleep(3000);
		driver.get("https://app2.abtasty.com/audiences/list");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[text()=\"New segment\"]")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[text()=\"Select segmentation criteria to build your own segment\"]")).click();
		Thread.sleep(1000);
		
		WebElement from = driver.findElement(By.xpath("//*[text()=\"Cookie\"]"));
		WebElement to = driver.findElement(By.xpath("//*[text()=\"Drag & Drop criteria from the left column to here\"]"));

		Thread.sleep(1000);
		//Creating object of Actions class to build composite actions
		Actions builder = new Actions(driver);
		
        // moving the cursor to the desired location
		Point location = to.getLocation();
		int x = location.getX();
		int y = location.getY();
		System.out.println("x="+ x + "y="+ y);

		Robot robot = new Robot();
		Thread.sleep(2000);
		robot.mouseMove(x, y);
		
		//drag and drop
		builder.dragAndDrop(from, to).click().build().perform();
	}
}
