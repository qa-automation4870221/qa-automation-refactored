
package regression;

import org.testng.annotations.Test;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

import java.io.IOException;
import java.util.Collections;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import pageObject.ObjectLibrary;

//*************************************************************************************************************
//*	                                Multipage Test Duplication to same account     		                       *
//*************************************************************************************************************

public class DuplicationMPToPatchTest {

	static WebDriver driver;
	private By by;
	static String parentWindow;
	public static String ReportURL;
	static String variationid;
	static String Customerid;
	static String testrowno;
	static String typeoftest;
	static String testenvironment;
	public static String browserName;
	public static String os;
	//static String chromedriverpath = "/usr/local/bin/chromedriver";
	static String chromedriverpath = "C:\\abTastySelenium\\drivers\\chromedriver.exe";
	static String browserpath = "--user-data-dir=/Users/monikapanda/Documents/Selenium/BrowserProfile/Profilenew";
	static String excelpath = System.getProperty("user.dir") + "/ABTastyData1.xlsx";

	@BeforeSuite
	public static void initialslackmsg() throws IOException {
		String testername = CommonLibrary.main_data_excel("tester", "Name");
		// EyeSlack.initialpost(testername ,
		// "https://hooks.slack.com/services/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");
	}

//*************************************************************************************************************
//*	                                Logging in into ABTasty                                                   *
//*************************************************************************************************************

	@Parameters({ "testenvironment" })
	@SuppressWarnings("deprecation")
	@Test(priority = 0)
	public static void Login(String testenv) throws InterruptedException, IOException {

		System.out.println("Login started");

		System.setProperty("webdriver.chrome.driver", chromedriverpath);

		// To check whether to test in staging or production

		testrowno = "1st";
		testenvironment = testenv;

		// String testtype = CommonLibrary.input_data_excel(testrowno, "typeofenv");
		if (testenv.equalsIgnoreCase("staging")) {
			// Initialize browser
			ChromeOptions options = new ChromeOptions();
			// options.addArguments("-incognito");
			options.setExperimentalOption("useAutomationExtension", false);
			options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));

			// to start chrome in headless mode
			options.addArguments("headless");
			options.addArguments("--window-size=1920,1080");
			options.addArguments("--no-sandbox");
			options.addArguments("--disable-dev-shm-usage");

			// DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			// capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			// driver=new ChromeDriver(capabilities);
			// options.addArguments(browserpath);
			System.out.println("staging");

			// Open the editor
			driver = new ChromeDriver(options);
			driver.get("https://staging-app2.abtasty.com/experiments");
			System.out.println("login " + driver.getCurrentUrl());
			if (driver.getPageSource().contains("403 ERROR")) {
				System.out.println("403 error on staging");
			}
		} else {
			// closing the extra line at top
			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("useAutomationExtension", false);
			options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
			// to start chrome in headless mode
			options.addArguments("headless");
			options.addArguments("--window-size=1920,1080");
			options.addArguments("--no-sandbox");
			options.addArguments("--disable-dev-shm-usage");

			driver = new ChromeDriver(options);
			driver.get("https://app2.abtasty.com/experiments");
			System.out.println("prod");
			System.out.println("login " + driver.getCurrentUrl());
		}
		// deriving the browser and os
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		browserName = caps.getBrowserName();
		String browserVersion = caps.getVersion();

		// Get OS name.
		os = System.getProperty("os.name").toLowerCase();
		System.out.println("browserName" + browserName + "browserVersion" + browserVersion + "os" + os);

		// login to the editor
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.id()).click();
		driver.findElement(ObjectLibrary.id()).sendKeys(CommonLibrary.main_data_excel("tester", "userid"));
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.password()).click();
		driver.findElement(ObjectLibrary.password()).sendKeys(CommonLibrary.main_data_excel("tester", "password"));
		driver.findElement(ObjectLibrary.signIn()).click();
		driver.manage().window().maximize();

		System.out.println("Login successfully done");
	}

//*************************************************************************************************************
//*	                                Changing account names                                                    *
//*************************************************************************************************************

	@Test(priority = 1)
	public static void selectAccount() throws Exception {

		System.out.println("select account names started");
		
		// Locate 'Account Names' drop down
		Thread.sleep(4000);
		if (driver.getPageSource().contains("Welcome to the new navigation experience!")) {
			driver.findElement(By.xpath("//*[@data-testid=\"onboardingModalCloseIcon\"]")).click();
		}
		// change account
		CommonLibrary.ChangeAccount(driver, "Mathumitha");

		if (driver.getPageSource().contains("Mathumitha MURUGAN")) {
			System.out.println("Given Account selected");
		} else {
			System.out.println("Selected other Account");
		}

		System.out.println("Select account names successfully done");
	}
	
//*************************************************************************************************************
//*	                               MultiPage Test To PAtch Test Duplication                	       	          *
//*************************************************************************************************************

	@Test(priority = 2)
	public void MPtoPatch() throws Exception {
		
		System.out.println("checking.......MPtoMP");

		Thread.sleep(1000);
		//Reset the filter
		driver.findElement(ObjectLibrary.newFilter()).click();
		Thread.sleep(1000);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,600)", "");
		driver.findElement(ObjectLibrary.resetFilter()).click();
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.applyFilter()).click();

		// scroll up
		js.executeScript("window.scrollBy(0,-300)", "");

		System.out.println("check.......scroll");

		Thread.sleep(1000);
		// click filter
		driver.findElement(ObjectLibrary.newFilter()).click();
		// Click A/B Test
		driver.findElement(ObjectLibrary.multiPageTestFilter()).click();
		js.executeScript("window.scrollBy(0,500)", "");
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.applyFilter()).click();

		Thread.sleep(2000);
		js.executeScript("window.scrollBy(0,-500)", "");
		// click 'action menu(3 dots)'
		CommonLibrary.hoverAndClick(driver, driver.findElement(ObjectLibrary.moreActions()),
				driver.findElement(ObjectLibrary.moreActions()));
		// Click 'Duplicate'
		driver.findElement(By.xpath("//*[(text()='Duplicate')]")).click();

		// Click 'dropdown'
		Thread.sleep(500);
		by = By.xpath("//div[1]/div[2][@data-testid = 'subType-dropdown']");
		driver.findElement(by).click();
		System.out.println("check.......dropdown");

		// Click Patch test
		Thread.sleep(500);
		by = By.xpath("//div[contains(text(), 'Patch')]");
		driver.findElement(by).click();
		Thread.sleep(500);

		// getCurrentDateTime
		String currentdate = CommonLibrary.getCurrentDateTime();
		System.out.println(currentdate);

		String MPtoPatchTest = "MPtoPatchTest" + currentdate;

		// clear and enter name
		Thread.sleep(500);
		by = By.xpath("//div[2]/label[1]/input[@class = 'Input__commonInput___35r8X']");
		driver.findElement(by).clear();
		driver.findElement(by).sendKeys(MPtoPatchTest);

		// Click 'Duplicate'
		Thread.sleep(500);
		by = By.xpath("//button[2][. = 'Duplicate']");
		driver.findElement(by).click();

		// Is 'SimplePersoAuto (duplicate)' present?
		Thread.sleep(500);
		by = By.xpath("//p[. = 'MPtoMP']");

		if (driver.getPageSource().contains(MPtoPatchTest)) {
			System.out.println("MP to Patch Duplicated test is visible");
		} else {
			System.out.println("MP to Patch Duplicated test is not visible");
		}
		System.out.println("MP to Patch Test Duplication ended");
	}
	
//*************************************************************************************************************
//*	                                Closing the drive                                        	              *
//*************************************************************************************************************	

	@AfterSuite
	public void closedriver() throws IOException {
		driver.quit();
	}

}