
package regression;

import org.testng.annotations.Test;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

import java.io.IOException;
import java.util.Collections;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import pageObject.ObjectLibrary;


//*************************************************************************************************************
//*	                                Multipage Perso Duplication to different account     		              *
//*************************************************************************************************************


public class DuplicationMPP2 {

	static WebDriver driver;
	private By by;
	static String parentWindow;
	public static String ReportURL;
	static String variationid;
	static String Customerid;
	static String testrowno;
	static String typeoftest;
	static String testenvironment;
	public static String browserName;
	public static String os;
	//static String chromedriverpath = "/usr/local/bin/chromedriver";
	static String chromedriverpath = "C:\\abTastySelenium\\drivers\\chromedriver.exe";
	static String browserpath = "--user-data-dir=/Users/monikapanda/Documents/Selenium/BrowserProfile/Profilenew";
	static String excelpath = System.getProperty("user.dir") + "/ABTastyData1.xlsx";

	@BeforeSuite
	public static void initialslackmsg() throws IOException {
		String testername = CommonLibrary.main_data_excel("tester", "Name");
		// EyeSlack.initialpost(testername ,
		// "https://hooks.slack.com/services/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");
	}

//*************************************************************************************************************
//*	                                Logging in into ABTasty                                                   *
//*************************************************************************************************************

	@Parameters({ "testenvironment" })
	@SuppressWarnings("deprecation")
	@Test(priority = 0)
	public static void Login(String testenv) throws InterruptedException, IOException {

		System.out.println("Login started");

		System.setProperty("webdriver.chrome.driver", chromedriverpath);

		// To check whether to test in staging or production

		testrowno = "1st";
		testenvironment = testenv;

		// String testtype = CommonLibrary.input_data_excel(testrowno, "typeofenv");
		if (testenv.equalsIgnoreCase("staging")) {
			// Initialize browser
			ChromeOptions options = new ChromeOptions();
			// options.addArguments("-incognito");
			options.setExperimentalOption("useAutomationExtension", false);
			options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));

			// to start chrome in headless mode
			options.addArguments("headless");
			options.addArguments("--window-size=1920,1080");
			options.addArguments("--no-sandbox");
			options.addArguments("--disable-dev-shm-usage");

			// DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			// capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			// driver=new ChromeDriver(capabilities);
			// options.addArguments(browserpath);
			System.out.println("staging");

			// Open the editor
			driver = new ChromeDriver(options);
			driver.get("https://staging-app2.abtasty.com/experiments");
			System.out.println("login " + driver.getCurrentUrl());
			if (driver.getPageSource().contains("403 ERROR")) {
				System.out.println("403 error on staging");
			}
		} else {
			// closing the extra line at top
			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("useAutomationExtension", false);
			options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
			// to start chrome in headless mode
			options.addArguments("headless");
			options.addArguments("--window-size=1920,1080");
			options.addArguments("--no-sandbox");
			options.addArguments("--disable-dev-shm-usage");

			driver = new ChromeDriver(options);
			driver.get("https://app2.abtasty.com/experiments");
			System.out.println("prod");
			System.out.println("login " + driver.getCurrentUrl());
		}
		// deriving the browser and os
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		browserName = caps.getBrowserName();
		String browserVersion = caps.getVersion();

		// Get OS name.
		os = System.getProperty("os.name").toLowerCase();
		System.out.println("browserName" + browserName + "browserVersion" + browserVersion + "os" + os);

		// login to the editor
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.id()).click();
		driver.findElement(ObjectLibrary.id()).sendKeys(CommonLibrary.main_data_excel("tester", "userid"));
		Thread.sleep(1000);
		driver.findElement(ObjectLibrary.password()).click();
		driver.findElement(ObjectLibrary.password()).sendKeys(CommonLibrary.main_data_excel("tester", "password"));
		driver.findElement(ObjectLibrary.signIn()).click();
		driver.manage().window().maximize();

		System.out.println("Login successfully done");
	}

//*************************************************************************************************************
//*	                                Changing account names                                                    *
//*************************************************************************************************************

	@Test(priority = 1)
	public static void selectAccount() throws Exception {

		System.out.println("select account names started");

		// Locate 'Account Names' drop down
		Thread.sleep(4000);
		if (driver.getPageSource().contains("Welcome to the new navigation experience!")) {
			driver.findElement(By.xpath("//*[@data-testid=\"onboardingModalCloseIcon\"]")).click();
		}
		// change account
		CommonLibrary.ChangeAccount(driver, "Mathumitha");

		if (driver.getPageSource().contains("Mathumitha MURUGAN")) {
			System.out.println("Given Account selected");
		} else {
			System.out.println("Selected other Account");
		}

		System.out.println("Select account names successfully done");
	}

//*************************************************************************************************************
//*	                               MultiPage Perso To MultiPage Perso Duplication       	       	          *
//*************************************************************************************************************

	@Test(priority = 2)
	public void MPPersoToMPPerso() throws Exception {
		
		System.out.println("checking.......MPPersoToMPPerso");

		// click perso
		driver.findElement(By.xpath("//div[contains(text(), 'Personalization')]")).click();

//		// Click 'Your personalizations'
//		Thread.sleep(500);
//		by = By.xpath("//a[. = 'Your personalizations']");
//		driver.findElement(by).click();

		// Scroll window by ('0','287')
		Thread.sleep(500);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,287)", "");

		System.out.println("check.......scroll");

		// click filter
		driver.findElement(By.xpath("//button[contains(text(),'Filter')]")).click();
		// Click A/B Test
		driver.findElement(By.xpath("//span[normalize-space()='Multipage personalization']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//button[normalize-space()='Apply']")).click();
		Thread.sleep(2000);

		// click 'action menu(3 dots)'
		CommonLibrary.hoverAndClick(driver, driver.findElement(By.xpath("//*[@data-testid=\"moreActions\"]")),
				driver.findElement(By.xpath("//*[@data-testid=\"moreActions\"]")));

		// Click 'Duplicate'
		Thread.sleep(500);
		by = By.xpath("//div[1]/div[9]/div/div[2]//div[2][. = 'Duplicate']");
		driver.findElement(by).click();

		// Click 'cancel'
		Thread.sleep(500);
		by = By.xpath("//button[contains(text(),'Cancel')]");
		driver.findElement(by).click();
		System.out.println("check.......Cancel");


		// click 'action menu(3 dots)'
		CommonLibrary.hoverAndClick(driver, driver.findElement(By.xpath("//*[@data-testid=\"moreActions\"]")),
				driver.findElement(By.xpath("//*[@data-testid=\"moreActions\"]")));

		// Click 'Duplicate'
		Thread.sleep(500);
		by = By.xpath("//div[1]/div[9]/div/div[2]//div[2][. = 'Duplicate']");
		driver.findElement(by).click();

		// Click account dropdown
		by = By.xpath("//div[@data-testid = 'account-dropdown']");
		Thread.sleep(1000);
		driver.findElement(by).click();
		System.out.println("check.......account-dropdown");

		Thread.sleep(1000);
		// Enter account name
		driver.findElement(By.xpath("//div/div/input[@autocorrect='off']")).sendKeys("Jérémie Franchomme");
		System.out.println("check.......Enter");

		// select account
		Thread.sleep(500);
		by = By.xpath("//div[@class = 'Option__option___2Gik5']");
		driver.findElement(by).click();

		// clear and enter name
		Thread.sleep(500);
		by = By.xpath("//div[2]/label[1]/input[@class = 'Input__commonInput___35r8X']");
		driver.findElement(by).clear();
		driver.findElement(by).sendKeys("MPPersoToMPPerso");

		// Click 'Duplicate'
		Thread.sleep(500);
		by = By.xpath("//button[2][. = 'Duplicate']");
		driver.findElement(by).click();

		// Click 'success
		Thread.sleep(500);
		by = By.xpath("//ul/div");
		driver.findElement(by).click();

		// Click account and enter name
		Thread.sleep(500);
		by = By.xpath("//div[@role=\"combobox\"]");
		driver.findElement(by).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@id='downshift-0-input']")).sendKeys("Jérémie Franchomme");

		// select account
		Thread.sleep(500);
		by = By.xpath("//li[@id='item-0']");
		driver.findElement(by).click();
		Thread.sleep(5000);

		// click perso
		driver.findElement(By.xpath("//div[contains(text(), 'Personalization')]")).click();

		// Scroll window by ('0','287')
		Thread.sleep(500);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,287)", "");

		// Is MPPersoToMPPerso present?
		Thread.sleep(500);
		by = By.xpath("//p[. = 'MPPersoToMPPerso']");

		if (driver.findElement(by).isDisplayed()) {
			System.out.println("MPPerso To MPPerso is visible");
		} else {
			System.out.println("MPPerso To MPPerso is not visible");
		}
		System.out.println("MPPerso To MPPerso Duplication ended");
	}

//*************************************************************************************************************
//*	                               MultiPage Perso To Simple Perso Duplication                  	          *
//*************************************************************************************************************

	@Test(priority = 3)
	public void MPPersoToSimplePerso() throws Exception {

		System.out.println("checking.......MPPersoToSimplePerso");

		// Click account and enter name
		Thread.sleep(500);
		by = By.xpath("//div[@role=\"combobox\"]");
		driver.findElement(by).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@id='downshift-0-input']")).sendKeys("Mathumitha Murugan");

		// select account
		Thread.sleep(500);
		by = By.xpath("//li[@id='item-0']");
		driver.findElement(by).click();
		Thread.sleep(5000);

		// click perso
		driver.findElement(By.xpath("//div[contains(text(), 'Personalization')]")).click();

		// click filter
		driver.findElement(By.xpath("//button[contains(text(),'Filter')]")).click();
		// Click A/B Test
		driver.findElement(By.xpath("//span[normalize-space()='Multipage personalization']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//button[normalize-space()='Apply']")).click();
		Thread.sleep(2000);

		// click 'action menu(3 dots)'
		CommonLibrary.hoverAndClick(driver, driver.findElement(By.xpath("//*[@data-testid=\"moreActions\"]")),
				driver.findElement(By.xpath("//*[@data-testid=\"moreActions\"]")));

		// Click 'Duplicate'
		Thread.sleep(500);
		by = By.xpath("//div[1]/div[9]/div/div[2]//div[2][. = 'Duplicate']");
		driver.findElement(by).click();
		System.out.println("check.......Duplicate");

		// Click 'dropdown'
		Thread.sleep(500);
		by = By.xpath("//div[1]/div[2][@data-testid = 'subType-dropdown']");
		driver.findElement(by).click();
		System.out.println("check.......dropdown");

		// Click 'Simple personalization'
		Thread.sleep(500);
		by = By.xpath("//div[contains(text(), 'Simple personalization')]");
		driver.findElement(by).click();

		// Click account dropdown
		by = By.xpath("//div[@data-testid = 'account-dropdown']");
		Thread.sleep(1000);
		driver.findElement(by).click();
		System.out.println("check.......account-dropdown");
		Thread.sleep(1000);

		// Enter account name
		driver.findElement(By.xpath("//div/div/input[@autocorrect='off']")).sendKeys("Jérémie Franchomme");
		System.out.println("check.......Enter");

		// select account
		Thread.sleep(500);
		by = By.xpath("//div[@class = 'Option__option___2Gik5']");
		driver.findElement(by).click();

		// clear and enter name
		Thread.sleep(500);
		by = By.xpath("//div[2]/label[1]/input[@class = 'Input__commonInput___35r8X']");
		driver.findElement(by).clear();
		driver.findElement(by).sendKeys("MPPersoToSimplePerso");

		// Click 'Duplicate'
		Thread.sleep(500);
		by = By.xpath("//button[2][. = 'Duplicate']");
		driver.findElement(by).click();

		// Click 'success
		Thread.sleep(500);
		by = By.xpath("//ul/div");
		driver.findElement(by).click();

		// Click account and enter name
		Thread.sleep(500);
		by = By.xpath("//div[@role=\"combobox\"]");
		driver.findElement(by).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@id='downshift-0-input']")).sendKeys("Jérémie Franchomme");

		// select account
		Thread.sleep(500);
		by = By.xpath("//li[@id='item-0']");
		driver.findElement(by).click();
		Thread.sleep(5000);

		// click perso
		driver.findElement(By.xpath("//div[contains(text(), 'Personalization')]")).click();

		// Scroll window by ('0','287')
		Thread.sleep(500);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,287)", "");

		// Is 'MPToSimplePersoDuplicate' present?
		Thread.sleep(500);
		by = By.xpath("//p[. = 'MPPersoToSimplePerso']");

		if (driver.findElement(by).isDisplayed()) {
			System.out.println("Duplicated MPPerso To SimplePerso Duplicate is visible");
		} else {
			System.out.println("Duplicated MPPerso To SimplePerso Duplicate  is not visible");
		}
		System.out.println("MPPerso To SimplePerso Duplication ended");
	}

//*************************************************************************************************************
//*	                               MultiPage Perso To MultiPage Test Duplication                	          *
//*************************************************************************************************************

	@Test(priority = 4)
	public void MPPersoToMPTest() throws Exception {

		System.out.println("checking.......MPPersoToMPTest");

		// Click account and enter name
		Thread.sleep(500);
		by = By.xpath("//div[@role=\"combobox\"]");
		driver.findElement(by).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@id='downshift-0-input']")).sendKeys("Mathumitha Murugan");

		// select account
		Thread.sleep(500);
		by = By.xpath("//li[@id='item-0']");
		driver.findElement(by).click();
		Thread.sleep(5000);

		// click filter
		driver.findElement(By.xpath("//button[contains(text(),'Filter')]")).click();
		// Click A/B Test
		driver.findElement(By.xpath("//span[normalize-space()='Multipage personalization']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//button[normalize-space()='Apply']")).click();
		Thread.sleep(2000);

		// click 'action menu(3 dots)'
		CommonLibrary.hoverAndClick(driver, driver.findElement(By.xpath("//*[@data-testid=\"moreActions\"]")),
				driver.findElement(By.xpath("//*[@data-testid=\"moreActions\"]")));

		// Click 'Duplicate'
		Thread.sleep(500);
		by = By.xpath("//div[1]/div[9]/div/div[2]//div[2][. = 'Duplicate']");
		driver.findElement(by).click();

		// Click 'dropdown'
		Thread.sleep(500);
		by = By.xpath("//div[1]/div[2][@data-testid = 'subType-dropdown']");
		driver.findElement(by).click();
		System.out.println("check.......dropdown");

		// Click 'Multipage test'
		Thread.sleep(500);
		by = By.xpath("//div[contains(text(), 'Multipage personalization')]");
		driver.findElement(by).click();

		// Click account dropdown
		by = By.xpath("//div[@data-testid = 'account-dropdown']");
		Thread.sleep(1000);
		driver.findElement(by).click();
		System.out.println("check.......account-dropdown");
		Thread.sleep(1000);

		// Enter account name
		driver.findElement(By.xpath("//div/div/input[@autocorrect='off']")).sendKeys("Jérémie Franchomme");
		System.out.println("check.......Enter");

		// select account
		Thread.sleep(500);
		by = By.xpath("//div[@class = 'Option__option___2Gik5']");
		driver.findElement(by).click();

		// clear and enter name
		Thread.sleep(500);
		by = By.xpath("//div[2]/label[1]/input[@class = 'Input__commonInput___35r8X']");
		driver.findElement(by).clear();
		driver.findElement(by).sendKeys("MPPersoToMPTest");

		// Click 'Duplicate'
		Thread.sleep(500);
		by = By.xpath("//button[2][. = 'Duplicate']");
		driver.findElement(by).click();

		// Click 'success
		Thread.sleep(500);
		by = By.xpath("//ul/div");
		driver.findElement(by).click();

		// Click account and enter name
		Thread.sleep(500);
		by = By.xpath("//div[@role=\"combobox\"]");
		driver.findElement(by).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@id='downshift-0-input']")).sendKeys("Jérémie Franchomme");

		// select account
		Thread.sleep(500);
		by = By.xpath("//li[@id='item-0']");
		driver.findElement(by).click();
		Thread.sleep(5000);

		// Scroll window by ('0','-287')
		Thread.sleep(500);
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,287)", "");

		// Is 'MultipageToMultiTest' present?
		Thread.sleep(500);
		by = By.xpath("//p[. = 'MPPersoToMPTest']");

		if (driver.findElement(by).isDisplayed()) {
			System.out.println("MPPerso To MPTest is visible");
		} else {
			System.out.println("MPPerso To MPTest is not visible");
		}
		
		System.out.println("MPPerso To MPTest Duplication ended");
	}

//*************************************************************************************************************
//*	                                Closing the drive                                        	              *
//*************************************************************************************************************	

	@AfterSuite
	public void closedriver() throws IOException {
		driver.quit();
	}

}
