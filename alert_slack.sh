#Send Link with the artifacts to the Slack channel
PROJECT_NAME=$1
JOB_ID=$2

newUrl="https://gitlab.com/abtasty/backoffice/qa-automation/-/jobs/$JOB_ID"

message="Selenium - some tests failed: $PROJECT_NAME results $newUrl"

curl --location --request POST 'https://hooks.slack.com/services/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' \
--header 'Content-Type: application/json' \
--data-raw "{ \"channel\": \"autotest-results\", \"text\": \"$message\" }"
